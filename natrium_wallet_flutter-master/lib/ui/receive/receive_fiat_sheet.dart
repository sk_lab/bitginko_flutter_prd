import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/themes.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheets.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/receive/share_card.dart';
import 'package:natrium_wallet_flutter/model/wallet.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../app_icons2.dart';

class AppReceiveFiatSheet {
  AppWallet _wallet;

  GlobalKey shareCardKey;
  Widget appShareCard;
  ByteData shareImageData;
  // Widget qrSVGBorder;
  Widget shareCardLogoSvg;
  Widget shareCardTickerSvg;

  // Widget qrCode;
  // Widget qrWidget;

  AppReceiveFiatSheet();

  // Address copied items
  // Current state references
  bool _showShareCard;
  bool _addressCopied;
  // Timer reference so we can cancel repeated events
  Timer _addressCopiedTimer;

  Future<Uint8List> _capturePng() async {
    if (shareCardKey != null && shareCardKey.currentContext != null) {
      RenderRepaintBoundary boundary =
          shareCardKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 5.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      return byteData.buffer.asUint8List();
    } else {
      return null;
    }
  }

  mainBottomSheet(BuildContext context, {String coinSymbol = 'BTC', IconData coinIcon = AppIcons2.hkd}) {
    _wallet = StateContainer.of(context).wallet;
    // Set initial state of copy button
    _addressCopied = false;
    double devicewidth = MediaQuery.of(context).size.width;
    // Create our SVG-heavy things in the constructor because they are slower operations
    // qrSVGBorder = SvgPicture.asset('assets/QR.svg');
    // shareCardLogoSvg = SvgPicture.asset('assets/icon/bitginkoIcon.png'); // johnny: assets/sharecard_logo.svg
    // Share card initialization
    // shareCardKey = GlobalKey();
    // appShareCard = Container(
    //   child: AppShareCard(shareCardKey, qrSVGBorder, shareCardLogoSvg),
    //   alignment: AlignmentDirectional(0.0, 0.0),
    // );
    // qrCode = qrWidget;

    _showShareCard = false;

    AppSheets.showAppHeightEightSheet(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return SafeArea(
                minimum: 
                  EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.035),
                child: Column(
                  children: <Widget>[
                    // A row for the '存入HKD' and warning text
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        // Left Empty SizedBox
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                        // middle main container
                        Column(
                          children: <Widget>[
                            // Sheet handle (johnny: the little horizontal bar at top)
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 5,
                              width: MediaQuery.of(context).size.width * 0.15,
                              decoration: BoxDecoration(
                                color: StateContainer.of(context).curTheme.text10,
                                borderRadius: BorderRadius.circular(100.0),
                              ),
                            ),

                            // johnny: title + warning
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    '存入港元 (銀行轉帳)', // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 22,
                                      fontWeight: FontWeight.w700,
                                      color: StateContainer.of(context).curTheme.text),
                                  ),
                                  SizedBox(
                                    width: 60,
                                    height: 10,
                                  ),
                                  Text(
                                    '(累計上限為 8000港元,', // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red //StateContainer.of(context).curTheme.text60
                                      ),
                                  ),
                                  Text(
                                    '如要提高存款上限請先進行身分認證)', // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red  // StateContainer.of(context).curTheme.text60
                                      ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        // Right Empty SizedBox
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                      ],
                    ),

                    SizedBox(
                      width: 60,
                      height: 30,
                    ),

                    Expanded(
                      child: // A column for displaying 4 rows of bank info, evenly spaced.
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          
                          children: <Widget>[
                              // 1st row
                              Row( 
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(
                                    // width: 2,
                                    height: 70,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      '銀行名稱:', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                      'Hang Seng Bank', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),

                                  OutlineButton(
                                    onPressed: () => {
                                      Clipboard.setData(new ClipboardData(text: "Hang Seng Bank")),
                                        // johnnyfuture: do the copied green style, when copied, later
                                        // setState(() {
                                        //   // Set copied style
                                        //   _addressCopied = true;
                                        // });
                                        // if (_addressCopiedTimer != null) {
                                        //   _addressCopiedTimer.cancel();
                                        // }
                                        // _addressCopiedTimer = new Timer(
                                        //     const Duration(milliseconds: 800), () {
                                        //   setState(() {
                                        //     _addressCopied = false;
                                        //   });
                                        // });
                                    },
                                    textColor: StateContainer.of(context).curTheme.primary,
                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(color: StateContainer.of(context).curTheme.primary),
                                    child: Container(
                                        margin: EdgeInsetsDirectional.only(start: 4),
                                        child: Text("複製", // johnnyTranslate
                                              style: AppStyles.textStyleParagraphThinPrimary(context)),
                                    ),
                                  ),

                                  // SizedBox(
                                  //   width: 10,
                                  //   // height: 60,
                                  // ),
                                ],
                              ),

                              // 2nd row
                              Row( 
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(
                                    // width: 2,
                                    height: 70,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      '戶口名稱:', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                      'Standard Kepler Limited', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),

                                  OutlineButton(
                                    onPressed: () => {
                                      Clipboard.setData(new ClipboardData(text: "Standard Kepler Limited")),
                                        // johnnyfuture: do the copied green style, when copied, later
                                        // setState(() {
                                        //   // Set copied style
                                        //   _addressCopied = true;
                                        // });
                                        // if (_addressCopiedTimer != null) {
                                        //   _addressCopiedTimer.cancel();
                                        // }
                                        // _addressCopiedTimer = new Timer(
                                        //     const Duration(milliseconds: 800), () {
                                        //   setState(() {
                                        //     _addressCopied = false;
                                        //   });
                                        // });
                                    },
                                    textColor: StateContainer.of(context).curTheme.primary,
                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(color: StateContainer.of(context).curTheme.primary),
                                    child: Container(
                                        margin: EdgeInsetsDirectional.only(start: 4),
                                        child: Text("複製", // johnnyTranslate
                                              style: AppStyles.textStyleParagraphThinPrimary(context)),
                                    ),
                                  ),

                                  // SizedBox(
                                  //   width: 10,
                                  //   // height: 60,
                                  // ),
                                ],
                              ),

                              // 3rd row
                              Row( 
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(
                                    // width: 2,
                                    height: 70,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      '戶口號碼:', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                      '395494719883', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),

                                  OutlineButton(
                                    onPressed: () => {
                                      Clipboard.setData(new ClipboardData(text: "395494719883")),
                                        // johnnyfuture: do the copied green style, when copied, later
                                        // setState(() {
                                        //   // Set copied style
                                        //   _addressCopied = true;
                                        // });
                                        // if (_addressCopiedTimer != null) {
                                        //   _addressCopiedTimer.cancel();
                                        // }
                                        // _addressCopiedTimer = new Timer(
                                        //     const Duration(milliseconds: 800), () {
                                        //   setState(() {
                                        //     _addressCopied = false;
                                        //   });
                                        // });
                                    },
                                    textColor: StateContainer.of(context).curTheme.primary,
                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(color: StateContainer.of(context).curTheme.primary),
                                    child: Container(
                                        margin: EdgeInsetsDirectional.only(start: 4),
                                        child: Text("複製", // johnnyTranslate
                                              style: AppStyles.textStyleParagraphThinPrimary(context)),
                                    ),
                                  ),

                                  // SizedBox(
                                  //   width: 10,
                                  //   // height: 60,
                                  // ),
                                ],
                              ),

                              // 4th row
                              Row( 
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(
                                    // width: 2,
                                    height: 70,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      'FPS:', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),
                                  Container(
                                    width: 140,
                                    child: Text(
                                      '2279313', // johnnyTranslate.
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: StateContainer.of(context).curTheme.text),
                                    ),
                                  ),

                                  OutlineButton(
                                    onPressed: () => {
                                      Clipboard.setData(new ClipboardData(text: "2279313")),
                                        // johnnyfuture: do the copied green style, when copied, later
                                        // setState(() {
                                        //   // Set copied style
                                        //   _addressCopied = true;
                                        // });
                                        // if (_addressCopiedTimer != null) {
                                        //   _addressCopiedTimer.cancel();
                                        // }
                                        // _addressCopiedTimer = new Timer(
                                        //     const Duration(milliseconds: 800), () {
                                        //   setState(() {
                                        //     _addressCopied = false;
                                        //   });
                                        // });
                                    },
                                    textColor: StateContainer.of(context).curTheme.primary,
                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(color: StateContainer.of(context).curTheme.primary),
                                    child: Container(
                                        margin: EdgeInsetsDirectional.only(start: 4),
                                        child: Text("複製", // johnnyTranslate
                                              style: AppStyles.textStyleParagraphThinPrimary(context)),
                                    ),
                                  ),

                                  // SizedBox(
                                  //   width: 10,
                                  //   // height: 60,
                                  // ),
                                ],
                              ),

                          ],
                        ),
                    ),
                    

                    Container(
                        //A column with Copy Address buttons
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                AppButton.buildAppButton(
                                    context,
                                    AppButtonType.PRIMARY,
                                    "上傳入數紙", // johnnyTranslate //AppLocalization.of(context).copyAddress,
                                    Dimens.BUTTON_TOP_DIMENS, onPressed: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                          return UIUtil.showWebview(context,'https://bitginko.com/support'); // johnnytodo: put this link into config, make it consistent everywhere
                                        }));
                                    }),
                              ],
                            ),
    
                          ],
                        ),
                    )


                  ],
                ));
          });
        });
  }
}
