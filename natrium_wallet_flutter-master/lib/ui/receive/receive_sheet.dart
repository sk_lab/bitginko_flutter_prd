import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:natrium_wallet_flutter/themes.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheets.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/receive/share_card.dart';
import 'package:natrium_wallet_flutter/model/wallet.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../app_icons2.dart';

class AppReceiveSheet {
  AppWallet _wallet;

  GlobalKey shareCardKey;
  Widget appShareCard;
  ByteData shareImageData;
  Widget qrSVGBorder;
  Widget shareCardLogoSvg;
  Widget shareCardTickerSvg;

  Widget qrCode;
  Widget qrWidget;

  AppReceiveSheet(this.qrWidget);

  // Address copied items
  // Current state references
  bool _showShareCard;
  bool _addressCopied;
  // Timer reference so we can cancel repeated events
  Timer _addressCopiedTimer;

  Future<Uint8List> _capturePng() async {
    if (shareCardKey != null && shareCardKey.currentContext != null) {
      RenderRepaintBoundary boundary =
          shareCardKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 5.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      return byteData.buffer.asUint8List();
    } else {
      return null;
    }
  }

  mainBottomSheet(BuildContext context, {String coinSymbol = 'BTC', IconData coinIcon = AppIcons2.hkd}) {
    _wallet = StateContainer.of(context).wallet;
    // Set initial state of copy button
    _addressCopied = false;
    double devicewidth = MediaQuery.of(context).size.width;
    // Create our SVG-heavy things in the constructor because they are slower operations
    qrSVGBorder = SvgPicture.asset('assets/QR.svg');
    shareCardLogoSvg = SvgPicture.asset('assets/icon/bitginkoIcon.png'); // johnny: assets/sharecard_logo.svg
    // Share card initialization
    shareCardKey = GlobalKey();
    appShareCard = Container(
      child: AppShareCard(shareCardKey, qrSVGBorder, shareCardLogoSvg),
      alignment: AlignmentDirectional(0.0, 0.0),
    );
    qrCode = qrWidget;

    _showShareCard = false;

    AppSheets.showAppHeightEightSheet(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return SafeArea(
                minimum: 
                  EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.035),
                child: Column(
                  children: <Widget>[
                    // A row for the '存入USDT' and warning text
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        //Empty SizedBox
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                        //Container for the address text and sheet handle
                        Column(
                          children: <Widget>[
                            // Sheet handle (johnny: the little horizontal bar at top)
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 5,
                              width: MediaQuery.of(context).size.width * 0.15,
                              decoration: BoxDecoration(
                                color: StateContainer.of(context).curTheme.text10,
                                borderRadius: BorderRadius.circular(100.0),
                              ),
                            ),

                            // johnny: symbol + warning
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    '存入 '+coinSymbol, // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 22,
                                      fontWeight: FontWeight.w900,
                                      color: StateContainer.of(context).curTheme.text),
                                  ),
                                  SizedBox(
                                    width: 60,
                                    height: 10,
                                  ),
                                  Text(
                                    '只可以存入'+coinSymbol+',', // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red), 
                                  ),
                                  Text(
                                    '如存入其他代幣會造成算損失', // johnnyTranslate.
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        //Empty SizedBox
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                      ],
                    ),

                    // QR which takes all the available space left from the buttons & address text
                    Expanded(
                      child: Center(
                        child: Stack(
                          children: <Widget>[
                            _showShareCard ? appShareCard : SizedBox(),
                            // This is for hiding the share card
                            Center(
                              child: Container(
                                width: 260,
                                height: 150,
                                color: StateContainer.of(context)
                                    .curTheme
                                    .backgroundDark,
                              ),
                            ),
                            // Background/border part the QR
                            Center(
                              child: Container(
                                width: devicewidth / 1.74,
                                child: qrSVGBorder,
                              ),
                            ),
                            // Actual QR part of the QR
                            Center(
                              child: Container(
                                height: devicewidth/2.65,
                                width: devicewidth/2.65,
                                child: qrCode,
                              ),
                            ),
                            // Outer ring
                            Center(
                              child: Container(
                                width: (StateContainer.of(context).curTheme
                                        is IndiumTheme)
                                    ? devicewidth / 1.68
                                    : devicewidth / 1.6,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .primary,
                                      width: devicewidth / 115),
                                ),
                              ),
                            ),

                            // johnnytodo: re-open the follow center logo, after finish putting the center logo 

                            // Logo Background White
                            Center(
                              child: Container(
                                width: devicewidth / 8.7,
                                height: devicewidth / 8.7,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: StateContainer.of(context).curTheme.background,  
                                ),
                              ),
                            ),

                            // Logo
                            Center(
                              child: Container(
                                height: devicewidth / 10,
                                child: Icon(
                                  coinIcon, // johnny
                                  color: StateContainer.of(context).curTheme.primary, 
                                  size: 30),
                                ),
                              ),
                            
                            // johnny: below the original code
                            // Center(
                            //   child: Container(
                            //     height: devicewidth / 30,
                            //     child: AutoSizeText(
                            //       "BG", // : change it to svg or png, instead of text // original: "",
                            //       style: TextStyle(
                            //           fontFamily: "AppIcons",
                            //           color: StateContainer.of(context).curTheme.backgroundDark,
                            //           fontWeight: FontWeight.bold),
                            //       textAlign: TextAlign.center,
                            //       minFontSize: 0.1,
                            //       stepGranularity: 0.1,
                            //       maxLines: 1,
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),

                    // A row for the address text 
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        //Empty SizedBox left
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                        //Container for the address text and sheet handle
                        Column(
                          children: <Widget>[
                            // johnny: the address text
                            Container(
                              // margin: EdgeInsets.only(top: 5.0),
                              child: UIUtil.twoLineAddressText(
                                  context, 
                                  _wallet.address, 
                                  type: TwoLineAddressTextType.PRIMARY),
                            ),
                          ],
                        ),
                        //Empty SizedBox right
                        SizedBox(
                          width: 60,
                          height: 60,
                        ),
                      ],
                    ),

                    //A column with Copy Address and Share Address buttons
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            AppButton.buildAppButton(
                                context,
                                // Share Address Button
                                _addressCopied
                                    ? AppButtonType.SUCCESS
                                    : AppButtonType.PRIMARY,
                                _addressCopied
                                    ? AppLocalization.of(context).addressCopied
                                    : AppLocalization.of(context).copyAddress,
                                Dimens.BUTTON_TOP_DIMENS, onPressed: () {
                              Clipboard.setData(
                                  new ClipboardData(text: _wallet.address));
                              setState(() {
                                // Set copied style
                                _addressCopied = true;
                              });
                              if (_addressCopiedTimer != null) {
                                _addressCopiedTimer.cancel();
                              }
                              _addressCopiedTimer = new Timer(
                                  const Duration(milliseconds: 800), () {
                                setState(() {
                                  _addressCopied = false;
                                });
                              });
                            }),
                          ],
                        ),
                        // Row(
                        //   children: <Widget>[
                        //     AppButton.buildAppButton(
                        //         context,
                        //         // Share Address Button
                        //         AppButtonType.PRIMARY_OUTLINE,
                        //         AppLocalization.of(context).addressShare,
                        //         Dimens.BUTTON_BOTTOM_DIMENS,
                        //         disabled: _showShareCard, onPressed: () {
                        //       String receiveCardFileName =
                        //           "share_${StateContainer.of(context).wallet.address}.png";
                        //       getApplicationDocumentsDirectory()
                        //           .then((directory) {
                        //         String filePath =
                        //             "${directory.path}/$receiveCardFileName";
                        //         File f = File(filePath);
                        //         setState(() {
                        //           _showShareCard = true;
                        //         });
                        //         Future.delayed(new Duration(milliseconds: 50),
                        //             () {
                        //           if (_showShareCard) {
                        //             _capturePng().then((byteData) {
                        //               if (byteData != null) {
                        //                 f.writeAsBytes(byteData).then((file) {
                        //                   UIUtil.cancelLockEvent();
                        //                   Share.shareFile(file,
                        //                       text: StateContainer.of(context)
                        //                           .wallet
                        //                           .address);
                        //                 });
                        //               } else {
                        //                 // TODO - show a something went wrong message
                        //               }
                        //               setState(() {
                        //                 _showShareCard = false;
                        //               });
                        //             });
                        //           }
                        //         });
                        //       });
                        //     }),
                        //   ],
                        // ),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
