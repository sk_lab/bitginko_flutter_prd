import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:manta_dart/manta_wallet.dart';
import 'package:manta_dart/messages.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/model/address.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/model/db/contact.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/ui/trade/trade_confirm_sheet.dart';
import 'package:natrium_wallet_flutter/ui/trade/trade_sheet.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/util/hapticutil.dart';
import 'package:natrium_wallet_flutter/util/manta.dart';
import 'package:natrium_wallet_flutter/util/user_data_util.dart';

// johnny: copy and edit from "AppPopupButton" (popup_button.dart)
class AppTradeButton extends StatefulWidget {
  String tradeMode;
  CoinData coinData;
  String hkdPrice;
  
  AppTradeButton({this.tradeMode, this.coinData, this.hkdPrice});

  @override
  _AppTradeButtonState createState() => _AppTradeButtonState();
}

class _AppTradeButtonState extends State<AppTradeButton> {
  // double scanButtonSize = 0;
  double popupMarginBottom = 0;
  bool isScrolledUpEnough = false;
  bool firstTime = true;
  bool isTradeButtonColorPrimary = true;
  Color popupColor = Colors.transparent;

  bool animationOpen;

  void _showMantaAnimation() {
    animationOpen = true;
    Navigator.of(context).push(
        AnimationLoadingOverlay(
            AnimationType.MANTA,
            StateContainer.of(context).curTheme.animationOverlayStrong,
            StateContainer.of(context).curTheme.animationOverlayMedium,
            onPoppedCallback: () => animationOpen = false));
  }

  @override
  void initState() {
    super.initState();
    animationOpen = false;
  }

  

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        // Hero(
        //   tag: 'scanButton',
        //   child: AnimatedContainer(
        //     duration: Duration(milliseconds: 100),
        //     curve: Curves.easeOut,
        //     height: scanButtonSize,
        //     width: scanButtonSize,
        //     decoration: BoxDecoration(
        //       color: popupColor,
        //       borderRadius: BorderRadius.circular(100),
        //     ),
        //     child: Icon(
        //       AppIcons.scan,
        //       size: scanButtonSize < 60 ? scanButtonSize / 1.8 : 33,
        //       color: StateContainer.of(context).curTheme.background,
        //     ),
        //   ),
        // ),
        // Trade Button
        GestureDetector(
          onVerticalDragStart: (StateContainer.of(context).wallet != null &&
                  StateContainer.of(context).wallet.accountBalance >
                      BigInt.zero)
              ? (value) {
                  setState(() {
                    popupColor = StateContainer.of(context).curTheme.primary;
                  });
                }
              : (value) {},
          onVerticalDragEnd: (StateContainer.of(context).wallet != null &&
                  StateContainer.of(context).wallet.accountBalance >
                      BigInt.zero)
              ? (value) {
                  isTradeButtonColorPrimary = true;
                  firstTime = true;
                  if (isScrolledUpEnough) {
                    setState(() {
                      popupColor = Colors.white;
                    });
                    
                  }
                  isScrolledUpEnough = false;
                  setState(() {
                    // scanButtonSize = 0;
                  });
                }
              : (value) {},
          onVerticalDragUpdate: (StateContainer.of(context).wallet != null &&
                  StateContainer.of(context).wallet.accountBalance >
                      BigInt.zero)
              ? (dragUpdateDetails) {
                  if (dragUpdateDetails.localPosition.dy < -60) {
                    isScrolledUpEnough = true;
                    if (firstTime) {
                      sl.get<HapticUtil>().success();
                    }
                    firstTime = false;
                    setState(() {
                      popupColor = StateContainer.of(context).curTheme.success;
                      isTradeButtonColorPrimary = true;
                    });
                  } else {
                    isScrolledUpEnough = false;
                    popupColor = StateContainer.of(context).curTheme.primary;
                    isTradeButtonColorPrimary = false;
                  }
                  // Swiping below the starting limit
                  if (dragUpdateDetails.localPosition.dy >= 0) {
                    setState(() {
                      // scanButtonSize = 0;
                      popupMarginBottom = 0;
                    });
                  } else if (dragUpdateDetails.localPosition.dy > -60) {
                    setState(() {
                      // scanButtonSize = dragUpdateDetails.localPosition.dy * -1;
                      popupMarginBottom = 5; // + scanButtonSize / 3;
                    });
                  } else {
                    setState(() {
                      // scanButtonSize = 60 +
                      //     ((dragUpdateDetails.localPosition.dy * -1) - 60) / 30;
                      // popupMarginBottom = 5 + scanButtonSize / 3;
                    });
                  }
                }
              : (dragUpdateDetails) {},
          child: AnimatedContainer(
            duration: Duration(milliseconds: 100),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              boxShadow: [StateContainer.of(context).curTheme.boxShadowButton],
            ),
            height: 40,
            width: (MediaQuery.of(context).size.width - 64) / 2,
            // margin: EdgeInsetsDirectional.only(
            //     start: 7, top: popupMarginBottom, end: 14.0),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              color: widget.tradeMode == "BUY" ? Colors.greenAccent : Colors.redAccent, 
              // color: StateContainer.of(context).wallet != null &&
              //         StateContainer.of(context).wallet.accountBalance >
              //             BigInt.zero
              //     ? isTradeButtonColorPrimary
              //         ? StateContainer.of(context).curTheme.primary
              //         : StateContainer.of(context).curTheme.success
              //     : StateContainer.of(context).curTheme.primary60,
              child: AutoSizeText(
                widget.tradeMode == "BUY" ? "買入" : "賣出", // johnnyTranslate: //AppLocalization.of(context).trade,
                textAlign: TextAlign.center,
                style: AppStyles.textStyleButtonPrimary(context),
                maxLines: 1,
                stepGranularity: 0.5,
              ),
              onPressed: () {
                print(widget.coinData.coinTicker);
                if (StateContainer.of(context).wallet != null 
                    && StateContainer.of(context).wallet.accountBalance > BigInt.zero 
                    && widget.hkdPrice != "0.00" 
                ) {
                  print("widget.hkdPrice: " + widget.hkdPrice);
                  Sheets.showAppHeightNineSheet(
                      context: context,
                      widget: TradeSheet(
                          localCurrency: StateContainer.of(context).curCurrency,
                          tradeMode: widget.tradeMode,
                          coinData: widget.coinData,
                          hkdPrice: widget.hkdPrice));
                }
              },
              highlightColor: StateContainer.of(context).wallet != null &&
                      StateContainer.of(context).wallet.accountBalance >
                          BigInt.zero
                  ? StateContainer.of(context).curTheme.background40
                  : Colors.transparent,
              splashColor: StateContainer.of(context).wallet != null &&
                      StateContainer.of(context).wallet.accountBalance >
                          BigInt.zero
                  ? StateContainer.of(context).curTheme.background40
                  : Colors.transparent,
            ),
          ),
        ),
      ],
    );
  }
}
