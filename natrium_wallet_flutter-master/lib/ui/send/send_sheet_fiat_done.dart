import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:manta_dart/messages.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';

// johnny: step2 of sending fiat
class SendFiatCompleteSheet extends StatefulWidget {
  final String amountRaw;
  // final String destination;
  final String coinSymbol;
  // final String contactName; // johnny
  // final String localAmount; // johnny
  // final PaymentRequestMessage paymentRequest; // johnny

  SendFiatCompleteSheet({this.amountRaw, this.coinSymbol}) : super();  

  _SendFiatCompleteSheetState createState() => _SendFiatCompleteSheetState();
}

class _SendFiatCompleteSheetState extends State<SendFiatCompleteSheet> {
  String amount;
  // String destinationAltered;
  bool isMantaTransaction;

  @override
  void initState() {
    super.initState();
    // Indicate that this is a special amount if some digits are not displayed
    if (NumberUtil.getRawAsUsableString(widget.amountRaw).replaceAll(",", "") ==
        NumberUtil.getRawAsUsableDecimal(widget.amountRaw).toString()) {
      amount = NumberUtil.getRawAsUsableString(widget.amountRaw);
    } else {
      amount = NumberUtil.truncateDecimal(
                  NumberUtil.getRawAsUsableDecimal(widget.amountRaw),
                  digits: 6)
              .toStringAsFixed(6) +
          "~";
    }

    // destinationAltered = widget.destination; // .replaceAll("xrb_", "nano_");
    isMantaTransaction = false; // widget.paymentRequest != null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        minimum: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.035),
        child: Column(
          children: <Widget>[
            // Sheet handle
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 5,
              width: MediaQuery.of(context).size.width * 0.15,
              decoration: BoxDecoration(
                color: StateContainer.of(context).curTheme.text10,
                borderRadius: BorderRadius.circular(100.0),
              ),
            ),
            //A main container that holds the texts
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Success tick (icon)
                  Container(
                    alignment: AlignmentDirectional(0, 0),
                    margin: EdgeInsets.only(bottom: 25),
                    child: Icon(AppIcons.success,
                        size: 100,
                        color: StateContainer.of(context)
                            .curTheme.success),
                  ),

                  // Container for the text
                  Container(
                    margin: EdgeInsets.only(top: 30.0, bottom: 10, left: 20, right: 20),
                    child: Column(
                      children: <Widget>[
                        // text
                        Text(
                          // CaseChange.toUpperCase(AppLocalization.of(context).sentTo,context),
                          "提款申請已提交!", // johnnyTranslate // "Withdraw Request has Been Submitted"
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 28.0,
                            fontWeight: FontWeight.w700,
                            color: StateContainer.of(context)
                                .curTheme
                                .success,
                            fontFamily: 'NunitoSans',
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),

            // CLOSE Button
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      AppButton.buildAppButton(
                          context,
                          AppButtonType.SUCCESS_OUTLINE,
                          CaseChange.toUpperCase(AppLocalization.of(context).close,context),
                          Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                            Navigator.of(context).pop();
                      }),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      );    
  }
}

