
import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:decimal/decimal.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:manta_dart/manta_wallet.dart';
import 'package:manta_dart/messages.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/model/authentication_method.dart';
import 'package:natrium_wallet_flutter/model/available_currency.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/model/address.dart';
import 'package:natrium_wallet_flutter/model/db/contact.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/ui/send/send_confirm_sheet.dart';
import 'package:natrium_wallet_flutter/ui/send/send_sheet_fiat_done.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/widgets/one_or_three_address_text.dart';
import 'package:natrium_wallet_flutter/ui/util/formatters.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/util/biometrics.dart';
import 'package:natrium_wallet_flutter/util/hapticutil.dart';
import 'package:natrium_wallet_flutter/util/manta.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'package:natrium_wallet_flutter/util/user_data_util.dart';

// johnny: copy and modified based on 'send_sheet.dart'
// johnny: step1 of sending fiat
class SendSheetFiat extends StatefulWidget {
  final AvailableCurrency localCurrency;
  // final Contact contact;
  final String address;
  final String quickSendAmount;
  final CoinData coinData; // johnny

  SendSheetFiat(
      {@required this.localCurrency,
      // this.contact,
      this.address,
      this.quickSendAmount,
      this.coinData})
      : super();

  _SendSheetFiatState createState() => _SendSheetFiatState();
}

enum AddressStyle { TEXT60, TEXT90, PRIMARY }

class _SendSheetFiatState extends State<SendSheetFiat> {
  final Logger log = sl.get<Logger>();

  
  // 1
  FocusNode _sendAmountFocusNode;
  TextEditingController _sendAmountController;
  // 2
  FocusNode _sendBankFocusNode;
  TextEditingController _sendBankController;
  // 3
  FocusNode _sendFullnameFocusNode;
  TextEditingController _sendFullnameController;
  // 4
  FocusNode _sendAcctNumFocusNode;
  TextEditingController _sendAcctNumController;

  // States
  AddressStyle _sendBankStyle;
  AddressStyle _sendFullnameStyle;
  AddressStyle _sendAcctNumStyle;

  String _amountHint = "";
  String _bankHint = "";
  String _fullnameHint = "";
  String _acctNumHint = "";

  String _amountValidationText = "";
  String _bankValidationText = "";
  String _fullnameValidationText = "";
  String _acctNumValidationText = "";

  String quickSendAmount;
  String avaCoinBalance;

  // List<Contact> _contacts; // johnny: remove contact logic

  bool animationOpen;
  // Used to replace address textfield with colorized TextSpan
  bool _bankValidAndUnfocused = false;
  bool _fullnameValidAndUnfocused = false;
  bool _acctNumValidAndUnfocused = false;

  // Set to true when a contact is being entered 
  // bool _isContact = false;

  // Buttons States (Used because we hide the buttons under certain conditions)
  bool _pasteButtonVisible = true;
  bool _pasteButtonVisible2 = true;
  bool _pasteButtonVisible3 = true;

  // johnny: remove conversion logic
  // Local currency mode/fiat conversion 
  bool _localCurrencyMode = false;
  String _lastLocalCurrencyAmount = "";
  String _lastCryptoAmount = "";
  NumberFormat _localCurrencyFormat;

  String _rawAmount;

  @override
  void initState() {
    super.initState();

    _sendAmountFocusNode = FocusNode();
    _sendBankFocusNode = FocusNode();
    _sendFullnameFocusNode = FocusNode();
    _sendAcctNumFocusNode = FocusNode();

    _sendAmountController = TextEditingController();
    _sendBankController = TextEditingController();
    _sendFullnameController = TextEditingController();
    _sendAcctNumController = TextEditingController();
    
    _sendBankStyle = AddressStyle.TEXT60;
    _sendFullnameStyle = AddressStyle.TEXT60;
    _sendAcctNumStyle = AddressStyle.TEXT60;
    
    quickSendAmount = widget.quickSendAmount;
    avaCoinBalance = widget.coinData.avaBalance;
    this.animationOpen = false;

    // On amount focus change
    _sendAmountFocusNode.addListener(() {
      if (_sendAmountFocusNode.hasFocus) {
        if (_rawAmount != null) {
          setState(() {
            _sendAmountController.text =
                NumberUtil.getRawAsUsableString(_rawAmount).replaceAll(",", "");
            _rawAmount = null;
          });
        }
        if (quickSendAmount != null) {
          _sendAmountController.text = "";
          setState(() {
            quickSendAmount = null;
          });
        }
        setState(() {
          _amountHint = null;
        });
      } else {
        setState(() {
          _amountHint = "";
        });
      }
    });
    // On Bank focus change
    _sendBankFocusNode.addListener(() {
      if (_sendBankFocusNode.hasFocus) {
        setState(() {
          _bankHint = null;
          _bankValidAndUnfocused = false;
        });
        _sendBankController.selection = TextSelection.fromPosition(
            TextPosition(offset: _sendBankController.text.length));
      } else {
        setState(() {
          _bankHint = "";
          if (Address(_sendBankController.text).isValid()) {
            _bankValidAndUnfocused = true;
          }
        });
      }
    });
    // On account name / fullname focus change
    _sendFullnameFocusNode.addListener(() {
      if (_sendFullnameFocusNode.hasFocus) {
        setState(() {
          _fullnameHint = null;
          _fullnameValidAndUnfocused = false;
        });
        _sendFullnameController.selection = TextSelection.fromPosition(
            TextPosition(offset: _sendFullnameController.text.length));
      } else {
        setState(() {
          _fullnameHint = "";
          if (Address(_sendFullnameController.text).isValid()) {
            _fullnameValidAndUnfocused = true;
          }
        });
      }
    });
    // on account num focus change
    _sendAcctNumFocusNode.addListener(() {
      if (_sendAcctNumFocusNode.hasFocus) {
        setState(() {
          _acctNumHint = null;
          _acctNumValidAndUnfocused = false;
        });
        _sendAcctNumController.selection = TextSelection.fromPosition(
            TextPosition(offset: _sendAcctNumController.text.length));
      } else {
        setState(() {
          _acctNumHint = "";
          if (Address(_sendAcctNumController.text).isValid()) {
            _acctNumValidAndUnfocused = true;
          }
        });
      }
    });

    // Set initial currency format
    _localCurrencyFormat = NumberFormat.currency(
        locale: widget.localCurrency.getLocale().toString(),
        symbol: widget.localCurrency.getCurrencySymbol());
    // Set quick send amount
    if (quickSendAmount != null) {
      _sendAmountController.text =
          NumberUtil.getRawAsUsableString(quickSendAmount).replaceAll(",", "");
    }
  }

  void _showMantaAnimation() {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.MANTA,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }

  @override
  Widget build(BuildContext context) {
    print('build send_sheet =========');
    // The main column that holds everything
    return SafeArea(
        minimum:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.035),
        child: Column(
          children: <Widget>[
            // A row for the header of the sheet
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Empty SizedBox
                SizedBox(
                  width: 60,
                  height: 60,
                ),

                // Container for the header, address and balance text
                Column(
                  children: <Widget>[
                    // Sheet handle
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 5,
                      width: MediaQuery.of(context).size.width * 0.15,
                      decoration: BoxDecoration(
                        color: StateContainer.of(context).curTheme.text10,
                        borderRadius: BorderRadius.circular(100.0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15.0),
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width - 140),
                      child: Column(
                        children: <Widget>[
                          // Header
                          AutoSizeText(
                            // CaseChange.toUpperCase(
                            //     AppLocalization.of(context).sendFrom, context),
                            '港元提款', //+ widget.coinData.coinTicker, // johnnyTranslate 
                            style: AppStyles.textStyleHeader(context),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            stepGranularity: 0.1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //Empty SizedBox
                SizedBox(
                  width: 60,
                  height: 60,
                ),
              ],
            ),

            // text of warning
            Container(
              margin: EdgeInsets.only(top: 10.0, left: 30, right: 30, bottom: 10),
              child: Container(
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan( 
                    text: '',
                    children: [
                      TextSpan(
                        text: '(港元提款需收取銀行手續費 港幣\$50元)', // johnnyTranslate //StateContainer.of(context).selectedAccount.name,
                        style: TextStyle(
                          color: StateContainer.of(context).curTheme.text60,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'NunitoSans',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),


            // text of ava. balance
            Container(
              margin: EdgeInsets.only(top: 10.0, left: 30, right: 30),
              child: Container(
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan( 
                    text: '',
                    children: [
                      TextSpan(
                        text: '港元餘額', // johnnyTranslate //StateContainer.of(context).selectedAccount.name,
                        style: TextStyle(
                          color: StateContainer.of(context).curTheme.text60,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'NunitoSans',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            
            // A main container that holds everything
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                child: Stack(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Clear focus of our fields when tapped in this empty space
                        _sendAmountFocusNode.unfocus();
                        _sendBankFocusNode.unfocus();
                        _sendFullnameFocusNode.unfocus();
                        _sendAcctNumFocusNode.unfocus();
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: SizedBox.expand(),
                        constraints: BoxConstraints.expand(),
                      ),
                    ),
                    // A column for all inputs
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        // Stack(
                        //   children: <Widget>[

                          // #1: Column for Balance Text + Enter Amount container + Enter Amount Error container
                          Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
                              child: Column(
                                children: <Widget>[

                                  Container(
                                    child: RichText(
                                      textAlign: TextAlign.start,
                                      text: TextSpan(
                                        text: '',
                                        children: [
                                          TextSpan(
                                            text: widget.coinData.avaBalance,
                                            style: TextStyle(
                                              color: StateContainer.of(context).curTheme.primary60,
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.w700,
                                              fontFamily: 'NunitoSans',
                                            ),
                                          ),
                                          TextSpan(
                                            text: ' ' + widget.coinData.coinTicker, 
                                            style: TextStyle(
                                              color: StateContainer.of(context).curTheme.primary60,
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.w700,
                                              fontFamily: 'NunitoSans',
                                            ),
                                          ), 
                                        ],
                                      ),
                                    ),
                                  ),
                                
                                  // ******* Enter Amount Container ******* //
                                  getEnterAmountContainer(),
                                  // ******* Enter Amount Container End ******* //

                                  // ******* Enter Amount Error Container ******* //
                                  Container(
                                    // decoration: BoxDecoration(border: Border.all(color: Colors.yellow)), // for debug
                                    alignment: AlignmentDirectional(0, 0),
                                    margin: EdgeInsets.only(top: 3),
                                    child: Text(_amountValidationText,
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.red, // StateContainer.of(context).curTheme.primary,
                                          fontFamily: 'NunitoSans',
                                          fontWeight: FontWeight.w600,
                                        )),
                                  ),
                                  // ******* Enter Amount Error Container End ******* //
                                ],
                            ),

                          ),

                          // #2: lower part column of 3 input textfields
                          Container( 
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start, // even 3 input textfields
                                children: <Widget>[

                                    // #1.1: Column for Enter Bank container + Enter Bank Error container
                                    bankContainer(),

                                    // #1.2: Column for Enter Fullname container + Enter Fullname Error container
                                    fullnameContainer(),

                                    // #1.3: Column for Enter AcctNum container + Enter AcctNum Error container
                                    acctNumContainer(),

                                ]
                              )
                          ),


                        //   ],
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
            ),

            //A column with "Send" buttons
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      // Send Button
                      AppButton.buildAppButton(
                          context,
                          AppButtonType.PRIMARY,
                          "提交", // johnnyTranslate // AppLocalization.of(context).send,
                          Dimens.BUTTON_TOP_DIMENS, onPressed: () async {

                            bool validRequest = _validateRequest();
                            if (validRequest) {

                              // johnny: replace this with the following code to apply bio-auth
                              _showSendingAnimation(context);
                              bool submitted = await _postWithdrawRequest();
                              if (submitted) {
                                // johnny: directly go to complete sheet (skip confirm sheet)
                                Sheets.showAppHeightNineSheet(
                                    context: context,
                                    closeOnTap: true,
                                    removeUntilHome: true,
                                    widget: SendFiatCompleteSheet(
                                        amountRaw: _sendAmountController.text.replaceAll(",", ""),
                                        coinSymbol: "HKD",
                                        )); 
                                
                                StateContainer.of(context).getUserBalance(); // johnny: update balance afterward
                              } else {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                                // johnnytodo: add error notification at top, when API return error or fail
                              }

                              // Authenticate. johnnytodo: add back bio-authenticate later
                              // AuthenticationMethod authMethod = await sl.get<SharedPrefsUtil>().getAuthMethod();
                              // bool hasBiometrics = await sl.get<BiometricUtil>().hasBiometrics();

                              // if (authMethod.method == AuthMethod.BIOMETRICS && hasBiometrics) {
                              //   try {
                              //       bool authenticated = await sl.get<BiometricUtil>()
                              //           .authenticateWithBiometrics(context, 
                              //             "Withdraw " + _sendAmountController.text + " HKD");
                                    
                              //       if (authenticated) {
                              //         sl.get<HapticUtil>().fingerprintSucess();

                              //         _showSendingAnimation(context);
                              //         bool submitted = await _postWithdrawRequest();
                              //         if (submitted) {
                              //           // johnny: directly go to complete sheet (skip confirm sheet)
                              //           Sheets.showAppHeightNineSheet(
                              //               context: context,
                              //               closeOnTap: true,
                              //               removeUntilHome: true,
                              //               widget: SendFiatCompleteSheet(
                              //                   amountRaw: _sendAmountController.text.replaceAll(",", ""),
                              //                   coinSymbol: "HKD",
                              //                   )); 
                              //         } else {
                              //           Navigator.of(context).pop();
                              //           Navigator.of(context).pop();
                              //           // johnnytodo: add error notification at top, when API return error or fail
                              //         }

                              //         // johnnytodo: refresh the wallet page to show new balance after this
                              //       }

                              //   } catch (e) {
                              //     // await authenticateWithPin(); // johnnytodo: fix this
                              //   }

                              // } else {
                              //   // await authenticateWithPin();
                              // }
                              
                            }

                          }),
                    ],
                  ),

                ],
              ),
            ),
          ],
        ));
  }

  void _showSendingAnimation(BuildContext context) {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.SEND,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }

  Future<bool> _postWithdrawRequest() async {

    var tokenData = {
      'data': {
        'token':            StateContainer.of(context).wallet.loginToken,
        'pid' :             StateContainer.of(context).wallet.pid, // uid
        'in_client':        StateContainer.of(context).wallet.uid, // uid
        'in_coin_id':       widget.coinData.coinId,
        'coin_ticker':      widget.coinData.coinTicker,
        'in_withdraw_amt':  _sendAmountController.text,
        'in_bank_name':     _sendBankController.text,
        'in_bank_acc_name': _sendFullnameController.text,
        'in_bank_numer':    _sendAcctNumController.text,
      }
    };

    Response response;
    Dio dio2 = new Dio();
    response = await dio2.post(
        // 'https://uat-bitginko.standardkepler.com/api/v4/uri/postWithDrawRequest.api.php', // UAT
        'https://bitginko.com/api/v4/uri/mobile.postWithDrawRequest.api.php', // PRD
        data: FormData.fromMap(tokenData),
        options: Options(responseType: ResponseType.plain,));

    var returnObj = jsonDecode(response.toString());
    print(returnObj);
    // print("Success post withdraw request");

    return true; // johnnytodo: see if need bool here later
  }

  /// Validate form data to see if valid
  /// @returns true if valid, false otherwise
  bool _validateRequest() {
    bool isValid = true;
    _sendAmountFocusNode.unfocus();
    _sendBankFocusNode.unfocus();
    _sendFullnameFocusNode.unfocus();
    _sendAcctNumFocusNode.unfocus();

    // Validate amount
    if (_sendAmountController.text.trim().isEmpty) {
      isValid = false;
      setState(() {
        _amountValidationText = AppLocalization.of(context).amountMissing;
      });
    } else if (_sendBankController.text.trim().isEmpty) {
      isValid = false;
      setState(() {
        _bankValidationText = "銀行名稱為空"; // johnnyTranslate //AppLocalization.of(context).addressMising;
      });
    } else if (_sendFullnameController.text.trim().isEmpty) {
      isValid = false;
      setState(() {
        _bankValidationText = "收款人名稱為空"; // johnnyTranslate/ /AppLocalization.of(context).addressMising;
      });
    } else if (_sendAcctNumController.text.trim().isEmpty) {
      isValid = false;
      setState(() {
        _bankValidationText = "戶口號碼為空"; // johnnyTranslate //AppLocalization.of(context).addressMising;
      });
    } else {

      Decimal avaBalance = Decimal.tryParse(avaCoinBalance.replaceAll(',', '')); //johnny:  get coin balance //StateContainer.of(context).wallet.accountBalance;
      Decimal sendAmount = Decimal.tryParse(_sendAmountController.text);
      print(avaBalance);
      print(sendAmount);
      if (sendAmount == null || sendAmount == 0) {
        isValid = false;
        setState(() {
          _amountValidationText = AppLocalization.of(context).amountMissing;
        });
      } else if (sendAmount > avaBalance) {
        isValid = false;
        setState(() {
          _amountValidationText =
              AppLocalization.of(context).insufficientBalance;
        });
      }
    }
    
    return isValid;
  }

  Widget bankContainer() {
      // #1.1: Column for Enter Bank container + Enter Bank Error container
      return Container(
        // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
        margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            // ******* Enter Bank Container ******* //
            AppTextField(
              focusNode: _sendBankFocusNode,
              controller: _sendBankController,
              topMargin: 5,
              cursorColor: StateContainer.of(context).curTheme.primary,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16.0,
                color: StateContainer.of(context).curTheme.primary,
                fontFamily: 'NunitoSans',
              ),
              inputFormatters: [LengthLimitingTextInputFormatter(50)],
              onChanged: (text) {
                // Always reset the error message to be less annoying
                setState(() {
                  _bankValidationText = "";
                  // Reset the raw amount
                  _rawAmount = null;
                });
              },
              textInputAction: TextInputAction.next,
              maxLines: null,
              autocorrect: false,
              hintText:
                  _bankHint == null ? "" : "香港本地銀行名稱", // AppLocalization.of(context).enterAmount, // johnnyTranslate
              suffixButton: TextFieldButton( 
                icon: AppIcons.paste,
                onPressed: () {

                  // johnnyfuture: for paste button here
                  // Clipboard.getData("text/plain").then((ClipboardData data) {
                  //   if (data == null || data.text == null || data.text.length > 55) {
                  //     return;
                  //   }

                  //   setState(() {
                  //     _bankValidationText = "";
                  //     _sendBankStyle = AddressStyle.TEXT90;
                  //     // _pasteButtonVisible = false;
                  //   });
                  //   _sendBankController.text = data.text;
                  //   _sendBankFocusNode.unfocus();
                  //   setState(() {
                  //     _bankValidAndUnfocused = true;
                  //   });

                  // });


                },
              ),
              fadeSuffixOnCondition: true,
              suffixShowFirstCondition: false,
              // keyboardType: TextInputType.numberWithOptions(decimal: true),
              textAlign: TextAlign.center,
              onSubmitted: (text) {
                FocusScope.of(context).unfocus();
                if (!Address(_sendBankController.text).isValid()) {
                  FocusScope.of(context).requestFocus(_sendBankFocusNode);
                }
              },
            ),
            // ******* Enter Bank Container End ******* //

            // ******* Enter Bank Error Container ******* //
            Container(
              alignment: AlignmentDirectional(0, 0),
              margin: EdgeInsets.only(top: 2),
              child: Text(_bankValidationText,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.red, // StateContainer.of(context).curTheme.primary,
                    fontFamily: 'NunitoSans',
                    fontWeight: FontWeight.w600,
                  )),
            ),
            // ******* Enter Bank Error Container End ******* //
          ],
        ),
      );
  }

  Widget fullnameContainer() {
    return Container(
        // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
        margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
          
            // ******* Enter Fullname Container ******* //
            AppTextField(
              focusNode: _sendFullnameFocusNode,
              controller: _sendFullnameController,
              topMargin: 5,
              cursorColor: StateContainer.of(context).curTheme.primary,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16.0,
                color: StateContainer.of(context).curTheme.primary,
                fontFamily: 'NunitoSans',
              ),
              inputFormatters: [LengthLimitingTextInputFormatter(50)],
              onChanged: (text) {
                // Always reset the error message to be less annoying
                setState(() {
                  _fullnameValidationText = "";
                  // Reset the raw amount
                  _rawAmount = null;
                });
              },
              textInputAction: TextInputAction.next,
              maxLines: null,
              autocorrect: false,
              hintText:
                  _fullnameHint == null ? "" : "收款人名稱", // AppLocalization.of(context).enterAmount,  // johnnyTranslate
              suffixButton: TextFieldButton( // Max button
                icon: AppIcons.paste,
                onPressed: () {
                  // johnnyfuture: for paste button here
                  // Clipboard.getData("text/plain").then((ClipboardData data) {
                  //   if (data == null || data.text == null || data.text.length > 55) {
                  //     return;
                  //   }
                    
                  //   setState(() {
                  //     _fullnameValidationText = "";
                  //     _sendFullnameStyle = AddressStyle.TEXT90;
                  //     // _pasteButtonVisible = false;
                  //   });
                  //   _sendFullnameController.text = data.text;
                  //   _sendFullnameFocusNode.unfocus();
                  //   setState(() {
                  //     _fullnameValidAndUnfocused = true;
                  //   });

                  // });
                },
              ),
              fadeSuffixOnCondition: true,
              suffixShowFirstCondition: false,
              // keyboardType: TextInputType.numberWithOptions(decimal: true),
              textAlign: TextAlign.center,
              onSubmitted: (text) {
                FocusScope.of(context).unfocus();
                if (!Address(_sendFullnameController.text).isValid()) {
                  FocusScope.of(context).requestFocus(_sendFullnameFocusNode);
                }
              },
            ),
            // ******* Enter Fullname Container End ******* //

            // ******* Enter Fullname Error Container ******* //
            Container(
              // decoration: BoxDecoration(border: Border.all(color: Colors.yellow)), // for debug
              alignment: AlignmentDirectional(0, 0),
              margin: EdgeInsets.only(top: 2),
              child: Text(_fullnameValidationText,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.red, // StateContainer.of(context).curTheme.primary,
                    fontFamily: 'NunitoSans',
                    fontWeight: FontWeight.w600,
                  )),
            ),
            // ******* Enter Fullname Error Container End ******* //
          ],
        ),
      );
  }

  Widget acctNumContainer() {
    return Container(
        // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
        margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
          
            // ******* Enter AcctNum Container ******* //
            AppTextField(
              focusNode: _sendAcctNumFocusNode,
              controller: _sendAcctNumController,
              topMargin: 5,
              cursorColor: StateContainer.of(context).curTheme.primary,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16.0,
                color: StateContainer.of(context).curTheme.primary,
                fontFamily: 'NunitoSans',
              ),
              inputFormatters: [LengthLimitingTextInputFormatter(20)],
              onChanged: (text) {
                // Always reset the error message to be less annoying
                setState(() {
                  _acctNumValidationText = "";
                  // Reset the raw amount
                  _rawAmount = null;
                });
              },
              textInputAction: TextInputAction.next,
              maxLines: null,
              autocorrect: false,
              hintText:
                  _acctNumHint == null ? "" : "戶口號碼", // AppLocalization.of(context).enterAmount, // johnnyTranslate
              suffixButton: TextFieldButton( // Max button
                icon: AppIcons.paste,
                onPressed: () {
                  // johnnyfuture: for paste button here
                },
              ),
              fadeSuffixOnCondition: true,
              suffixShowFirstCondition: false,
              // keyboardType: TextInputType.numberWithOptions(decimal: true),
              textAlign: TextAlign.center,
              onSubmitted: (text) {
                FocusScope.of(context).unfocus();
                if (!Address(_sendAcctNumController.text).isValid()) {
                  FocusScope.of(context).requestFocus(_sendAcctNumFocusNode);
                }
              },
            ),
            // ******* Enter AcctNum Container End ******* //

            // ******* Enter AcctNum Error Container ******* //
            Container(
              alignment: AlignmentDirectional(0, 0),
              margin: EdgeInsets.only(top: 2),
              child: Text(_acctNumValidationText,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.red, // StateContainer.of(context).curTheme.primary,
                    fontFamily: 'NunitoSans',
                    fontWeight: FontWeight.w600,
                  )),
            ),
            // ******* Enter AcctNum Error Container End ******* //
          ],
        ),
      );
  }

  

  String _convertLocalCurrencyToCrypto() {
    String convertedAmt = _sendAmountController.text.replaceAll(",", ".");
    convertedAmt = NumberUtil.sanitizeNumber(convertedAmt);
    if (convertedAmt.isEmpty) {
      return "";
    }
    Decimal valueLocal = Decimal.parse(convertedAmt);
    Decimal conversion = Decimal.parse(
        StateContainer.of(context).wallet.localCurrencyConversion);
    return NumberUtil.truncateDecimal(valueLocal / conversion).toString();
  }

  String _convertCryptoToLocalCurrency() {
    String convertedAmt = NumberUtil.sanitizeNumber(_sendAmountController.text,
        maxDecimalDigits: 2);
    if (convertedAmt.isEmpty) {
      return "";
    }
    Decimal valueCrypto = Decimal.parse(convertedAmt);
    Decimal conversion = Decimal.parse(
        StateContainer.of(context).wallet.localCurrencyConversion);
    convertedAmt =
        NumberUtil.truncateDecimal(valueCrypto * conversion, digits: 2)
            .toString();
    convertedAmt =
        convertedAmt.replaceAll(".", _localCurrencyFormat.symbols.DECIMAL_SEP);
    convertedAmt = _localCurrencyFormat.currencySymbol + convertedAmt;
    return convertedAmt;
  }

  // Determine if this is a max send or not by comparing balances
  bool _isMaxSend() {
    // Sanitize commas
    if (_sendAmountController.text.isEmpty) {
      return false;
    }
    try {
      
      return _sendAmountController.text == widget.coinData.avaBalance; // johnny

    } catch (e) {
      return false;
    }
  }

  void toggleLocalCurrency() {
    // Keep a cache of previous amounts because, it's kinda nice to see approx what nano is worth
    // this way you can tap button and tap back and not end up with X.9993451 NANO
    if (_localCurrencyMode) {
      // Switching to crypto-mode
      String cryptoAmountStr;
      // Check out previous state
      if (_sendAmountController.text == _lastLocalCurrencyAmount) {
        cryptoAmountStr = _lastCryptoAmount;
      } else {
        _lastLocalCurrencyAmount = _sendAmountController.text;
        _lastCryptoAmount = _convertLocalCurrencyToCrypto();
        cryptoAmountStr = _lastCryptoAmount;
      }
      setState(() {
        _localCurrencyMode = false;
      });
      Future.delayed(Duration(milliseconds: 50), () {
        _sendAmountController.text = cryptoAmountStr;
        _sendAmountController.selection = TextSelection.fromPosition(
            TextPosition(offset: cryptoAmountStr.length));
      });
    } else {
      // Switching to local-currency mode
      String localAmountStr;
      // Check our previous state
      if (_sendAmountController.text == _lastCryptoAmount) {
        localAmountStr = _lastLocalCurrencyAmount;
      } else {
        _lastCryptoAmount = _sendAmountController.text;
        _lastLocalCurrencyAmount = _convertCryptoToLocalCurrency();
        localAmountStr = _lastLocalCurrencyAmount;
      }
      setState(() {
        _localCurrencyMode = true;
      });
      Future.delayed(Duration(milliseconds: 50), () {
        _sendAmountController.text = localAmountStr;
        _sendAmountController.selection = TextSelection.fromPosition(
            TextPosition(offset: localAmountStr.length));
      });
    }
  }


  //************ Enter Amount Container Method ************//
  //*******************************************************//
  getEnterAmountContainer() {
    return AppTextField(
      focusNode: _sendAmountFocusNode,
      controller: _sendAmountController,
      topMargin: 15,
      cursorColor: StateContainer.of(context).curTheme.primary,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16.0,
        color: StateContainer.of(context).curTheme.primary,
        fontFamily: 'NunitoSans',
      ),
      inputFormatters: [LengthLimitingTextInputFormatter(13)],
      onChanged: (text) {
        // Always reset the error message to be less annoying
        setState(() {
          _amountValidationText = "";
          // Reset the raw amount
          _rawAmount = null;
        });
      },
      textInputAction: TextInputAction.next,
      maxLines: null,
      autocorrect: false,
      hintText:
          _amountHint == null ? "" : AppLocalization.of(context).enterAmount, // text: enter amount
      suffixButton: TextFieldButton( // Max button
        icon: AppIcons.max,
        onPressed: () {
          if (_isMaxSend()) {
            return;
          }

          _sendAmountController.text = widget.coinData.avaBalance.replaceAll(",", ""); //_localCurrencyFormat.currencySymbol + localAmount;
          _sendBankController.selection = TextSelection.fromPosition(
                TextPosition(offset: _sendBankController.text.length));
        },
      ),
      fadeSuffixOnCondition: true,
      suffixShowFirstCondition: !_isMaxSend(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      textAlign: TextAlign.center,
      onSubmitted: (text) {
        FocusScope.of(context).unfocus();
        if (!Address(_sendBankController.text).isValid()) {
          FocusScope.of(context).requestFocus(_sendBankFocusNode);
        }
      },
    );
  } //************ Enter Address Container Method End ************//
  //*************************************************************//

  //************ Enter Bank Container Method ************//
  //*******************************************************//
  // getEnterBankContainer() {
  //   return AppTextField(
  //       topMargin: 0,
  //       padding: _bankValidAndUnfocused
  //           ? EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0)
  //           : EdgeInsets.zero,
  //       textAlign: TextAlign.center,
  //       focusNode: _sendBankFocusNode,
  //       controller: _sendBankController,
  //       cursorColor: StateContainer.of(context).curTheme.primary,
  //       inputFormatters: [LengthLimitingTextInputFormatter(65),],
  //       textInputAction: TextInputAction.done,
  //       maxLines: null,
  //       autocorrect: false,
  //       hintText: _bankHint == null
  //           ? ""
  //           : "Enter Bank Name/Code", //AppLocalization.of(context).enterAddress, 
  //       fadePrefixOnCondition: true,
  //       prefixShowFirstCondition: false, // _showContactButton && _contacts.length == 0,
  //       suffixButton: TextFieldButton( // paste button
  //         icon: AppIcons.paste,
  //         onPressed: () {
  //           if (!_pasteButtonVisible) {
  //             return;
  //           }
  //           Clipboard.getData("text/plain").then((ClipboardData data) {
  //             if (data == null || data.text == null || data.text.length > 55) {
  //               return;
  //             }
  //             Address address = Address(data.text);
  //             if (address.isValid()) {
  //               setState(() {
  //                 _bankValidationText = "";
  //                 _sendBankStyle = AddressStyle.TEXT90;
  //                 _pasteButtonVisible = false;
  //               });
  //               _sendBankController.text = data.text;
  //               _sendBankFocusNode.unfocus();
  //               setState(() {
  //                 _bankValidAndUnfocused = true;
  //               });
  //             }
  //           });
  //         },
  //       ),
  //       fadeSuffixOnCondition: true,
  //       suffixShowFirstCondition: _pasteButtonVisible,
  //       style: _sendBankStyle == AddressStyle.TEXT60
  //           ? AppStyles.textStyleAddressText60(context)
  //           : _sendBankStyle == AddressStyle.TEXT90
  //               ? AppStyles.textStyleAddressText90(context)
  //               : AppStyles.textStyleAddressPrimary(context),
  //       onChanged: (text) {

  //         if (Address(text).isValid()) {
  //           setState(() {
  //             _sendBankStyle = AddressStyle.TEXT90;
  //             _bankValidationText = "";
  //             _pasteButtonVisible = false;
  //           });
  //         } else {
  //           setState(() {
  //             _sendBankStyle = AddressStyle.TEXT60;
  //             _pasteButtonVisible = true;
  //           });
  //         } 

  //       },
  //       // overrideTextFieldWidget: _bankValidAndUnfocused
  //       //     ? GestureDetector(
  //       //         onTap: () {
  //       //           setState(() {
  //       //             _bankValidAndUnfocused = false;
  //       //           });
  //       //           Future.delayed(Duration(milliseconds: 50), () {
  //       //             FocusScope.of(context).requestFocus(_sendBankFocusNode);
  //       //           });
  //       //         },
  //       //         child: UIUtil.twoLineAddressText(
  //       //             context, _sendBankController.text))
  //       //     : null
  //   );
  // } //************ Enter Address Container Method End ************//
  // *************************************************************//

  //************ Enter Fullname Container Method ************//
  //*******************************************************//
  // getEnterFullnameContainer() {
  //   return AppTextField(
  //       topMargin: 0,
  //       padding: _fullnameValidAndUnfocused
  //           ? EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0)
  //           : EdgeInsets.zero,
  //       textAlign: TextAlign.center,
  //       focusNode: _sendFullnameFocusNode,
  //       controller: _sendFullnameController,
  //       cursorColor: StateContainer.of(context).curTheme.primary,
  //       inputFormatters: [LengthLimitingTextInputFormatter(65),],
  //       textInputAction: TextInputAction.done,
  //       maxLines: null,
  //       autocorrect: false,
  //       hintText: _fullnameHint == null
  //           ? ""
  //           : "Enter Account Name", //AppLocalization.of(context).enterAddress,
  //       fadePrefixOnCondition: true,
  //       prefixShowFirstCondition: false, // _showContactButton && _contacts.length == 0,
  //       suffixButton: TextFieldButton( // paste button
  //         icon: AppIcons.paste,
  //         onPressed: () {
  //           if (!_pasteButtonVisible2) {
  //             return;
  //           }
  //           Clipboard.getData("text/plain").then((ClipboardData data) {
  //             if (data == null || data.text == null || data.text.length > 55) {
  //               return;
  //             }
  //             Address address = Address(data.text);
  //             if (address.isValid()) {
  //               setState(() {
  //                 _fullnameValidationText = "";
  //                 _sendFullnameStyle = AddressStyle.TEXT90;
  //                 _pasteButtonVisible2 = false;
  //               });
  //               _sendFullnameController.text = data.text;
  //               _sendFullnameFocusNode.unfocus();
  //               setState(() {
  //                 _fullnameValidAndUnfocused = true;
  //               });
  //             }
  //           });
  //         },
  //       ),
  //       fadeSuffixOnCondition: true,
  //       suffixShowFirstCondition: _pasteButtonVisible2,
  //       style: _sendFullnameStyle == AddressStyle.TEXT60
  //           ? AppStyles.textStyleAddressText60(context)
  //           : _sendFullnameStyle == AddressStyle.TEXT90
  //               ? AppStyles.textStyleAddressText90(context)
  //               : AppStyles.textStyleAddressPrimary(context),
  //       onChanged: (text) {

  //         if (Address(text).isValid()) {
  //           setState(() {
  //             _sendFullnameStyle = AddressStyle.TEXT90;
  //             _fullnameValidationText = "";
  //             _pasteButtonVisible2 = false;
  //           });
  //         } else {
  //           setState(() {
  //             _sendFullnameStyle = AddressStyle.TEXT60;
  //             _pasteButtonVisible2 = true;
  //           });
  //         } 

  //       },
  //       // overrideTextFieldWidget: _bankValidAndUnfocused
  //       //     ? GestureDetector(
  //       //         onTap: () {
  //       //           setState(() {
  //       //             _bankValidAndUnfocused = false;
  //       //           });
  //       //           Future.delayed(Duration(milliseconds: 50), () {
  //       //             FocusScope.of(context).requestFocus(_sendBankFocusNode);
  //       //           });
  //       //         },
  //       //         child: UIUtil.twoLineAddressText(
  //       //             context, _sendBankController.text))
  //       //     : null
  //   );
  // } //************ Enter Address Container Method End ************//
  // *************************************************************//

  //************ Enter AcctNum Container Method ************//
  //*******************************************************//
  // getEnterAcctNumContainer() {
  //   return AppTextField(
  //       topMargin: 0,
  //       padding: _acctNumValidAndUnfocused
  //           ? EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0)
  //           : EdgeInsets.zero,
  //       textAlign: TextAlign.center,
  //       focusNode: _sendAcctNumFocusNode,
  //       controller: _sendAcctNumController,
  //       cursorColor: StateContainer.of(context).curTheme.primary,
  //       inputFormatters: [LengthLimitingTextInputFormatter(65),],
  //       textInputAction: TextInputAction.done,
  //       maxLines: null,
  //       autocorrect: false,
  //       hintText: _acctNumHint == null
  //           ? ""
  //           : "Enter Account Number", //AppLocalization.of(context).enterAddress,
  //       fadePrefixOnCondition: true,
  //       prefixShowFirstCondition: false, // _showContactButton && _contacts.length == 0,
  //       suffixButton: TextFieldButton( // paste button
  //         icon: AppIcons.paste,
  //         onPressed: () {
  //           if (!_pasteButtonVisible3) {
  //             return;
  //           }
  //           Clipboard.getData("text/plain").then((ClipboardData data) {
  //             if (data == null || data.text == null || data.text.length > 55) {
  //               return;
  //             }
  //             Address address = Address(data.text);
  //             if (address.isValid()) {
  //               setState(() {
  //                 _acctNumValidationText = "";
  //                 _sendAcctNumStyle = AddressStyle.TEXT90;
  //                 _pasteButtonVisible3 = false;
  //               });
  //               _sendAcctNumController.text = data.text;
  //               _sendAcctNumFocusNode.unfocus();
  //               setState(() {
  //                 _acctNumValidAndUnfocused = true;
  //               });
  //             }
  //           });
  //         },
  //       ),
  //       fadeSuffixOnCondition: true,
  //       suffixShowFirstCondition: _pasteButtonVisible3,
  //       style: _sendAcctNumStyle == AddressStyle.TEXT60
  //           ? AppStyles.textStyleAddressText60(context)
  //           : _sendAcctNumStyle == AddressStyle.TEXT90
  //               ? AppStyles.textStyleAddressText90(context)
  //               : AppStyles.textStyleAddressPrimary(context),
  //       onChanged: (text) {

  //         if (Address(text).isValid()) {
  //           setState(() {
  //             _sendAcctNumStyle = AddressStyle.TEXT90;
  //             _acctNumValidationText = "";
  //             _pasteButtonVisible3 = false;
  //           });
  //         } else {
  //           setState(() {
  //             _sendAcctNumStyle = AddressStyle.TEXT60;
  //             _pasteButtonVisible3 = true;
  //           });
  //         } 

  //       },
  //       // overrideTextFieldWidget: _bankValidAndUnfocused
  //       //     ? GestureDetector(
  //       //         onTap: () {
  //       //           setState(() {
  //       //             _bankValidAndUnfocused = false;
  //       //           });
  //       //           Future.delayed(Duration(milliseconds: 50), () {
  //       //             FocusScope.of(context).requestFocus(_sendBankFocusNode);
  //       //           });
  //       //         },
  //       //         child: UIUtil.twoLineAddressText(
  //       //             context, _sendBankController.text))
  //       //     : null
  //   );
  // } //************ Enter Address Container Method End ************//
  // *************************************************************//
}
