
import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:natrium_wallet_flutter/ui/widgets/tap_outside_unfocus.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/localization.dart';

import 'asset_detail_page.dart';

class AssetList extends StatefulWidget {
  @override
  _AssetListState createState() => _AssetListState();
}

class _AssetListState extends State<AssetList> {
  FocusNode enterPasswordFocusNode;
  TextEditingController enterPasswordController;

  String passwordError;

  @override
  void initState() {
    super.initState();
    this.enterPasswordFocusNode = FocusNode();
    this.enterPasswordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: TapOutsideUnfocus(
        child: Container(
          // margin: EdgeInsetsDirectional.only(bottom: 0),
          decoration: BoxDecoration(
            // border: Border.all(color: Colors.red),
            color: StateContainer.of(context).curTheme.backgroundDark,
          ),
          width: double.infinity,
          child: SafeArea(
            minimum: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.035,
            ),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // row1-c1: search assets text field 
                      Expanded(
                        child: Container(
                          // decoration: BoxDecoration(border: Border.all(color: Colors.yellow),),
                          child: KeyboardAvoider(
                            duration: Duration(milliseconds: 0),
                            autoScroll: true,
                            focusPadding: 40,
                            child: AppTextField(
                              prefixButton: TextFieldButton(
                                icon: AppIcons.search,
                              ),
                              leftMargin: 10,
                              rightMargin: 10,
                              // onChanged: (){},
                              hintText: "Search assets",  // AppLocalization.of(context).confirmPasswordHint,
                              keyboardType: TextInputType.text,
                              obscureText: false,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 16.0,
                                color: StateContainer.of(context).curTheme.primary,
                                fontFamily: 'NunitoSans',
                              ),
                            ),
                          )
                        )
                      ),

                      // row1-c2: cancel button
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red),),
                        // margin: EdgeInsetsDirectional.only(start: 16, top: 12),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            FlatButton(
                              // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              highlightColor:
                                  StateContainer.of(context).curTheme.text15,
                              splashColor: StateContainer.of(context).curTheme.text30,
                              // padding: EdgeInsetsDirectional.fromSTEB(12, 4, 12, 4),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      // margin: EdgeInsetsDirectional.only(start: 4),
                                      child: Text(AppLocalization.of(context).cancel,
                                          style:
                                              AppStyles.textStyleLogoutButton(context)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),

                Expanded(
                  flex: 15,
                  child: ListView(
                    padding: EdgeInsetsDirectional.fromSTEB(0, 10, 0, 0),
                    children: <Widget>[
                      // _buildWelcomeTransactionCard(context),
                      _buildDummyTransactionCard(
                          "BUY",
                          AppLocalization.of(context).exampleCardLittle,
                          AppLocalization.of(context).exampleCardTo,
                          context),
                      _buildDummyTransactionCard(
                          "SELL",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                      _buildDummyTransactionCard(
                          "BUY",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                      _buildDummyTransactionCard(
                          "SELL",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                      _buildDummyTransactionCard(
                          "BUY",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                      _buildDummyTransactionCard(
                          "SELL",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                      _buildDummyTransactionCard(
                          "BUY",
                          AppLocalization.of(context).exampleCardLot,
                          AppLocalization.of(context).exampleCardFrom,
                          context),
                    ],
                  ),
                )
                
              ],
            )
            
          ),
        )
      )
    );
  }

  // Dummy Transaction Card
  Widget _buildDummyTransactionCard(
      String type, String amount, String address, BuildContext context) {
    String text;
    IconData icon;
    Color iconColor;
    TextStyle buySellStyle;
  
    text = type;
    buySellStyle = type == "BUY" ? AppStyles.textStyleBuy(context) : AppStyles.textStyleSell(context);
    icon = AppIcons.btc;
    iconColor = StateContainer.of(context).curTheme.primary60;
    
    return Container(
      margin: EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0), // 14.0, 4.0, 14.0, 4.0
      decoration: BoxDecoration(
        color: StateContainer.of(context).curTheme.backgroundDark,
        // borderRadius: BorderRadius.circular(0.0),
        boxShadow: [StateContainer.of(context).curTheme.boxShadow],
        border: Border(top: BorderSide(color: StateContainer.of(context).curTheme.text20)),
      ),
      child: FlatButton(
        onPressed: () {
          // return null;
          Navigator.pop(context);
          Navigator.push(context,
            MaterialPageRoute(builder: (context) => AssetDetail()));
        },
        highlightColor: StateContainer.of(context).curTheme.text15,
        splashColor: StateContainer.of(context).curTheme.text15,
        color: StateContainer.of(context).curTheme.backgroundDark,
        // padding: EdgeInsets.all(0.0),
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Center(
          child: Padding(
            padding:
                const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

                Align(
                  alignment: Alignment.center,
                  child: Container(
                      // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                      margin: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                      child: Icon(icon, color: iconColor, size: 20)
                    ),
                ),

                Text(
                  "BTC",
                  textAlign: TextAlign.start,
                  style: AppStyles.textStyleTransactionType(context),
                ),

                Text(
                  " (Bitcoin)",
                  textAlign: TextAlign.start,
                  style: AppStyles.textStyleTransactionAddress(context),
                ),
                
              ],
            ),
          ),
        ),
      ),
    );
  } //Dummy Transaction Card End


}
