import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:flare_flutter/flare_actor.dart';

class IntroWelcomePage extends StatefulWidget {
  @override
  _IntroWelcomePageState createState() => _IntroWelcomePageState();
}

class _IntroWelcomePageState extends State<IntroWelcomePage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
      body: LayoutBuilder(
        builder: (context, constraints) => SafeArea(
              minimum: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.035,
                top: MediaQuery.of(context).size.height * 0.10,
              ),
              child: Column(
                children: <Widget>[
                  //A widget that holds welcome animation + paragraph
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        //Container for the animation
                        Container(
                          //Width/Height ratio for the animation is needed because BoxFit is not working as expected
                          width: double.infinity,
                          height: MediaQuery.of(context).size.width * 5 / 8,
                          child: Center(
                            child: Image.asset('assets/images/bitginko_blue.png'),
                            
                            // FlareActor( // johnnytodo: change to png logo first, or use canva to make a bitginko gif and conver to flare.
                            //   // "assets/welcome_animation.flr",
                            //   "assets/WorldSpin.flr",
                            //   animation: "roll",
                            //   fit: BoxFit.contain,
                            //   // color: StateContainer.of(context).curTheme.primary,
                            // ),
                          ),
                        ),
                        //Container for the paragraph
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: smallScreen(context)?30:40, vertical: 20),
                          child: AutoSizeText(
                            "Welcome to BitGinko. \n你的專業比特幣投資平台。", // johnnyTranslate //AppLocalization.of(context).welcomeText,
                            style: AppStyles.textStyleParagraph(context),
                            maxLines: 4,
                            stepGranularity: 0.5,
                          ),
                        ),
                      ],
                    ),
                  ),

                  //A column with "Login" and "Signup" buttons
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // Login Button
                          AppButton.buildAppButton(
                              context,
                              AppButtonType.PRIMARY,
                              '登錄', // johnnyTranslate //AppLocalization.of(context).newWallet,
                              Dimens.BUTTON_TOP_DIMENS, onPressed: () {
                                Navigator.of(context).pushNamed('/intro_login'); // 
                                // Navigator.of(context).pushNamed('/home'); // /
                                // Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                          }),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          // Signup Button
                          AppButton.buildAppButton(
                              context,
                              AppButtonType.PRIMARY_OUTLINE,
                              '注冊', // johnnyTranslate //AppLocalization.of(context).importWallet,
                              Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                                Navigator.of(context).pushNamed('/intro_signup'); // /intro_import
                          }),
                        ],
                      ),
                    ],
                  ),

                  //A column with "New Wallet" and "Import Wallet" buttons
                  // Column(
                  //   children: <Widget>[
                  //     Row(
                  //       children: <Widget>[
                  //         // New Wallet Button
                  //         AppButton.buildAppButton(
                  //             context,
                  //             AppButtonType.PRIMARY,
                  //             AppLocalization.of(context).newWallet,
                  //             Dimens.BUTTON_TOP_DIMENS, onPressed: () {
                  //               Navigator.of(context)
                  //                   .pushNamed('/intro_password_on_launch');
                  //         }),
                  //       ],
                  //     ),
                  //     Row(
                  //       children: <Widget>[
                  //         // Import Wallet Button
                  //         AppButton.buildAppButton(
                  //             context,
                  //             AppButtonType.PRIMARY_OUTLINE,
                  //             AppLocalization.of(context).importWallet,
                  //             Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                  //           Navigator.of(context).pushNamed('/intro_import');
                  //         }),
                  //       ],
                  //     ),
                  //   ],
                  // ),


                ],
              ),
            ),
      ),
    );
  }
}