import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:nanodart/nanodart.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/security.dart';
import 'package:natrium_wallet_flutter/ui/widgets/tap_outside_unfocus.dart';
import 'package:natrium_wallet_flutter/util/nanoutil.dart';
import 'package:natrium_wallet_flutter/model/vault.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

class IntroLogin extends StatefulWidget {
  // final String seed;
  // IntroLogin({this.seed});
  @override
  _IntroLoginState createState() => _IntroLoginState();
}

class _IntroLoginState extends State<IntroLogin> {
  FocusNode inputEmailFocusNode;
  TextEditingController inputEmailController;
  FocusNode inputPasswordFocusNode;
  TextEditingController inputPasswordController;

  String passwordError;

  bool passwordsMatch;

  @override
  void initState() {
    super.initState();
    this.passwordsMatch = false;
    this.inputEmailFocusNode = FocusNode();
    this.inputPasswordFocusNode = FocusNode();
    this.inputEmailController = TextEditingController();
    this.inputPasswordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
      body: TapOutsideUnfocus(
        child: LayoutBuilder(
          builder: (context, constraints) => SafeArea(
            minimum: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.035,
                top: MediaQuery.of(context).size.height * 0.075),
            child: Column(
              children: <Widget>[
                //A widget that holds the header, the paragraph and Back Button
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // Back Button
                          Container(
                            margin: EdgeInsetsDirectional.only(
                                start: smallScreen(context) ? 15 : 20),
                            height: 50,
                            width: 50,
                            child: FlatButton(
                                highlightColor:
                                    StateContainer.of(context).curTheme.text15,
                                splashColor:
                                    StateContainer.of(context).curTheme.text15,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0)),
                                padding: EdgeInsets.all(0.0),
                                child: Icon(AppIcons.back,
                                    color:
                                        StateContainer.of(context).curTheme.text,
                                    size: 24)),
                          ),
                        ],
                      ),
                      // The header
                      Container(
                        margin: EdgeInsetsDirectional.only(
                          start: smallScreen(context) ? 30 : 40,
                          end: smallScreen(context) ? 30 : 40,
                          top: 10,
                        ),
                        alignment: Alignment.center,
                        // alignment: AlignmentDirectional(-1, 0),
                        child: AutoSizeText(
                          '登入交易後台', // johnnyTranslate // AppLocalization.of(context).createAPasswordHeader,
                          maxLines: 3,
                          stepGranularity: 0.5,
                          style: AppStyles.textStyleHeaderColored(context),
                        ),
                      ),
                      // The paragraph
                      // Container(
                      //   margin: EdgeInsetsDirectional.only(
                      //       start: smallScreen(context) ? 30 : 40,
                      //       end: smallScreen(context) ? 30 : 40,
                      //       top: 16.0),
                      //   child: AutoSizeText(
                      //     AppLocalization.of(context).passwordWillBeRequiredToOpenParagraph,
                      //     style: AppStyles.textStyleParagraph(context),
                      //     maxLines: 5,
                      //     stepGranularity: 0.5,
                      //   ),
                      // ),
                      Expanded(
                        child: KeyboardAvoider(
                          duration: Duration(milliseconds: 0),
                          autoScroll: true,
                          focusPadding: 40,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              // upper text: Create a Email Text Field
                              AppTextField(
                                topMargin: 30,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputEmailFocusNode,
                                controller: inputEmailController,
                                textInputAction: TextInputAction.next,
                                maxLines: 1,
                                autocorrect: false,
                                // onChanged: (String newText) {
                                //   if (passwordError != null) {
                                //     setState(() {
                                //       passwordError = null;
                                //     });
                                //   }
                                //   if (inputPasswordController.text == inputEmailController.text) {
                                //     if (mounted) {
                                //       setState(() {
                                //         passwordsMatch = true;
                                //       });
                                //     }
                                //   } else {
                                //     if (mounted) {
                                //       setState(() {
                                //         passwordsMatch = false;
                                //       });
                                //     }
                                //   }
                                // },
                                hintText: '登入電郵', // johnnyTranslate //AppLocalization.of(context).inputEmailHint,
                                keyboardType: TextInputType.text,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                    this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                                onSubmitted: (text) {
                                  inputPasswordFocusNode.requestFocus();
                                },
                              ),
                 
                              // lower text: Password Text Field
                              AppTextField(
                                topMargin: 20,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputPasswordFocusNode,
                                controller: inputPasswordController,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                autocorrect: false,
                                // onChanged: (String newText) {
                                //   if (passwordError != null) {
                                //     setState(() {
                                //       passwordError = null;
                                //     });
                                //   }
                                //   if (inputPasswordController.text == inputEmailController.text) {
                                //     if (mounted) {
                                //       setState(() {
                                //         passwordsMatch = true;
                                //       });
                                //     }
                                //   } else {
                                //     if (mounted) {
                                //       setState(() {
                                //         passwordsMatch = false;
                                //       });
                                //     }
                                //   }                              
                                // },
                                hintText: '密碼', // johnnyTranslate // AppLocalization.of(context).inputPasswordHint,
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                      this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              // Error Text
                              Container(
                                alignment: AlignmentDirectional(0, 0),
                                margin: EdgeInsets.only(top: 3),
                                child: Text(this.passwordError == null ? "" : passwordError, 
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.redAccent, //StateContainer.of(context).curTheme.primary,
                                      fontFamily: 'NunitoSans',
                                      fontWeight: FontWeight.w600,
                                    )),
                              ),
                            ]
                          )
                        )
                      )
                    ],
                  ),
                ),

                //A column with "Next" and "Go Back" buttons
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        // Next Button
                        AppButton.buildAppButton(context, AppButtonType.PRIMARY,
                            AppLocalization.of(context).nextButton, Dimens.BUTTON_TOP_DIMENS, onPressed: () async {
                            // await submitAndEncrypt();

                            // johnny: 
                            // 1. frontend input checking 
                            // 2. use login api + use .then() - done
                            // 3. if success, request update and redirect page - done
                            // 4. if fail, show corresponding error msg - done

                            String inputEmail = inputEmailController.text;
                            String inputPw = inputPasswordController.text;

                            if (inputEmail.isEmpty || inputPw.isEmpty) {
                              setState(() {
                                  this.passwordError = "請填寫登錄資料"; // "Please fill in both email and password";  // johnnyTranslate
                                });
                            } else {
                              
                              String md5pw = md5.convert(utf8.encode(inputPw)).toString();
                              NanoUtil().loginAccountBitginko(context, email: inputEmail, md5pw: md5pw).then((result) {
                              //   StateContainer.of(context).requestUpdate();
                                if (result == "Success")
                                  Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                                else {
                                  String err = result == "pass_not_correct" ? "密碼不正確" //"Incorrect Password"  // johnnyTranslate
                                                : result == "mail_invalid" ? "郵件格式無效"
                                                : "登錄資料錯誤"; // johnnytodo: show all types of error here, from web bitginko
                                  setState(() {
                                    this.passwordError = err; 
                                  });
                                }
                              });
                            }
                            
                          }),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Go Back Button
                        AppButton.buildAppButton(
                            context,
                            AppButtonType.PRIMARY_OUTLINE,
                            AppLocalization.of(context).goBackButton,
                            Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                              Navigator.of(context).pop();
                            }
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      )
    );
  }

  // Future<void> submitAndEncrypt() async {
  //   if (inputEmailController.text.isEmpty || inputPasswordController.text.isEmpty) {
  //     if (mounted) {
  //       setState(() {
  //         passwordError = AppLocalization.of(context).passwordBlank;
  //       });
  //     }
  //   } else if (inputEmailController.text != inputPasswordController.text) {
  //     if (mounted) {
  //       setState(() {
  //         passwordError = AppLocalization.of(context).passwordsDontMatch;
  //       });
  //     }
  //   } else if (widget.seed != null) {
  //     String encryptedSeed = NanoHelpers.byteToHex(NanoCrypt.encrypt(widget.seed, inputPasswordController.text));
  //     await sl.get<Vault>().setSeed(encryptedSeed);
  //     StateContainer.of(context).setEncryptedSecret(NanoHelpers.byteToHex(NanoCrypt.encrypt(widget.seed, await sl.get<Vault>().getSessionKey())));
  //     await sl.get<DBHelper>().dropAccounts();
  //     await NanoUtil().loginAccount(widget.seed, context);
  //     StateContainer.of(context).requestUpdate();
  //     Navigator.of(context).push(
  //       MaterialPageRoute(builder: (BuildContext context) {
  //         return PinScreen(PinOverlayType.NEW_PIN, _pinEnteredCallback);
  //     }));
  //   } else {
  //     // Generate a new seed and encrypt
  //     String seed = NanoSeeds.generateSeed();
  //     String encryptedSeed = NanoHelpers.byteToHex(NanoCrypt.encrypt(seed, inputPasswordController.text));
  //     await sl.get<Vault>().setSeed(encryptedSeed);
  //     // Also encrypt it with the session key, so user doesnt need password to sign blocks within the app
  //     StateContainer.of(context).setEncryptedSecret(NanoHelpers.byteToHex(NanoCrypt.encrypt(seed, await sl.get<Vault>().getSessionKey())));
  //     // Update wallet
  //     NanoUtil().loginAccount(await StateContainer.of(context).getSeed(), context).then((_) {
  //       StateContainer.of(context).requestUpdate();
  //       Navigator.of(context)
  //           .pushNamed('/intro_backup_safety');
  //     });
  //   }    
  // }

  void _pinEnteredCallback(String pin) async {
    Navigator.of(context).pop();
    await sl.get<Vault>().writePin(pin);
    PriceConversion conversion = await sl.get<SharedPrefsUtil>().getPriceConversion();
    // Update wallet
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false, arguments: conversion);

  }
}
