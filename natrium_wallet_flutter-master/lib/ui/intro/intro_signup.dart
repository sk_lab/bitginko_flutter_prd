import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:nanodart/nanodart.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/ui/intro/intro_signup_done.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/widgets/security.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/tap_outside_unfocus.dart';
import 'package:natrium_wallet_flutter/util/nanoutil.dart';
import 'package:natrium_wallet_flutter/model/vault.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

class IntroSignup extends StatefulWidget {
  // final String seed;
  // IntroSignup({this.seed});
  @override
  _IntroSignupState createState() => _IntroSignupState();
}

class _IntroSignupState extends State<IntroSignup> {
  FocusNode inputEmailFocusNode;
  TextEditingController inputEmailController;
  FocusNode inputPasswordFocusNode;
  TextEditingController inputPasswordController;
  FocusNode inputNameFocusNode;
  TextEditingController inputNameController;
  FocusNode inputRefCodeFocusNode;
  TextEditingController inputRefCodeController;
  FocusNode inputPhoneFocusNode;
  TextEditingController inputPhoneController;

  String passwordError;

  bool passwordsMatch;
  bool _checked;

  bool animationOpen;

  @override
  void initState() {
    super.initState();
    this.passwordsMatch = false;
    this.inputEmailFocusNode = FocusNode();
    this.inputEmailController = TextEditingController();
    this.inputPasswordFocusNode = FocusNode();
    this.inputPasswordController = TextEditingController();
    this.inputNameFocusNode = FocusNode();
    this.inputNameController = TextEditingController();
    this.inputRefCodeFocusNode = FocusNode();
    this.inputRefCodeController = TextEditingController();
    this.inputPhoneFocusNode = FocusNode();
    this.inputPhoneController = TextEditingController();
    this._checked = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
      body: TapOutsideUnfocus(
        child: LayoutBuilder(
          builder: (context, constraints) => SafeArea(
            minimum: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.035,
                top: MediaQuery.of(context).size.height * 0.075),
            child: Column(
              children: <Widget>[
                //A widget that holds the header, the paragraph and Back Button
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // Back Button
                          // Container(
                          //   margin: EdgeInsetsDirectional.only(
                          //       start: smallScreen(context) ? 15 : 20),
                          //   height: 50,
                          //   width: 50,
                          //   child: FlatButton(
                          //       highlightColor:
                          //           StateContainer.of(context).curTheme.text15,
                          //       splashColor:
                          //           StateContainer.of(context).curTheme.text15,
                          //       onPressed: () {
                          //         Navigator.pop(context);
                          //       },
                          //       shape: RoundedRectangleBorder(
                          //           borderRadius: BorderRadius.circular(50.0)),
                          //       padding: EdgeInsets.all(0.0),
                          //       child: Icon(AppIcons.back,
                          //           color:
                          //               StateContainer.of(context).curTheme.text,
                          //           size: 24)),
                          // ),
                        ],
                      ),
                      // The header
                      Container(
                        margin: EdgeInsetsDirectional.only(
                          start: smallScreen(context) ? 30 : 40,
                          end: smallScreen(context) ? 30 : 40,
                          top: 10,
                        ),
                        alignment: Alignment.center,
                        // alignment: AlignmentDirectional(-1, 0),
                        child: AutoSizeText(
                          '注冊帳號', // johnnyTranslate // AppLocalization.of(context).createAPasswordHeader,
                          maxLines: 3,
                          stepGranularity: 0.5,
                          style: AppStyles.textStyleHeaderColored(context),
                        ),
                      ),
                      // The paragraph
                      // Container(
                      //   margin: EdgeInsetsDirectional.only(
                      //       start: smallScreen(context) ? 30 : 40,
                      //       end: smallScreen(context) ? 30 : 40,
                      //       top: 16.0),
                      //   child: AutoSizeText(
                      //     AppLocalization.of(context).passwordWillBeRequiredToOpenParagraph,
                      //     style: AppStyles.textStyleParagraph(context),
                      //     maxLines: 5,
                      //     stepGranularity: 0.5,
                      //   ),
                      // ),
                      Expanded(
                        child: KeyboardAvoider(
                          duration: Duration(milliseconds: 0),
                          autoScroll: true,
                          // focusPadding: 40,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              // upper text: Create a Email Text Field
                              AppTextField(
                                topMargin: 30,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputEmailFocusNode,
                                controller: inputEmailController,
                                textInputAction: TextInputAction.next,
                                maxLines: 1,
                                autocorrect: false,
                                hintText: '登入電郵', // johnnyTranslate //AppLocalization.of(context).inputEmailHint,
                                keyboardType: TextInputType.text,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                    this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                                onSubmitted: (text) {
                                  inputPasswordFocusNode.requestFocus();
                                },
                              ),
                 
                              // 2nd text: Password Text Field
                              AppTextField(
                                topMargin: 20,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputPasswordFocusNode,
                                controller: inputPasswordController,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                autocorrect: false,
                                hintText: '密碼', // johnnyTranslate // AppLocalization.of(context).inputPasswordHint,
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                      this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),

                              // 3rd text: Name Text Field
                              AppTextField(
                                topMargin: 20,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputNameFocusNode,
                                controller: inputNameController,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                autocorrect: false,
                                hintText: '姓名(須於銀行戶口名稱一致)', // johnnyTranslate 
                                keyboardType: TextInputType.text,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                      this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),

                              // 4th text: Referral code Text Field
                              AppTextField(
                                topMargin: 20,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputRefCodeFocusNode,
                                controller: inputRefCodeController,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                autocorrect: false,
                                hintText: '推薦碼(可留空)', // johnnyTranslate 
                                keyboardType: TextInputType.text,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                      this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),

                              // 5th text: Phone Text Field
                              AppTextField(
                                topMargin: 20,
                                padding: EdgeInsetsDirectional.only(start: 5, end: 5),
                                focusNode: inputPhoneFocusNode,
                                controller: inputPhoneController,
                                textInputAction: TextInputAction.done,
                                maxLines: 1,
                                autocorrect: false,
                                hintText: '電話號碼', // johnnyTranslate 
                                keyboardType: TextInputType.text,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                  color:
                                      this.passwordsMatch ? StateContainer.of(context).curTheme.primary : StateContainer.of(context).curTheme.text,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),
                              Theme(
                                data: ThemeData(unselectedWidgetColor: Colors.grey), 
                                child: CheckboxListTile(
                                  contentPadding: EdgeInsets.only(left: 35),
                                  title: Text("本人確認我已年滿18歳並同意遵守 ",
                                      style: AppStyles.textStyleParagraph(context),), // johnnyTranslate
                                  value: _checked,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _checked = newValue;
                                    });
                                  },
                                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                  activeColor: Colors.green,
                                  checkColor: Colors.grey,
                                ),
                              ),
                              
                              Container(
                                padding: EdgeInsets.only(left: 95),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: new InkWell(
                                            child: new Text('使用條款',
                                              style: TextStyle(decoration: TextDecoration.underline, color: Colors.grey)
                                            ),
                                            onTap: () => {
                                              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                                return UIUtil.showWebview(context,'https://bitginko.com/tc'); // johnnytodo: put this link into config, make it consistent everywhere
                                              }))
                                            }
                                        ),
                                      ),
                                      Text("  &  ", style: TextStyle(color: Colors.grey)),
                                      Container(
                                        child: new InkWell(
                                            child: new Text('私隱政策',
                                              style: TextStyle(decoration: TextDecoration.underline, color: Colors.grey)
                                            ),
                                            onTap: () => {
                                              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                                return UIUtil.showWebview(context,'https://bitginko.com/privacy'); // johnnytodo: put this link into config, make it consistent everywhere
                                              }))
                                            }
                                        ),
                                      ),
                                    ],
                                  ),
                              ),
                              
                              


                              SizedBox(
                                height: 10,
                              ),
                              // Error Text
                              Container(
                                alignment: AlignmentDirectional(0, 0),
                                margin: EdgeInsets.only(top: 3, left:15, right:15),
                                child: Text(this.passwordError == null ? "" : passwordError, 
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.redAccent, //StateContainer.of(context).curTheme.primary,
                                      fontFamily: 'NunitoSans',
                                      fontWeight: FontWeight.w600,
                                    )),
                              ),
                            ]
                          )
                        )
                      )
                    ],
                  ),
                ),
                

                //A column with "Next" and "Go Back" buttons
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        // Next Button
                        AppButton.buildAppButton(context, AppButtonType.PRIMARY,
                            AppLocalization.of(context).nextButton, Dimens.BUTTON_TOP_DIMENS, onPressed: () async {
                            // await submitAndEncrypt();

                            // johnny: 
                            // 1. frontend input checking 
                            // 2. use login api + use .then() 
                            // 3. if success, request update and redirect page 
                            // 4. if fail, show corresponding error msg 

                            String inputEmail = inputEmailController.text;
                            String inputPw = inputPasswordController.text;
                            String inputName = inputNameController.text;
                            String inputRefCode = inputRefCodeController.text;
                            String inputPhone = inputPhoneController.text;

                            if (inputEmail.isEmpty || inputPw.isEmpty || inputName.isEmpty || inputPhone.isEmpty) {
                              setState(() {
                                  this.passwordError = "Please fill in the required information";  // johnnyTranslate
                                });
                            } else if (_checked == false) {
                                setState(() {
                                  this.passwordError = "Please check to confirm your age and the understanding on the TC and privacy";  // johnnyTranslate
                                });
                            } else {
                              // johnny: add loading logo when ok to submit signup
                              _showSendingAnimation(context);
                              String md5pw = md5.convert(utf8.encode(inputPw)).toString();
                              NanoUtil().registerAccountBitginko(context, 
                                  email: inputEmail, pw: md5pw, name: inputName, code: inputRefCode, phone: inputPhone).then((result) {

                              //   StateContainer.of(context).requestUpdate();
                                if (result == "Success") {                                  
                                  Sheets.showAppHeightNineSheet(
                                          context: context,
                                          closeOnTap: true,
                                          removeUntilHome: true,
                                          widget: SignupCompleteSheet());
                                } else {
                                  // johnnyTranslate
                                  String err = result == "pass_not_correct" ? "密碼不正確" 
                                                : result == "mail_invalid" ? "郵件格式無效"
                                                : result == "contact_admin" ? "Account not exists"
                                                : result == "pass_invalid" ? "密碼格式無效"
                                                : result == "account_exist" ? "帳戶已存在"
                                                : result == "name_cannot_empty" ? "名字是空的"
                                                : result == "mail_cannot_empty" ? "郵件是空的"
                                                : result == "phone_error" ? "電話號碼錯誤"
                                                : "注冊錯誤"; // johnnytodo: show all types of error here, from web bitginko
                                  setState(() {
                                    this.passwordError = err; 
                                  });
                                }
                              });
                            }
                            
                          }),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Go Back Button
                        AppButton.buildAppButton(
                            context,
                            AppButtonType.PRIMARY_OUTLINE,
                            AppLocalization.of(context).goBackButton,
                            Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                              Navigator.of(context).pop();
                            }
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      )
    );
  }

  void _showSendingAnimation(BuildContext context) {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.SEND,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }

  // Future<void> submitAndEncrypt() async {
  //   if (inputEmailController.text.isEmpty || inputPasswordController.text.isEmpty) {
  //     if (mounted) {
  //       setState(() {
  //         passwordError = AppLocalization.of(context).passwordBlank;
  //       });
  //     }
  //   } else if (inputEmailController.text != inputPasswordController.text) {
  //     if (mounted) {
  //       setState(() {
  //         passwordError = AppLocalization.of(context).passwordsDontMatch;
  //       });
  //     }
  //   } else if (widget.seed != null) {
  //     String encryptedSeed = NanoHelpers.byteToHex(NanoCrypt.encrypt(widget.seed, inputPasswordController.text));
  //     await sl.get<Vault>().setSeed(encryptedSeed);
  //     StateContainer.of(context).setEncryptedSecret(NanoHelpers.byteToHex(NanoCrypt.encrypt(widget.seed, await sl.get<Vault>().getSessionKey())));
  //     await sl.get<DBHelper>().dropAccounts();
  //     await NanoUtil().loginAccount(widget.seed, context);
  //     StateContainer.of(context).requestUpdate();
  //     Navigator.of(context).push(
  //       MaterialPageRoute(builder: (BuildContext context) {
  //         return PinScreen(PinOverlayType.NEW_PIN, _pinEnteredCallback);
  //     }));
  //   } else {
  //     // Generate a new seed and encrypt
  //     String seed = NanoSeeds.generateSeed();
  //     String encryptedSeed = NanoHelpers.byteToHex(NanoCrypt.encrypt(seed, inputPasswordController.text));
  //     await sl.get<Vault>().setSeed(encryptedSeed);
  //     // Also encrypt it with the session key, so user doesnt need password to sign blocks within the app
  //     StateContainer.of(context).setEncryptedSecret(NanoHelpers.byteToHex(NanoCrypt.encrypt(seed, await sl.get<Vault>().getSessionKey())));
  //     // Update wallet
  //     NanoUtil().loginAccount(await StateContainer.of(context).getSeed(), context).then((_) {
  //       StateContainer.of(context).requestUpdate();
  //       Navigator.of(context)
  //           .pushNamed('/intro_backup_safety');
  //     });
  //   }    
  // }

  void _pinEnteredCallback(String pin) async {
    Navigator.of(context).pop();
    await sl.get<Vault>().writePin(pin);
    PriceConversion conversion = await sl.get<SharedPrefsUtil>().getPriceConversion();
    // Update wallet
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false, arguments: conversion);

  }
}
