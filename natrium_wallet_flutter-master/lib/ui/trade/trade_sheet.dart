import 'dart:convert';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:decimal/decimal.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:manta_dart/manta_wallet.dart';
import 'package:manta_dart/messages.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/model/available_currency.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/model/address.dart';
import 'package:natrium_wallet_flutter/model/db/contact.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/themes.dart';
import 'package:natrium_wallet_flutter/ui/trade/trade_complete_sheet.dart';
import 'package:natrium_wallet_flutter/ui/trade/trade_confirm_sheet.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_text_field.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/util/formatters.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/util/manta.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'package:natrium_wallet_flutter/util/user_data_util.dart';

// johnny: step1 of trading
class TradeSheet extends StatefulWidget {
  final AvailableCurrency localCurrency;
  String tradeMode;
  CoinData coinData;
  String hkdPrice;

  // Contact contact;
  // String address;
  // String quickTradeAmount;

  TradeSheet(
      {@required this.localCurrency,
      this.tradeMode,
      this.coinData,
      this.hkdPrice
      // this.contact,
      // this.address,
      // this.quickTradeAmount
      })
      : super();

  _TradeSheetState createState() => _TradeSheetState();
}

enum AddressStyle { TEXT60, TEXT90, PRIMARY }

class _TradeSheetState extends State<TradeSheet> {
  final Logger log = sl.get<Logger>();

  FocusNode _tradeCoinAmountFocusNode;
  TextEditingController _tradeCoinAmountController;
  FocusNode _tradeFiatAmountFocusNode;
  TextEditingController _tradeFiatAmountController;

  // States
  AddressStyle _tradeCoinAmountStyle;
  String _fiatAmtHint = "";
  String _coinAmtHint = "";
  String _fiatAmtValidationText = "";
  String _coinAmtValidationText = "";
  String quickTradeAmount;
  // List<Contact> _contacts;
  bool animationOpen;
  // Used to replace address textfield with colorized TextSpan
  bool _coinAmtValidAndUnfocused = false;
  // Set to true when a contact is being entered
  // bool _isContact = false;
  // Buttons States (Used because we hide the buttons under certain conditions)
  // bool _pasteButtonVisible = true;
  // bool _showContactButton = true;
  // Local currency mode/fiat conversion
  // bool _localCurrencyMode = false;
  // String _lastLocalCurrencyAmount = "";
  String _lastCryptoAmount = "";
  NumberFormat _localCurrencyFormat;

  String coinAvaBalance;

  // String _rawAmount; // johnny no use

  @override
  void initState() {
    super.initState();
    _tradeFiatAmountFocusNode = FocusNode();
    _tradeCoinAmountFocusNode = FocusNode();
    _tradeFiatAmountController = TextEditingController();
    _tradeCoinAmountController = TextEditingController(); 
    _tradeCoinAmountStyle = AddressStyle.TEXT60;
    // _contacts = List();
    // quickTradeAmount = widget.quickTradeAmount;
    this.animationOpen = false;
    coinAvaBalance = widget.coinData.avaBalance.replaceAll(",", "");


    // if (widget.contact != null) {
    //   // Setup initial state for contact pre-filled
    //   _tradeCoinAmountController.text = widget.contact.name;
    //   _isContact = true;
    //   _showContactButton = false;
    //   _pasteButtonVisible = false;
    //   _tradeCoinAmountStyle = AddressStyle.PRIMARY;
    // } else if (widget.address != null) {
    //   // Setup initial state with prefilled address
    //   _tradeCoinAmountController.text = widget.address;
    //   _showContactButton = false;
    //   _pasteButtonVisible = false;
    //   _tradeCoinAmountStyle = AddressStyle.TEXT90;
    //   _coinAmtValidAndUnfocused = true;
    // }
    
    // On FiatAmt focus change
    _tradeFiatAmountFocusNode.addListener(() {
      if (_tradeFiatAmountFocusNode.hasFocus) {
        // if (_rawAmount != null) {
        //   setState(() {
        //     _tradeFiatAmountController.text =
        //         NumberUtil.getRawAsUsableString(_rawAmount).replaceAll(",", "");
        //     _rawAmount = null;
        //   });
        // }
        if (quickTradeAmount != null) {
          _tradeFiatAmountController.text = "";
          setState(() {
            quickTradeAmount = null;
          });
        }
        setState(() {
          _fiatAmtHint = null;
        });
      } else {
        setState(() {
          _fiatAmtHint = "";
        });
      }
    });
    // On CoinAmt focus change
    _tradeCoinAmountFocusNode.addListener(() {
      if (_tradeCoinAmountFocusNode.hasFocus) {
        setState(() {
          _coinAmtHint = null;
          _coinAmtValidAndUnfocused = false;
        });
        _tradeCoinAmountController.selection = TextSelection.fromPosition(
            TextPosition(offset: _tradeCoinAmountController.text.length));

      } else {
        setState(() {
          _coinAmtHint = "";
          
          if (Address(_tradeCoinAmountController.text).isValid()) {
            _coinAmtValidAndUnfocused = true;
          }
        });
      }
    });
    // Set initial currency format
    _localCurrencyFormat = NumberFormat.currency(
        locale: widget.localCurrency.getLocale().toString(),
        symbol: widget.localCurrency.getCurrencySymbol());
    // Set quick trade amount
    if (quickTradeAmount != null) {
      _tradeFiatAmountController.text =
          NumberUtil.getRawAsUsableString(quickTradeAmount).replaceAll(",", "");
    }
  }

  void _showMantaAnimation() {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.MANTA,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }

  String _getTradePrice(String hkdPrice) {
    if (hkdPrice == "") return "0";

    hkdPrice = hkdPrice.replaceAll(",", "");

    Decimal hkdPriceDecimal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(hkdPrice);
    Decimal tradePriceDecimal;
    
    if (widget.tradeMode == "BUY") {
      tradePriceDecimal = hkdPriceDecimal * Decimal.parse("1.03919"); // johnnytodo: config
    } else {
      tradePriceDecimal = hkdPriceDecimal * Decimal.parse("0.9629"); // johnnytodo: config
    }
    return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:2).format(tradePriceDecimal.toDouble());
  }

  @override
  Widget build(BuildContext context) {
    // The main column that holds everything
    return SafeArea(
        minimum:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.035),
        child: Column(
          children: <Widget>[
            // A row for the header of the sheet, balance text and close button
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Empty SizedBox
                SizedBox(
                  width: 60,
                  height: 60,
                ),

                // Container for the header, address and balance text
                Column(
                  children: <Widget>[
                    // Sheet handle
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 5,
                      width: MediaQuery.of(context).size.width * 0.15,
                      decoration: BoxDecoration(
                        color: StateContainer.of(context).curTheme.text10,
                        borderRadius: BorderRadius.circular(100.0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15.0),
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width - 140),
                      child: Column(
                        children: <Widget>[
                          // Header
                          AutoSizeText(
                            (widget.tradeMode == "BUY" ? "買入" : "賣出") // johnnyTranslate
                            + ' ' + widget.coinData.coinTicker, //johnny //CaseChange.toUpperCase(AppLocalization.of(context).tradeFrom, context),
                            style: AppStyles.textStyleHeader(context),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            stepGranularity: 0.1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //Empty SizedBox
                SizedBox(
                  width: 60,
                  height: 60,
                ),
              ],
            ),

            // label of your account name
            Container(
              margin: EdgeInsets.only(top: 10.0, left: 30, right: 30),
              child: Container(
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                    text: '',
                    children: [
                      TextSpan(
                        text: (widget.tradeMode == "BUY" ? "買入價:" : "賣出價:") // johnnyTranslate
                          +" \$" 
                          + _getTradePrice(widget.hkdPrice), 
                        style: TextStyle(
                          color: StateContainer.of(context).curTheme.text60,
                          fontSize: 22.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'NunitoSans',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            // A main container that holds everything
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 50, bottom: 5),
                child: Stack(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Clear focus of our fields when tapped in this empty space
                        _tradeCoinAmountFocusNode.unfocus();
                        _tradeFiatAmountFocusNode.unfocus();
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: SizedBox.expand(),
                        constraints: BoxConstraints.expand(),
                      ),
                    ),
                    // A column for Enter Amount, Enter Address, Error containers and the pop up list
                    Column(
                      children: <Widget>[

                        // Stack(
                        //   children: <Widget>[


                            // 1. Column for Balance Text, Enter Amount container + Enter Amount Error container
                            Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width * 0.105,
                                    right: MediaQuery.of(context).size.width * 0.105,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[ 
                                      Text('結餘:', // johnnyTranslate
                                        style: TextStyle(
                                          color: StateContainer.of(context).curTheme.primary60,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w700,
                                          fontFamily: 'NunitoSans',
                                        ),),
                                      FutureBuilder(
                                            future: sl.get<SharedPrefsUtil>().getPriceConversion(),
                                            builder: (BuildContext context, AsyncSnapshot snapshot) {
                                              if (snapshot.hasData &&
                                                  snapshot.data != null &&
                                                  snapshot.data != PriceConversion.HIDDEN) {
                                                return Container(
                                                  child: RichText(
                                                    textAlign: TextAlign.start,
                                                    text: TextSpan(
                                                      text: '',
                                                      children: [
                                                        TextSpan(
                                                          text: "",
                                                          style: TextStyle(
                                                            color: StateContainer.of(context).curTheme.primary60,
                                                            fontSize: 14.0,
                                                            fontWeight: FontWeight.w200,
                                                            fontFamily: 'NunitoSans',
                                                          ),
                                                        ),
                                                        TextSpan( 
                                                          text: widget.tradeMode == "BUY"
                                                              ? StateContainer.of(context).wallet.getBalance_HKD()
                                                              : coinAvaBalance,
                                                          style: TextStyle(
                                                            color: StateContainer.of(context).curTheme.primary60,
                                                            fontSize: 14.0,
                                                            fontWeight: FontWeight.w700,
                                                            fontFamily: 'NunitoSans',
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text: widget.tradeMode == "BUY"
                                                              ? ""
                                                              : " " + widget.coinData.coinTicker,
                                                          style: TextStyle(
                                                            color: StateContainer.of(context).curTheme.primary60,
                                                            fontSize: 14.0,
                                                            fontWeight: FontWeight.w500,
                                                            fontFamily: 'NunitoSans',
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Container(
                                                child: Text(
                                                  "*******",
                                                  style: TextStyle(
                                                    color: Colors.transparent,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.w100,
                                                    fontFamily: 'NunitoSans',
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                    ],
                                  ),
                                ),
                                
                                
                              ],
                            ),

                            twoInputWidgets(), // a column of two input fields
                            


                      ],
                    ),
                  ],
                ),
              ),
            ),


            //A column with "trade" buttons
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      // trade Button
                      AppButton.buildAppButton(
                          context,
                          AppButtonType.PRIMARY,
                          widget.tradeMode == "BUY" ? "買入 " : "賣出 ", // johnnyTranslate //AppLocalization.of(context).trade,
                          Dimens.BUTTON_TOP_DIMENS, onPressed: () async {

                            // Authenticate. johnnytodo: add back bio-authenticate later
                            
                            bool validRequest = _validateRequest();
                            if (validRequest) {
                              _showTradeingAnimation(context);
                              bool tradeDone = await _postTradeRequest();
                              if (tradeDone) {
                                Sheets.showAppHeightNineSheet(
                                  context: context,
                                  closeOnTap: true,
                                  removeUntilHome: true,
                                  widget: TradeCompleteSheet(
                                      fromAmt: widget.tradeMode == "BUY" ? _tradeFiatAmountController.text : _tradeCoinAmountController.text, 
                                      toAmt: widget.tradeMode == "BUY" ? _tradeCoinAmountController.text : _tradeFiatAmountController.text, 
                                      fromSymbol: widget.tradeMode == "BUY" ? "HKD" : widget.coinData.coinTicker, 
                                      toSymbol: widget.tradeMode == "BUY" ? widget.coinData.coinTicker : "HKD",
                                    ));

                                StateContainer.of(context).getUserBalance(); // johnny: update balance after each trade
                              } else {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              }
                              
                              
                            }
                          }),
                    ],
                  ),
                  
                ],
              ),
            ),
          ],
        ));
  }

  Widget fiatInputWidget() {
    return Column(
      children: <Widget>[
            // ******* Enter Amount Container ******* //
            getEnterFiatAmountContainer(),
            // ******* Enter Amount Container End ******* //

            // ******* Enter Amount Error Container ******* //
            Container(
              alignment: AlignmentDirectional(0, 0),
              margin: EdgeInsets.only(top: 3),
              child: Text(_fiatAmtValidationText,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.red, // StateContainer.of(context).curTheme.primary,
                    fontFamily: 'NunitoSans',
                    fontWeight: FontWeight.w600,
                  )),
            ),
            // ******* Enter Amount Error Container End ******* //

        ],
    );
  }

  Widget coinInputWidget() {
    return Column(
      children: <Widget>[
        
        // ******* Enter Amount Container ******* //
        getEnterCoinAmountContainer(),
        // ******* Enter Amount Container End ******* //

        // ******* Enter Amount Error Container ******* //
        Container(
          alignment: AlignmentDirectional(0, 0),
          margin: EdgeInsets.only(top: 3),
          child: Text(_coinAmtValidationText,
              style: TextStyle(
                fontSize: 14.0,
                color: Colors.red, // StateContainer.of(context).curTheme.primary,
                fontFamily: 'NunitoSans',
                fontWeight: FontWeight.w600,
              )),
        ),
        // ******* Enter Amount Error Container End ******* //
      ],
    );
  }

  Widget twoInputWidgets() {
    if (widget.tradeMode == "BUY") {
      return Column(
        children: [
          fiatInputWidget(),
          coinInputWidget()
        ],
      );
    } else {
      return Column(
        children: [
          coinInputWidget(),
          fiatInputWidget()
        ],
      );
    }
  }

  // Determine if this is a max trade or not by comparing balances
  bool _isFiatMax() {
    if (widget.tradeMode == "SELL") {
      return true;
    }
    // Sanitize commas
    if (_tradeFiatAmountController.text.isEmpty) {
      return false;
    }
    try {
      String textField = _tradeFiatAmountController.text;
      String maxBalance;
      // balance = StateContainer.of(context).wallet.getAccountBalanceDisplay().replaceAll(r",", "");
      maxBalance = getPureHkdBalance();

      // Convert to Integer representations
      int textFieldInt;
      int maxBalanceInt;
      textField = textField.replaceAll(",", "");
      textFieldInt = (Decimal.parse(textField) *
              Decimal.fromInt(pow(10, NumberUtil.maxDecimalDigits))).toInt();
      maxBalanceInt = (Decimal.parse(maxBalance) *
              Decimal.fromInt(pow(10, NumberUtil.maxDecimalDigits))).toInt();
      

      return textFieldInt == maxBalanceInt;
    } catch (e) {
      return false;
    }
  }

  bool _isCoinMax() {
    if (widget.tradeMode == "BUY") {
      return true;
    }
    // Sanitize commas
    if (_tradeCoinAmountController.text.isEmpty) {
      return false;
    }
    try {
      String textField = _tradeCoinAmountController.text;
      String maxBalance;
      // balance = StateContainer.of(context).wallet.getAccountBalanceDisplay().replaceAll(r",", "");
      maxBalance = coinAvaBalance;

      // Convert to Integer representations
      int textFieldInt;
      int maxBalanceInt;
      textField = textField.replaceAll(",", "");
      textFieldInt = (Decimal.parse(textField) *
              Decimal.fromInt(pow(10, NumberUtil.maxDecimalDigits))).toInt();
      maxBalanceInt = (Decimal.parse(maxBalance) *
              Decimal.fromInt(pow(10, NumberUtil.maxDecimalDigits))).toInt();
      
      return textFieldInt == maxBalanceInt;
    } catch (e) {
      return false;
    }
  }

  /// Validate form data to see if valid
  /// @returns true if valid, false otherwise
  bool _validateRequest() {
    bool isValid = true;
    _tradeFiatAmountFocusNode.unfocus();
    _tradeCoinAmountFocusNode.unfocus();

    Decimal inputFiatAmount = Decimal.parse(_tradeFiatAmountController.text);
    Decimal inputCoinAmount = Decimal.parse(_tradeCoinAmountController.text);
    
    if (widget.tradeMode == "BUY") {

      // Validate fiat amount, when buying
      if (_tradeFiatAmountController.text.trim().isEmpty) {
        isValid = false;
        setState(() {
          _fiatAmtValidationText = AppLocalization.of(context).amountMissing;
        });
      } else {
        Decimal fiatBalance = Decimal.parse(getPureHkdBalance());
        if (inputFiatAmount == null || inputFiatAmount == Decimal.zero) { // if input none
          isValid = false;
          setState(() {
            _fiatAmtValidationText = AppLocalization.of(context).amountMissing;
          });
        } else if (inputFiatAmount > fiatBalance) { // if not having enough balance
          isValid = false;
          setState(() {
            _fiatAmtValidationText = AppLocalization.of(context).insufficientBalance;
          });
        } else if (inputFiatAmount < Decimal.parse("200")) {
          isValid = false;
          setState(() {
            _fiatAmtValidationText = "交易額需為200 港元以上"; // johnnytodo: hardcode
          });
        }
      }

    } else { // Validate coin amount, when selling

      if (_tradeCoinAmountController.text.trim().isEmpty) {
        isValid = false;
        setState(() {
          _coinAmtValidationText = AppLocalization.of(context).amountMissing;
        });
      } else {
        Decimal coinBalance = Decimal.parse(coinAvaBalance);
        if (inputCoinAmount == null || inputCoinAmount == Decimal.zero) {
          isValid = false;
          setState(() {
            _coinAmtValidationText = AppLocalization.of(context).amountMissing;
          });
        } else if (inputCoinAmount > coinBalance) { // if not having enough balance
          isValid = false;
          setState(() {
            _coinAmtValidationText = AppLocalization.of(context).insufficientBalance;
          });
        } else if (inputFiatAmount < Decimal.parse("200")) {
          isValid = false;
          setState(() {
            _fiatAmtValidationText = "交易額需為200 港元以上"; // johnnytodo: hardcode
          });
        }
      }

    }
    
    return isValid;
  }

  //************ Enter Fiat Amount Container Method ************//
  //*******************************************************//
  getEnterFiatAmountContainer() {
    return AppTextField(
      focusNode: _tradeFiatAmountFocusNode,
      controller: _tradeFiatAmountController,
      topMargin: 5,
      cursorColor: StateContainer.of(context).curTheme.primary,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16.0,
        color: StateContainer.of(context).curTheme.primary,
        fontFamily: 'NunitoSans',
      ),
      inputFormatters: [
              LengthLimitingTextInputFormatter(13),
              CurrencyFormatter(
                      maxDecimalDigits: NumberUtil.maxDecimalDigits),
              LocalCurrencyFormatter(
                  active: false,
                  currencyFormat: _localCurrencyFormat)
            ],
      onChanged: (text) {
        // Always reset the error message to be less annoying
        setState(() {
          _fiatAmtValidationText = "";
          // Reset the raw amount
          // _rawAmount = null;

          // johnny: auto calculate corresponding coin amt
          if (_tradeFiatAmountController.text == "")
            _tradeCoinAmountController.text = "";
          else 
            _tradeCoinAmountController.text = getCoinAmount(_tradeFiatAmountController.text);

          // johnny: handle fiat amount > MAX for buying
          if (widget.tradeMode == "BUY") {
            Decimal hkdInput = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(_tradeFiatAmountController.text.replaceAll(",", ""));
            Decimal maxBal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(getPureHkdBalance());
            if (hkdInput > maxBal) {
              _tradeFiatAmountController.text = getPureHkdBalance();
              _tradeCoinAmountController.text = getCoinAmount(_tradeFiatAmountController.text);
            }
          } else {
            Decimal coinInput = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(_tradeCoinAmountController.text.replaceAll(",", ""));
            Decimal maxBal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(coinAvaBalance);
            if (coinInput > maxBal) {
              _tradeCoinAmountController.text =coinAvaBalance;
              _tradeFiatAmountController.text = getFiatAmount(_tradeCoinAmountController.text);
            }
          }
          
        });
      },
      textInputAction: TextInputAction.next,
      maxLines: null,
      autocorrect: false,
      hintText:
          _fiatAmtHint == null ? "" : '金額', // johnnyTranslate //AppLocalization.of(context).enterAmount,
      prefixButton: TextFieldButton(icon: Icons.attach_money),
      suffixButton: TextFieldButton(
        icon: AppIcons.max,
        onPressed: () {
          if (_isFiatMax()) {
            return;
          }

          _tradeFiatAmountController.text = widget.tradeMode == "BUY"
                  ? getPureHkdBalance()
                  : coinAvaBalance;
          _tradeCoinAmountController.selection = TextSelection.fromPosition(
              TextPosition(offset: _tradeCoinAmountController.text.length));

          // johnny: auto calculate corresponding coin amt
          if (_tradeFiatAmountController.text.isEmpty)
            _tradeCoinAmountController.text = "";
          else 
            _tradeCoinAmountController.text = getCoinAmount(_tradeFiatAmountController.text);

        },
      ),
      fadeSuffixOnCondition: true,
      suffixShowFirstCondition: !_isFiatMax(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      textAlign: TextAlign.center,
      onSubmitted: (text) {
        FocusScope.of(context).unfocus();
        if (!Address(_tradeCoinAmountController.text).isValid()) {
          FocusScope.of(context).requestFocus(_tradeCoinAmountFocusNode);
        }
      },
    );
  } //************ Enter Address Container Method End ************//
  //*************************************************************//




//************ Enter Coin-Amount Container Method ************//
  //*******************************************************//
  getEnterCoinAmountContainer() {
    return AppTextField(
      focusNode: _tradeCoinAmountFocusNode,
      controller: _tradeCoinAmountController,
      topMargin: 15,
      cursorColor: StateContainer.of(context).curTheme.primary,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16.0,
        color: StateContainer.of(context).curTheme.primary,
        fontFamily: 'NunitoSans',
      ),
      inputFormatters: [
              LengthLimitingTextInputFormatter(13),
              CurrencyFormatter(maxDecimalDigits: NumberUtil.maxDecimalDigits),
              LocalCurrencyFormatter(
                  active: false,
                  currencyFormat: _localCurrencyFormat)
            ],
      onChanged: (text) {
        // Always reset the error message to be less annoying
        setState(() {
          _coinAmtValidationText = "";
          // Reset the raw amount
          // _rawAmount = null;

          // johnny: auto calculate corresponding fiat amt
          if (_tradeCoinAmountController.text.isEmpty)
            _tradeFiatAmountController.text = "";
          else 
            _tradeFiatAmountController.text = getFiatAmount(_tradeCoinAmountController.text);

          // johnny: handle fiat amount > MAX for buying
          if (widget.tradeMode == "BUY") {
            Decimal hkdInput = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(_tradeFiatAmountController.text.replaceAll(",", ""));
            Decimal maxBal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(getPureHkdBalance());
            if (hkdInput > maxBal) {
              _tradeFiatAmountController.text = getPureHkdBalance();
              _tradeCoinAmountController.text = getCoinAmount(_tradeFiatAmountController.text);
            }
          } else {
            Decimal coinInput = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(_tradeCoinAmountController.text.replaceAll(",", ""));
            Decimal maxBal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(coinAvaBalance);
            if (coinInput > maxBal) {
              _tradeCoinAmountController.text =coinAvaBalance;
              _tradeFiatAmountController.text = getFiatAmount(_tradeCoinAmountController.text);
            }
          }

        });
      },
      textInputAction: TextInputAction.next,
      maxLines: null,
      autocorrect: false,
      hintText:
          _coinAmtHint == null ? "" : widget.coinData.coinTicker+' 數量',  // johnnyTranslate // AppLocalization.of(context).enterAmount,
      prefixButton: TextFieldButton(icon: widget.coinData.icon), 
      suffixButton: TextFieldButton(
        icon: AppIcons.max,
        onPressed: () {
          if (_isCoinMax()) {
            return;
          }

          _tradeCoinAmountController.text = widget.tradeMode == "BUY"
                  ? getPureHkdBalance()
                  : coinAvaBalance;
          _tradeCoinAmountController.selection = TextSelection.fromPosition(
              TextPosition(offset: _tradeCoinAmountController.text.length));

          // johnny: auto calculate corresponding coin amt
          if (_tradeCoinAmountController.text.isEmpty)
            _tradeFiatAmountController.text = "";
          else 
            _tradeFiatAmountController.text = getFiatAmount(_tradeCoinAmountController.text);
          
        },
      ),
      fadeSuffixOnCondition: true,
      suffixShowFirstCondition: !_isCoinMax(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      textAlign: TextAlign.center,
      onSubmitted: (text) {
        FocusScope.of(context).unfocus();
        if (!Address(_tradeCoinAmountController.text).isValid()) {
          FocusScope.of(context).requestFocus(_tradeCoinAmountFocusNode);
        }
      },
    );
  } //************ Enter Coin-Amount Container Method End ************//
  //*************************************************************//

Future<bool> _postTradeRequest() async {

    var tokenData = {
      'data': {
        'token':         StateContainer.of(context).wallet.loginToken,
        'coinName' :     widget.coinData.coinTicker,
        'action':        widget.tradeMode.toLowerCase(),
        'hkdAmount':      _tradeFiatAmountController.text,
        'coinAmount':     _tradeCoinAmountController.text,
        'price':         _getTradePrice(widget.hkdPrice).replaceAll(",", "").toString()
      }
    };

    // print('action:' + widget.tradeMode.toLowerCase()); // johnnyworking: why buy has no DB record for mobile
    // print(tokenData);

    Response response;
    Dio dio2 = new Dio();
    response = await dio2.post(
        // 'https://uat-bitginko.standardkepler.com/api/v4/uri/tradeCoinV2.api.php', // UAT 
        'https://bitginko.com/api/v4/uri/mobile.tradeCoinV2.api.php', // PRD
        data: FormData.fromMap(tokenData),
        options: Options(responseType: ResponseType.plain,));

    // var returnObj = jsonDecode(response.toString());
    // print(response.toString());
    print("=> Success send trade request");

    return true; // johnnytodo: see if need bool here later
  }
  

  String getCoinAmount(String fiatAmt) {
    Decimal converted = Decimal.parse(fiatAmt) / NumberUtil.getRawAsUsableDecimal(_getTradePrice(widget.hkdPrice).replaceAll(",", "").toString());
    // Show 4 decimal places
    return new NumberFormat("###0.0000", "en_US").format(converted.toDouble());
  }

  String getFiatAmount(String coinAmt) {
    Decimal converted = Decimal.parse(coinAmt) * NumberUtil.getRawAsUsableDecimal(_getTradePrice(widget.hkdPrice).replaceAll(",", "").toString());
    // Show 4 decimal places
    return new NumberFormat("###0.0000", "en_US").format(converted.toDouble());
  }

  String getPureHkdBalance() {
    return StateContainer.of(context).wallet.getBalance_HKD().replaceAll(r",", "").replaceAll(r"HKD ", "");
  }

  void _showTradeingAnimation(BuildContext context) {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.SEND,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }

}
