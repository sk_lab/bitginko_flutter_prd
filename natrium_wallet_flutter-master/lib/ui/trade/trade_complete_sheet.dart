import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:manta_dart/messages.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';

// johnny: step2 of trading
class TradeCompleteSheet extends StatefulWidget {
  final String fromAmt;    
  final String toAmt; 
  final String fromSymbol;
  final String toSymbol;

  TradeCompleteSheet({this.fromAmt, this.toAmt, this.fromSymbol, this.toSymbol}) : super();  

  _TradeCompleteSheetState createState() => _TradeCompleteSheetState();
}

class _TradeCompleteSheetState extends State<TradeCompleteSheet> {
  String amount;
  String convertedAmt;
  bool isMantaTransaction;

  @override
  void initState() {
    super.initState();
    // Indicate that this is a special amount if some digits are not displayed
    // if (NumberUtil.getRawAsUsableString(widget.fromAmt).replaceAll(",", "") ==
    //     NumberUtil.getRawAsUsableDecimal(widget.fromAmt).toString()) {
    //   amount = NumberUtil.getRawAsUsableString(widget.fromAmt);
    // } else {
    //   amount = NumberUtil.truncateDecimal(
    //               NumberUtil.getRawAsUsableDecimal(widget.amountRaw),
    //               digits: 6)
    //           .toStringAsFixed(6) +
    //       "~";
    // }

    amount = widget.fromAmt;
    convertedAmt = widget.toAmt;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        minimum: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.035),
        child: Column(
          children: <Widget>[
            // Sheet handle
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 5,
              width: MediaQuery.of(context).size.width * 0.15,
              decoration: BoxDecoration(
                color: StateContainer.of(context).curTheme.text10,
                borderRadius: BorderRadius.circular(100.0),
              ),
            ),
            //A main container that holds the amount, address and "SENT TO" texts
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Success tick (icon)
                  Container(
                    alignment: AlignmentDirectional(0, 0),
                    margin: EdgeInsets.only(bottom: 25),
                    child: Icon(AppIcons.success,
                        size: 100,
                        color: StateContainer.of(context)
                            .curTheme
                            .success),
                  ),
                  // 1. Container for the fromAmount Text
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.105,
                        right:
                            MediaQuery.of(context).size.width * 0.105),
                    padding: EdgeInsets.symmetric(
                        horizontal: 25, vertical: 15),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: StateContainer.of(context)
                          .curTheme
                          .backgroundDarkest,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    // From Amount text
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: '',
                        children: [
                          TextSpan(
                            text: widget.fromAmt,
                            style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .success,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'NunitoSans',
                            ),
                          ),
                          TextSpan(
                            text: " "+ widget.fromSymbol,
                            style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .success,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'NunitoSans',
                            ),
                          ),
                          // TextSpan(
                          //   text: "",
                          //   style: TextStyle(
                          //     color: StateContainer.of(context)
                          //         .curTheme
                          //         .success,
                          //     fontSize: 16.0,
                          //     fontWeight: FontWeight.w700,
                          //     fontFamily: 'NunitoSans',
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                  // 2. Container for the "CONVERTED TO" text
                  Container(
                    margin: EdgeInsets.only(top: 30, bottom: 30),
                    child: Column(
                      children: <Widget>[
                        // "SENT TO" text
                        Text(
                          CaseChange.toUpperCase(
                              "已成功兌換", // johnnyTranslate //AppLocalization.of(context).sentTo,
                              context),
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700,
                            color: StateContainer.of(context)
                                .curTheme
                                .success,
                            fontFamily: 'NunitoSans',
                          ),
                        ),
                      ],
                    ),
                  ),

                  // 3. the toAmt container 
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.105,
                        right:
                            MediaQuery.of(context).size.width * 0.105),
                    padding: EdgeInsets.symmetric(
                        horizontal: 25, vertical: 15),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: StateContainer.of(context)
                          .curTheme
                          .backgroundDarkest,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    // Amount text
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: '',
                        children: [
                          TextSpan(
                            text: widget.toAmt,
                            style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .success,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'NunitoSans',
                            ),
                          ),
                          TextSpan(
                            text: " "+ widget.toSymbol,
                            style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .success,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'NunitoSans',
                            ),
                          ),
                          // TextSpan(
                          //   text: "",
                          //   style: TextStyle(
                          //     color: StateContainer.of(context)
                          //         .curTheme
                          //         .success,
                          //     fontSize: 16.0,
                          //     fontWeight: FontWeight.w700,
                          //     fontFamily: 'NunitoSans',
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                  // // The container for the address
                  // Container(
                  //     padding: EdgeInsets.symmetric(
                  //         horizontal: 25.0, vertical: 15.0),
                  //     margin: EdgeInsets.only(
                  //         left:
                  //             MediaQuery.of(context).size.width * 0.105,
                  //         right: MediaQuery.of(context).size.width *
                  //             0.105),
                  //     width: double.infinity,
                  //     decoration: BoxDecoration(
                  //       color: StateContainer.of(context)
                  //           .curTheme
                  //           .backgroundDarkest,
                  //       borderRadius: BorderRadius.circular(25),
                  //     ),
                  //     child: UIUtil.twoLineAddressText(
                  //             context, convertedAmt,
                  //             type: TwoLineAddressTextType.SUCCESS)),
                ],
              ),
            ),

            // CLOSE Button
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      AppButton.buildAppButton(
                          context,
                          AppButtonType.SUCCESS_OUTLINE,
                          CaseChange.toUpperCase(
                              AppLocalization.of(context).close,
                              context),
                          Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                        Navigator.of(context).pop();
                      }),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      );    
  }
}

