
import 'dart:convert';

import 'package:decimal/decimal.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:natrium_wallet_flutter/ui/buy_coin_page.dart';
import 'package:natrium_wallet_flutter/ui/popup_button.dart';
import 'package:natrium_wallet_flutter/ui/receive/receive_sheet.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:qr_flutter/qr_flutter.dart';

class AssetDetail extends StatefulWidget {
  CoinData coinData; // johnny
  String hkdPrice;
  String priceChange;
  bool priceChangePositive;

  AssetDetail({this.coinData, this.hkdPrice, this.priceChange, this.priceChangePositive}); // johnny

  @override
  _AssetDetailState createState() => _AssetDetailState();
}

class _AssetDetailState extends State<AssetDetail> {
  FocusNode enterPasswordFocusNode;
  TextEditingController enterPasswordController;

  String passwordError;
  bool doneLoadingAddr = false;

  AppReceiveSheet receive;

  @override
  void initState() {
    super.initState();
    this.enterPasswordFocusNode = FocusNode();
    this.enterPasswordController = TextEditingController();

    // print('coin id: ' + widget.coinSymbol);
    Future.delayed(new Duration(seconds: 1), () { 
      // _refreshBalance();
      _loadDepositAddr(); // johnnyworking: disable the deposit button until finished loading data
      // setState(() {
      //   _isRefreshing = false;
      // });
    });

  }

  Future<void> _loadDepositAddr() async {

      var tokenData = {
        'data': {
          'token': StateContainer.of(context).wallet.loginToken,
          'coin_id': widget.coinData.coinId //widget.coinData // load the correct coin id
        }
      };
      
      Response response;
      Dio dio = new Dio();
      response = await dio.post(
          // 'https://uat-bitginko.standardkepler.com/api/v4/uri/getUserAddress.api.php', // UAT
          'https://bitginko.com/api/v4/uri/getUserAddress.api.php', // PRD
          data: FormData.fromMap(tokenData),
          options: Options(responseType: ResponseType.plain,));

      var returnObj = jsonDecode(response.toString());
      print("[debug] assest detail page return obj: ");
      print(returnObj);

      String depositAddr = "";

      if (returnObj['StatusCode'] == 0) {
        depositAddr = returnObj['Result']['address'];
        // print("depositAddr:");
        // print(depositAddr);
      } else {
        depositAddr = await _getNewDepositAddr();
      }
      

      StateContainer.of(context).updateWalletAddressBitginko(address: depositAddr);

      // print('build paintQrCode A1');
      paintQrCode(address: depositAddr);
      // receive = null;
      // print('build paintQrCode A2');
      doneLoadingAddr = true;

  }

  Future<String> _getNewDepositAddr() async {

      var tokenData = {
        'data': {
          'token': StateContainer.of(context).wallet.loginToken,
          'coin_id': widget.coinData.coinId //widget.coinData // load the correct coin id
        }
      };
      
      Response response;
      Dio dio = new Dio();
      response = await dio.post(
          // 'https://uat-bitginko.standardkepler.com/api/v4/uri/getUserAddress.api.php', // UAT
          'https://bitginko.com/api/v4/uri/assignUserAddress.api.php', // PRD
          data: FormData.fromMap(tokenData),
          options: Options(responseType: ResponseType.plain,));

      var returnObj = jsonDecode(response.toString());
      print("[debug] assignUserAddress return obj: ");
      print(returnObj);

      String depositAddr = returnObj['Result']['address'];
      return depositAddr;
      // print("new depositAddr:");
      // print(depositAddr);
  }

  void paintQrCode({String address}) {
    String tmp = address == null ? StateContainer.of(context).wallet.address : address;
    print(tmp);

    QrPainter painter = QrPainter(
      data:
          address == null ? StateContainer.of(context).wallet.address : address,
      version: 6,
      gapless: false,
      errorCorrectionLevel: QrErrorCorrectLevel.Q,
    );
    painter.toImageData(MediaQuery.of(context).size.width).then((byteData) {
      setState(() {
        receive = AppReceiveSheet(
          Container(
              width: MediaQuery.of(context).size.width / 2.675,
              child: Image.memory(byteData.buffer.asUint8List())),
        );

        print('done setState painter');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // var expanded = Expanded;
    if (receive == null) { // johnny: removed ' && StateContainer.of(context).wallet != null '
      print('build paintQrCode B1');
      paintQrCode();
      print('build paintQrCode B2');
    }
    print('build assetsDetail ==================');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
        brightness: StateContainer.of(context).curTheme.brightness,
        iconTheme: IconThemeData(color: StateContainer.of(context).curTheme.text),
        // leading: FlatButton(icon:Icon(Icons.arrow_back_ios), onPressed:() => Navigator.pop(context, false),),
        title: Text(
          '', // johnnyTranslate
          style: TextStyle(color: StateContainer.of(context).curTheme.text),),
      ),
      resizeToAvoidBottomInset: false,
      body: Container(
          color: StateContainer.of(context).curTheme.background,
          width: double.infinity,
          child: SafeArea(
            minimum: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.035,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[

                
                Container(
                  // padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 20.0),
                  child: Column(
                    children: <Widget>[
                      // icon and name
                      Container(
                        margin: EdgeInsetsDirectional.only(start: 20, end: 20, top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // row1
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              margin: EdgeInsetsDirectional.only(end: 16.0),
                              child: Icon(
                                widget.coinData.icon, // johnny
                                color: StateContainer.of(context).curTheme.primary, size: 30),
                            ),
                          
                            // row2
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    widget.coinData.coinTicker, 
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w900,
                                      color: StateContainer.of(context).curTheme.text),
                                  ),
                                  Text(
                                    widget.coinData.coinName, 
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      color: StateContainer.of(context).curTheme.primary60,
                                      fontSize: AppFontSizes.smallest,
                                      fontWeight: FontWeight.w600)
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      

                      // c1 - total and available
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        margin: EdgeInsetsDirectional.only(top: 45, bottom: 15, start: 20, end: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '可用資產', // johnnyTranslate
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: StateContainer.of(context).curTheme.text60),
                                  ),
                                  Text(
                                    '凍結', // johnnyTranslate
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: StateContainer.of(context).curTheme.text60)
                                  ),
                                ],
                              ),
                            ),
                            
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    widget.coinData.avaBalance,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w900,
                                      color: StateContainer.of(context).curTheme.text),
                                  ),
                                  Text(
                                    widget.coinData.freezeBalance,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w900,
                                      color: StateContainer.of(context).curTheme.text)
                                  ),
                                ]
                              ),
                            ),
                          
                        ],),
                      ),
                      
                      // c2 - separator
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 20.0),
                        child: Divider(
                          height: 2,
                          color: StateContainer.of(context).curTheme.text60,
                        ),
                      ),
                      
                      // c3 - equity value
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        margin: EdgeInsetsDirectional.only(top: 15, bottom: 40, start: 20, end: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '總值', // johnnyTranslate
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: StateContainer.of(context).curTheme.text60),
                                  ),
                                ],
                              ),
                            ),
                            
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    widget.coinData.totalBalance,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w900,
                                      color: StateContainer.of(context).curTheme.text),
                                  ),
                                ]
                              ),
                            ),
                          
                        ],),
                      ),

                      // c4 - the gap area
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: 15.0,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: StateContainer.of(context).curTheme.backgroundDark,
                          ),
                        ),
                      ), 

                      // c5 - go to trade
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        margin: EdgeInsetsDirectional.only(top: 30, bottom: 15, start: 20, end: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '去交易', // johnnyTranslate
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: StateContainer.of(context).curTheme.text60),
                                  ),
                                ],
                              ),
                            ),
                        ],),
                      ),

                      // c6 - trade button
                      Container(
                        margin: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 20.0, 4.0),
                        decoration: BoxDecoration(
                          color: StateContainer.of(context).curTheme.backgroundDark,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [StateContainer.of(context).curTheme.boxShadow],
                        ),
                        child: FlatButton(
                          onPressed: () {
                            // return null;
                            Navigator.of(context).pop();

                            Navigator.push(context,
                              MaterialPageRoute(builder: (context) => BuyCoinPage(coinData: widget.coinData))); 
                          },
                          highlightColor: StateContainer.of(context).curTheme.text15,
                          splashColor: StateContainer.of(context).curTheme.text15,
                          color: StateContainer.of(context).curTheme.backgroundDark,
                          padding: EdgeInsets.all(0.0),
                          shape:
                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                          child: Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 14.0, horizontal: 20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      //icon
                                      Container(
                                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                                        margin: EdgeInsetsDirectional.only(end: 16.0),
                                        child: Icon(widget.coinData.icon,
                                            color: StateContainer.of(context).curTheme.primary, 
                                            size: 20)
                                      ),
                                      //text
                                      Container(
                                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                                        width: MediaQuery.of(context).size.width / 2,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              widget.coinData.coinTicker +' / HKD',
                                              textAlign: TextAlign.start,
                                              style: AppStyles.textStyleTransactionType(context),
                                            ),
                                            // RichText(
                                            //   textAlign: TextAlign.start,
                                            //   text: TextSpan(
                                            //     text: '',
                                            //     children: [
                                            //       TextSpan(
                                            //         text: "~HKD " + widget.hkdWorth + " (" + widget.priceChangePositive
                                            //         style: AppStyles.textStyleTransactionAmount(context),
                                            //       ),
                                            //     ],
                                            //   ),
                                            // ),
                                            Container(
                                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: Row(
                                                children: <Widget>[
                                                  Text(
                                                    "\$ " + widget.hkdPrice,
                                                    textAlign: TextAlign.start,
                                                    style: AppStyles.textStyleTransactionAmount(context)
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                    child: Text(
                                                      " ("+ widget.priceChange +"%)",
                                                      textAlign: TextAlign.start,
                                                      style: TextStyle(
                                                        fontFamily: "NunitoSans",
                                                        fontSize: 10,
                                                        fontWeight: FontWeight.w500,
                                                        color: widget.priceChangePositive ? Colors.green : Colors.redAccent,//StateContainer.of(context).curTheme.text,
                                                      ),
                                                    ),
                                                  )
                                              ],)
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                                    width: MediaQuery.of(context).size.width / 5,
                                    child : Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                              '交易', // johnnyTranslate
                                              textAlign: TextAlign.end,
                                              style: AppStyles.textStyleTransactionType(context),
                                            ),
                                            // Text(
                                            //   "~ \$2,010,289.11",
                                            //   textAlign: TextAlign.end,
                                            //   style: AppStyles.textStyleTransactionAmount(context),
                                            // ),
                                          ],
                                        ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),

                    ],
                  )
                ),
                
                


                // column #3
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                  child: Container(
                    // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                    // margin: EdgeInsetsDirectional.only(top: 150),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[

                        // button left (Deposit)
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [StateContainer.of(context).curTheme.boxShadowButton],
                          ),
                          height: 40,
                          width: (MediaQuery.of(context).size.width - 64) / 2,
                          // margin: EdgeInsetsDirectional.only(start: 6, top: 0.0, end: 6.0),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            color: Colors.greenAccent, //StateContainer.of(context).curTheme.primary,
                            child: AutoSizeText(
                              '存入', // johnnyTranslate
                              textAlign: TextAlign.center,
                              style: AppStyles.textStyleButtonPrimary(context),
                              maxLines: 1,
                              stepGranularity: 0.5,
                            ),
                            onPressed: () {
                              // if (receive == null) {
                              //   return;
                              // }
                              if (doneLoadingAddr)
                                receive.mainBottomSheet(context, coinSymbol: widget.coinData.coinTicker, coinIcon: widget.coinData.icon);
                              else {
                                // johnnytodo: add loading info or animation
                              }
                            },
                            highlightColor: receive != null
                                ? StateContainer.of(context).curTheme.background40
                                : Colors.transparent,
                            splashColor: receive != null
                                ? StateContainer.of(context).curTheme.background40
                                : Colors.transparent,
                          ),
                        ),

                        // button right (withdraw/send)
                        // AppPopupButton(coinSymbol: widget.coinData.coinTicker),
                        AppPopupButton(coinData: widget.coinData),
                        // Container(
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(5),
                        //     boxShadow: [StateContainer.of(context).curTheme.boxShadowButton],
                        //   ),
                        //   height: 40,
                        //   width: (MediaQuery.of(context).size.width - 58) / 2,
                        //   // margin: EdgeInsetsDirectional.only(start: 14, top: 0.0, end: 7.0),
                        //   child: FlatButton(
                        //     shape: RoundedRectangleBorder(
                        //         borderRadius: BorderRadius.circular(5.0)),
                        //     color: Colors.redAccent, //StateContainer.of(context).curTheme.primary,
                        //     child: AutoSizeText(
                        //       'Transfer',
                        //       textAlign: TextAlign.center,
                        //       style: AppStyles.textStyleButtonPrimary(context),
                        //       maxLines: 1,
                        //       stepGranularity: 0.5,
                        //     ),
                        //     onPressed: () {
                        //       // if (receive == null) {
                        //       //   return;
                        //       // }
                        //       // receive.mainBottomSheet(context);
                        //     },
                        //     // highlightColor: receive != null
                        //     //     ? StateContainer.of(context).curTheme.background40
                        //     //     : Colors.transparent,
                        //     // splashColor: receive != null
                        //     //     ? StateContainer.of(context).curTheme.background40
                        //     //     : Colors.transparent,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                )

                // Expanded(child: Container(color: Colors.red,),),

                // Expanded(child: Container(color: Colors.blue,),),

                

              ],
            ),
          ),
        )
      
    );
  }


}

String _get4decimalsNum(String bal) {
    Decimal converted = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(bal);
    return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(converted.toDouble());
  }
 