import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:decimal/decimal.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:event_taxi/event_taxi.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:manta_dart/manta_wallet.dart';
import 'package:manta_dart/messages.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';
import 'package:natrium_wallet_flutter/model/price_model.dart';
import 'package:natrium_wallet_flutter/ui/popup_button.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/model/address.dart';
import 'package:natrium_wallet_flutter/model/list_model.dart';
import 'package:natrium_wallet_flutter/model/db/contact.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/network/model/block_types.dart';
import 'package:natrium_wallet_flutter/network/model/response/account_history_response_item.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/ui/contacts/add_contact.dart';
import 'package:natrium_wallet_flutter/ui/send/send_sheet.dart';
import 'package:natrium_wallet_flutter/ui/send/send_confirm_sheet.dart';
import 'package:natrium_wallet_flutter/ui/send/send_complete_sheet.dart';
import 'package:natrium_wallet_flutter/ui/receive/receive_sheet.dart';
import 'package:natrium_wallet_flutter/ui/settings/settings_drawer.dart';
import 'package:natrium_wallet_flutter/ui/trade_button.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_drawer.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_scaffold.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/widgets/fl_line_chart_big.dart';
import 'package:natrium_wallet_flutter/ui/widgets/line_chart.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/list_slidable.dart';
import 'package:natrium_wallet_flutter/ui/widgets/bar_chart.dart';
import 'package:natrium_wallet_flutter/ui/widgets/bitginko_datatable.dart';
import 'package:natrium_wallet_flutter/ui/util/routes.dart';
import 'package:natrium_wallet_flutter/ui/widgets/reactive_refresh.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/util/manta.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'package:natrium_wallet_flutter/util/hapticutil.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';

import 'package:qr_flutter/qr_flutter.dart';
import 'package:natrium_wallet_flutter/bus/events.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class BuyCoinPage extends StatefulWidget {
  CoinData coinData; 

  BuyCoinPage({this.coinData}) : super();

  @override
  _BuyCoinPageState createState() => _BuyCoinPageState();
}

class _BuyCoinPageState extends State<BuyCoinPage>
    with
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin
        // FlareController 
{
  final GlobalKey<AppScaffoldState> _scaffoldKey =
      new GlobalKey<AppScaffoldState>();
  final Logger log = sl.get<Logger>();

  // Controller for placeholder card animations
  AnimationController _placeholderCardAnimationController;
  Animation<double> _opacityAnimation;
  bool _animationDisposed;

  // Manta
  bool mantaAnimationOpen;

  // Receive card instance
  AppReceiveSheet receive;

  // A separate unfortunate instance of this list, is a little unfortunate
  // but seems the only way to handle the animations
  final Map<String, GlobalKey<AnimatedListState>> _listKeyMap = Map();
  final Map<String, ListModel<AccountHistoryResponseItem>> _historyListMap = Map();

  // List of contacts (Store it so we only have to query the DB once for transaction cards)
  // List<Contact> _contacts = List();

  // Price conversion state (BTC, NANO, NONE)
  // PriceConversion _priceConversion;

  bool _isRefreshing = false;

  bool _lockDisabled = false; // whether we should avoid locking the app

  // Main card height
  double mainCardHeight;
  double settingsIconMarginTop = 5;
  double chartCardHeight;
  double tableCardHeight;
  // FCM instance
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // // Animation for swiping to send
  // ActorAnimation _sendSlideAnimation;
  // ActorAnimation _sendSlideReleaseAnimation;
  // double _fanimationPosition;
  // bool releaseAnimation = false;

  // void initialize(FlutterActorArtboard actor) {
  //   _fanimationPosition = 0.0;
  //   _sendSlideAnimation = actor.getAnimation("pull");
  //   _sendSlideReleaseAnimation = actor.getAnimation("release");
  // }

  // void setViewTransform(Mat2D viewTransform) {}

  // bool advance(FlutterActorArtboard artboard, double elapsed) {
  //   if (releaseAnimation) {
  //     _sendSlideReleaseAnimation.apply(
  //         _sendSlideReleaseAnimation.duration * (1 - _fanimationPosition),
  //         artboard,
  //         1.0);
  //   } else {
  //     _sendSlideAnimation.apply(
  //         _sendSlideAnimation.duration * _fanimationPosition, artboard, 1.0);
  //   }
  //   return true;
  // }

  /// Notification includes which account its for, automatically switch to it if they're entering app from notification
  Future<void> _chooseCorrectAccountFromNotification(
      Map<String, dynamic> message) async {
    // TODO repair this method
    return;
    /*
    try {
      if (message.containsKey("account")) {
        Account selectedAccount = await sl.get<DBHelper>().getSelectedAccount();
        if (message['account'] != selectedAccount.address) {
          List<Account> accounts = await sl.get<DBHelper>().getAccounts();
          for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].address == message['account']) {
              await sl.get<DBHelper>().changeAccount(accounts[i]);
              EventTaxiImpl.singleton()
                  .fire(AccountChangedEvent(account: accounts[i]));
              break;
            }
          }
        }
      }
    } catch (e) {
      log.severe(e.toString());
    }*/
  }

  @override
  void initState() {
    super.initState();
    _registerBus();
    this.mantaAnimationOpen = false;
    WidgetsBinding.instance.addObserver(this);
    // if (widget.priceConversion != null) {
    //   _priceConversion = widget.priceConversion;
    // } else {
    //   _priceConversion = PriceConversion.BTC;  
    // }
    // Main Card Size
    // if (_priceConversion == PriceConversion.BTC) {
    //   mainCardHeight = 120;
    //   chartCardHeight = 200;
    //   tableCardHeight = 200;
    //   settingsIconMarginTop = 7;
    // } else if (_priceConversion == PriceConversion.NONE) {
    //   mainCardHeight = 64;
    //   chartCardHeight = 200;
    //   tableCardHeight = 200;
    //   settingsIconMarginTop = 7;
    // } else if (_priceConversion == PriceConversion.HIDDEN) {
    //   mainCardHeight = 64;
    //   chartCardHeight = 200;
    //   tableCardHeight = 200;
    //   settingsIconMarginTop = 5;
    // }
    _addSampleContact();
    // _updateContacts();
    // Setup placeholder animation and start
    _animationDisposed = false;
    _placeholderCardAnimationController = new AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    _placeholderCardAnimationController
        .addListener(_animationControllerListener);
    _opacityAnimation = new Tween(begin: 1.0, end: 0.4).animate(
      CurvedAnimation(
        parent: _placeholderCardAnimationController,
        curve: Curves.easeIn,
        reverseCurve: Curves.easeOut,
      ),
    );
    _opacityAnimation.addStatusListener(_animationStatusListener);
    _placeholderCardAnimationController.forward();

    // johnnytodo: add loading icon when landing the page
    Future.delayed(new Duration(seconds: 1), () { // johnnytodo: copy this loading logic to other pages
      _refreshPrice();

      // // runs every 5 second
      // Timer.periodic(new Duration(seconds: 5), (timer) {
      //   _refreshPrice();
      // });

      // setState(() {
      //   _isRefreshing = false;
      // });
    });

    // Register push notifications
    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     //print("onMessage: $message");
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     _chooseCorrectAccountFromNotification(message);
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     _chooseCorrectAccountFromNotification(message);
    //   },
    // );
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(sound: true, badge: true, alert: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   if (settings.alert || settings.badge || settings.sound) {
    //     sl.get<SharedPrefsUtil>().getNotificationsSet().then((beenSet) {
    //       if (!beenSet) {
    //         sl.get<SharedPrefsUtil>().setNotificationsOn(true);
    //       }
    //     });
    //     _firebaseMessaging.getToken().then((String token) {
    //       if (token != null) {
    //         EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //       }
    //     });
    //   } else {
    //     sl.get<SharedPrefsUtil>().setNotificationsOn(false).then((_) {
    //       _firebaseMessaging.getToken().then((String token) {
    //         EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //       });
    //     });
    //   }
    // });
    // _firebaseMessaging.getToken().then((String token) {
    //   if (token != null) {
    //     EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //   }
    // });
  }

  void _animationStatusListener(AnimationStatus status) {
    switch (status) {
      case AnimationStatus.dismissed:
        _placeholderCardAnimationController.forward();
        break;
      case AnimationStatus.completed:
        _placeholderCardAnimationController.reverse();
        break;
      default:
        return null;
    }
  }

  void _animationControllerListener() {
    // setState(() {});
  }

  void _startAnimation() {
    if (_animationDisposed) {
      _animationDisposed = false;
      _placeholderCardAnimationController
          .addListener(_animationControllerListener);
      _opacityAnimation.addStatusListener(_animationStatusListener);
      _placeholderCardAnimationController.forward();
    }
  }

  void _disposeAnimation() {
    if (!_animationDisposed) {
      _animationDisposed = true;
      _opacityAnimation.removeStatusListener(_animationStatusListener);
      _placeholderCardAnimationController
          .removeListener(_animationControllerListener);
      _placeholderCardAnimationController.stop();
    }
  }

  /// Add donations contact if it hasnt already been added
  Future<void> _addSampleContact() async {
    bool contactAdded = await sl.get<SharedPrefsUtil>().getFirstContactAdded();
    if (!contactAdded) {
      bool addressExists = await sl.get<DBHelper>().contactExistsWithAddress(
          "nano_1natrium1o3z5519ifou7xii8crpxpk8y65qmkih8e8bpsjri651oza8imdd");
      if (addressExists) {
        return;
      }
      bool nameExists =
          await sl.get<DBHelper>().contactExistsWithName("@NatriumDonations");
      if (nameExists) {
        return;
      }
      await sl.get<SharedPrefsUtil>().setFirstContactAdded(true);
      Contact c = Contact(
          name: "@NatriumDonations",
          address:
              "nano_1natrium1o3z5519ifou7xii8crpxpk8y65qmkih8e8bpsjri651oza8imdd");
      await sl.get<DBHelper>().saveContact(c);
    }
  }

  // void _updateContacts() {
  //   sl.get<DBHelper>().getContacts().then((contacts) {
  //     setState(() {
  //       _contacts = contacts;
  //     });
  //   });
  // }

  StreamSubscription<HistoryHomeEvent> _historySub;
  StreamSubscription<ContactModifiedEvent> _contactModifiedSub;
  StreamSubscription<SendCompleteEvent> _sendCompleteSub;
  StreamSubscription<DisableLockTimeoutEvent> _disableLockSub;
  StreamSubscription<AccountChangedEvent> _switchAccountSub;

  void _registerBus() {
    _historySub = EventTaxiImpl.singleton()
        .registerTo<HistoryHomeEvent>()
        .listen((event) {
      diffAndUpdateHistoryList(event.items);
      setState(() {
        _isRefreshing = false;
      });
      if (StateContainer.of(context).initialDeepLink != null) {
        handleDeepLink(StateContainer.of(context).initialDeepLink);
        StateContainer.of(context).initialDeepLink = null;
      }
    });
    _sendCompleteSub = EventTaxiImpl.singleton()
        .registerTo<SendCompleteEvent>()
        .listen((event) {
      // Route to send complete if received process response for send block
      if (event.previous != null) {
        // Route to send complete
        sl
            .get<DBHelper>()
            .getContactWithAddress(event.previous.link)
            .then((contact) {
          // String contactName = contact == null ? null : contact.name;
          Navigator.of(context).popUntil(RouteUtils.withNameLike('/home'));
          Sheets.showAppHeightNineSheet(
              context: context,
              closeOnTap: true,
              removeUntilHome: true,
              widget: SendCompleteSheet(
                  amountRaw: event.previous.sendAmount,
                  destination: event.previous.link,
                  // contactName: contactName,
                  // localAmount: event.previous.localCurrencyValue,
                  // paymentRequest: event.previous.paymentRequest
                  ));
        });
      }
    });
    // _contactModifiedSub = EventTaxiImpl.singleton()
    //     .registerTo<ContactModifiedEvent>()
    //     .listen((event) {
    //   _updateContacts();
    // });
    // Hackish event to block auto-lock functionality
    _disableLockSub = EventTaxiImpl.singleton()
        .registerTo<DisableLockTimeoutEvent>()
        .listen((event) {
      if (event.disable) {
        cancelLockEvent();
      }
      _lockDisabled = event.disable;
    });
    // User changed account
    _switchAccountSub = EventTaxiImpl.singleton()
        .registerTo<AccountChangedEvent>()
        .listen((event) {
      setState(() {
        StateContainer.of(context).wallet.loading = true;
        StateContainer.of(context).wallet.historyLoading = true;
        _startAnimation();
        StateContainer.of(context).updateWallet(account: event.account);
      });
      // paintQrCode(address: event.account.address);
      if (event.delayPop) {
        Future.delayed(Duration(milliseconds: 300), () {
          Navigator.of(context).popUntil(RouteUtils.withNameLike("/home"));
        });
      } else if (!event.noPop) {
        Navigator.of(context).popUntil(RouteUtils.withNameLike("/home"));
      }
    });
  }

  @override
  void dispose() {
    _destroyBus();
    WidgetsBinding.instance.removeObserver(this);
    _placeholderCardAnimationController.dispose();
    super.dispose();
  }

  void _destroyBus() {
    if (_historySub != null) {
      _historySub.cancel();
    }
    if (_contactModifiedSub != null) {
      _contactModifiedSub.cancel();
    }
    if (_sendCompleteSub != null) {
      _sendCompleteSub.cancel();
    }
    if (_disableLockSub != null) {
      _disableLockSub.cancel();
    }
    if (_switchAccountSub != null) {
      _switchAccountSub.cancel();
    }
  }

  // @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Handle websocket connection when app is in background
    // terminate it to be eco-friendly
    switch (state) {
      case AppLifecycleState.paused:
        setAppLockEvent();
        StateContainer.of(context).disconnect();
        super.didChangeAppLifecycleState(state);
        break;
      case AppLifecycleState.resumed:
        cancelLockEvent();
        StateContainer.of(context).reconnect();
        if (!StateContainer.of(context).wallet.loading &&
            StateContainer.of(context).initialDeepLink != null) {
          handleDeepLink(StateContainer.of(context).initialDeepLink);
          StateContainer.of(context).initialDeepLink = null;
        }
        super.didChangeAppLifecycleState(state);
        break;
      default:
        super.didChangeAppLifecycleState(state);
        break;
    }
  }

  // To lock and unlock the app
  StreamSubscription<dynamic> lockStreamListener;

  Future<void> setAppLockEvent() async {
    if (((await sl.get<SharedPrefsUtil>().getLock()) ||
            StateContainer.of(context).encryptedSecret != null) &&
        !_lockDisabled) {
      if (lockStreamListener != null) {
        lockStreamListener.cancel();
      }
      Future<dynamic> delayed = new Future.delayed(
          (await sl.get<SharedPrefsUtil>().getLockTimeout()).getDuration());
      delayed.then((_) {
        return true;
      });
      lockStreamListener = delayed.asStream().listen((_) {
        try {
          StateContainer.of(context).resetEncryptedSecret();
        } catch (e) {
          log.w(
              "Failed to reset encrypted secret when locking ${e.toString()}");
        } finally {
          Navigator.of(context)
              .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
        }
      });
    }
  }

  Future<void> cancelLockEvent() async {
    if (lockStreamListener != null) {
      lockStreamListener.cancel();
    }
  }

  // Refresh list // johnnytodo: study and use
  Future<void> _refresh() async {
    setState(() {
      _isRefreshing = true; // johnny: it will show loading screen / card
    });
    sl.get<HapticUtil>().success();
    StateContainer.of(context).requestUpdate();
    // Hide refresh indicator after 3 seconds if no server response
    Future.delayed(new Duration(seconds: 3), () {
      setState(() {
        _isRefreshing = false;
      });
    });
  }

  ///
  /// Because there's nothing convenient like DiffUtil, some manual logic
  /// to determine the differences between two lists and to add new items.
  ///
  /// Depends on == being overriden in the AccountHistoryResponseItem class
  ///
  /// Required to do it this way for the animation
  ///
  void diffAndUpdateHistoryList(List<AccountHistoryResponseItem> newList) {
    if (newList == null ||
        newList.length == 0 ||
        _historyListMap[StateContainer.of(context).wallet.address] == null)
      return;
    // Get items not in current list, and add them from top-down
    newList.reversed
        .where((item) =>
            !_historyListMap[StateContainer.of(context).wallet.address]
                .items
                .contains(item))
        .forEach((historyItem) {
      setState(() {
        _historyListMap[StateContainer.of(context).wallet.address]
            .insertAtTop(historyItem);
      });
    });
    // Re-subscribe if missing data
    if (StateContainer.of(context).wallet.loading) {
      StateContainer.of(context).requestSubscribe();
    }
  }

  Future<void> handleDeepLink(link) async {
    Address address = Address(link);
    if (address.isValid()) {
      String amount;
      String contactName;
      if (address.amount != null) {
        // Require minimum 1 rai to send
        if (BigInt.parse(address.amount) >= BigInt.from(10).pow(24)) {
          amount = address.amount;
        }
      }
      // See if a contact
      Contact contact =
          await sl.get<DBHelper>().getContactWithAddress(address.address);
      if (contact != null) {
        contactName = contact.name;
      }
      // Remove any other screens from stack
      Navigator.of(context).popUntil(RouteUtils.withNameLike('/home'));
      if (amount != null) {
        // Go to send confirm with amount
        Sheets.showAppHeightNineSheet(
            context: context,
            widget: SendConfirmSheet(
                amountRaw: amount,
                destination: address.address,
                contactName: contactName));
      } else {
        // Go to send with address
        Sheets.showAppHeightNineSheet(
            context: context,
            widget: SendSheet(
                localCurrency: StateContainer.of(context).curCurrency,
                // contact: contact, // johnny remove contact
                address: address.address));
      }
    } else if (MantaWallet.parseUrl(link) != null) {
      // Manta URI handling
      try {
        _showMantaAnimation();
        // Get manta payment request
        MantaWallet manta = MantaWallet(link);
        PaymentRequestMessage paymentRequest =
            await MantaUtil.getPaymentDetails(manta);
        if (mantaAnimationOpen) {
          Navigator.of(context).pop();
        }
        MantaUtil.processPaymentRequest(context, manta, paymentRequest);
      } catch (e) {
        if (mantaAnimationOpen) {
          Navigator.of(context).pop();
        }
        UIUtil.showSnackbar(AppLocalization.of(context).mantaError, context);
      }
    }
  }

  void _showMantaAnimation() {
    mantaAnimationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.MANTA,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => mantaAnimationOpen = false));
  }

  String _getHkdPrice(String usdPrice) {
    if (usdPrice == "") return "0";

    Decimal usdPriceDecimal = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(usdPrice);
    Decimal hkdPriceDecimal = usdPriceDecimal * Decimal.parse("7.75"); // johnnytodo config
    return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:2).format(hkdPriceDecimal.toDouble());
  }

  // variable for price display
  String priceDisplayAPI = "0";
  String priceChangeAPI = "0";
  bool priceChangePositive = true;

  Future<void> _refreshPrice() async {
    String coinName = widget.coinData.coinTicker;
    String pairName = 'XXBTZUSD';
    if (coinName == 'ETH') pairName = 'XETHZUSD';
    if (coinName == 'BCH') pairName = 'BCHUSD';
    if (coinName == 'LTC') pairName = 'XLTCZUSD';
    if (coinName == 'USDT') pairName = 'USDTZUSD';
    if (coinName == 'DOGE') pairName = 'XDGUSD';

    var url = 'https://api.kraken.com/0/public/Ticker?pair='+pairName;

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      // var itemCount = jsonResponse['result'];
      var _coin = jsonResponse['result'][pairName];
      var _sell = double.parse(_coin['a'][0]);
      var _buy = double.parse(_coin['b'][0]);
      var _open = double.parse(_coin['o']);
      
      var _price = double.parse(_coin['c'][0]); // last trade price
      var _change = (_price - _open)*100 / _open;
      bool isChangePositive = _price >= _open;
      // print('bitcoin price: $btc_price.');
      // print('bitcoin change: $btc_change.');

      setState(() {
        print('buy_coin_page : setState : refreshPrice()');
        priceDisplayAPI = _price.toStringAsFixed(2);
        priceChangeAPI = _change.toStringAsFixed(2);
        priceChangePositive = isChangePositive;
      });

    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    // sl.get<HapticUtil>().success();
    // StateContainer.of(context).requestUpdate();
    // // Hide refresh indicator after 3 seconds if no server response
    // Future.delayed(new Duration(seconds: 3), () {
    //   setState(() {
    //     _isRefreshing = false;
    //   });
    // });
  }

  void paintQrCode({String address}) {
    QrPainter painter = QrPainter(
      data:
          address == null ? StateContainer.of(context).wallet.address : address,
      version: 6,
      gapless: false,
      errorCorrectionLevel: QrErrorCorrectLevel.Q,
    );
    painter.toImageData(MediaQuery.of(context).size.width).then((byteData) {
      setState(() {
        receive = AppReceiveSheet(
          Container(
              width: MediaQuery.of(context).size.width / 2.675,
              child: Image.memory(byteData.buffer.asUint8List())),
        );
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    print('buy_coin_page building =======');
    // Create QR ahead of time because it improves performance this way
    // if (receive == null && StateContainer.of(context).wallet != null) {
    //   paintQrCode();
    // }

    return AppScaffold(
        resizeToAvoidBottomPadding: false,
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
          type: BottomNavigationBarType.fixed,
          currentIndex: 1,
          onTap: (int idx) {
                // setState(() {index = idx;}),

                print('BottomNavigationBar idx buy_coin:' + idx.toString());
                if (idx == 0) {
                  Navigator.of(context).pushNamed('/wallet'); 
                } else if (idx == 1) {
                  Navigator.of(context).pushNamed('/invest'); 
                } else if (idx == 2) {
                  Navigator.of(context).pushNamed('/history'); 
                } else if (idx == 3) {
                  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                    return UIUtil.showWebview(context,'https://bitginko.com/support');
                  }));
                }

              },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.account_balance_wallet, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.account_balance_wallet, color: StateContainer.of(context).curTheme.primary), title: Text('錢包', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
            BottomNavigationBarItem(icon: Icon(Icons.attach_money, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.attach_money, color: StateContainer.of(context).curTheme.primary), title: Text('交易', style: TextStyle(color: StateContainer.of(context).curTheme.primary60))),
            BottomNavigationBarItem(icon: Icon(Icons.history, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.history, color: StateContainer.of(context).curTheme.primary), title: Text('歷史', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
            BottomNavigationBarItem(icon: Icon(Icons.chat, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.chat, color: StateContainer.of(context).curTheme.primary), title: Text('客服', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
          ]),
        body: SafeArea(
          // minimum: EdgeInsets.only(
          //     // top: MediaQuery.of(context).size.height * 0.045,
          //     // bottom: MediaQuery.of(context).size.height * 0.035
          // ),
          child: Column(
            
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              // #1: the text of price and name
              Expanded(
                flex: 4,
                child: Container(
                  // padding: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                  // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      // back button 
                      Align(
                        alignment: Alignment.centerLeft,
                          child: IconButton(
                          // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          highlightColor: StateContainer.of(context).curTheme.text15,
                          splashColor: StateContainer.of(context).curTheme.text30,
                          // padding: EdgeInsetsDirectional.fromSTEB(12, 4, 12, 4),
                          icon: Icon(AppIcons.back,
                              size: 16,
                              color: StateContainer.of(context).curTheme.text
                          ),
                        ),
                      ),
                      
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                        child: Text(
                          widget.coinData.coinName + " 價格", // johnnyTranslate
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontFamily: "NunitoSans",
                            fontSize: 15.0,
                            fontWeight: FontWeight.w500,
                            color: StateContainer.of(context).curTheme.text60,
                          ),
                        ),
                      ),
                      
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                        child: Row(
                          children: <Widget>[

                            // original code
                            Text(
                              "\$ " + _getHkdPrice(priceDisplayAPI),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: "NunitoSans",
                                fontSize: 28,
                                fontWeight: FontWeight.w900,
                                color: StateContainer.of(context).curTheme.text,
                              ),
                            ),

                            Container(
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Text(
                                "("+ priceChangeAPI +"%)",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: "NunitoSans",
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: priceChangePositive ? Colors.greenAccent : Colors.redAccent,//StateContainer.of(context).curTheme.text,
                                ),
                              ),
                            )
                        ],)
                      ),
                      
                      
                    ],
                  ),
                ),
                
              ),
    
              // #2: the crypto-price line chart
              Expanded(
                flex: 12,
                child: Column(children: <Widget>[
                  // the line chart
                  Container(
                    margin: EdgeInsets.only(top: 15,),
                    // padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    // height: 350,
                    // height:
                    //     screenAwareSize(_media.longestSide <= 775 ? 180 : 130, context),
                    decoration: BoxDecoration(
                      color: StateContainer.of(context).curTheme.background,
                      // border: Border.all(color: Colors.yellow), // for debug
                      // borderRadius: BorderRadius.circular(10.0),
                      // boxShadow: [StateContainer.of(context).curTheme.boxShadow],
                    ),
                    child: flLineChartBig(coinName: widget.coinData.coinTicker) // fl_chart  // SelectionLineHighlight.withSampleData(),
                  ),
                ],)
              ),
              
              // #3: balance display
              Expanded(
                flex:3,
                child: Container(
                  // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
                  padding: EdgeInsets.only(left: 18, right: 18, bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        width: (MediaQuery.of(context).size.width - 64) / 2,
                        // decoration: BoxDecoration(border: Border.all(color: Colors.yellow)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "現金結餘", // johnnyTranslate
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: "NunitoSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w200,
                                color: StateContainer.of(context).curTheme.text60
                              ),
                            ),
                            Text(
                              StateContainer.of(context).wallet.getBalance_HKD(), 
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: StateContainer.of(context).curTheme.text,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: (MediaQuery.of(context).size.width - 64) / 2,
                        // decoration: BoxDecoration(border: Border.all(color: Colors.yellow)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              widget.coinData.coinName + " 結餘",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: "NunitoSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w200,
                                color: StateContainer.of(context).curTheme.text60
                              ),
                            ),
                            Text(
                              widget.coinData.avaBalance + ' ' + widget.coinData.coinTicker, 
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: StateContainer.of(context).curTheme.text,
                              ),
                            ),
                          ],
                        ),
                      )
                      
                      
                    ],
                  ),
                ), 
              ),

              // #4: trade / buy sell button
              Expanded(
                flex:3,
                child: Container(
                  // decoration: BoxDecoration(border: Border.all(color: Colors.red)), // for debug
                  padding: EdgeInsets.only(left: 18, right: 18, bottom: 20),
                  // margin: EdgeInsetsDirectional.fromSTEB(30.0, 20.0, 26.0, 0.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      // button 1 : BUY
                      AppTradeButton(tradeMode: "BUY", 
                        coinData: widget.coinData, hkdPrice: _getHkdPrice(priceDisplayAPI)),

                      // button 2 : SELL
                      AppTradeButton(tradeMode: "SELL", 
                          coinData: widget.coinData, hkdPrice: _getHkdPrice(priceDisplayAPI)),

                    ],
                  ),
                ), 
              )
              
            ],
          ),
        ),
      );
  }

}



/// This is used so that the elevation of the container is kept and the
/// drop shadow is not clipped.
///
// class _SizeTransitionNoClip extends AnimatedWidget {
//   final Widget child;

//   const _SizeTransitionNoClip(
//       {@required Animation<double> sizeFactor, this.child})
//       : super(listenable: sizeFactor);

//   @override
//   Widget build(BuildContext context) {
//     return new Align(
//       alignment: const AlignmentDirectional(-1.0, -1.0),
//       widthFactor: null,
//       heightFactor: (this.listenable as Animation<double>).value,
//       child: child,
//     );
//   }
// }
