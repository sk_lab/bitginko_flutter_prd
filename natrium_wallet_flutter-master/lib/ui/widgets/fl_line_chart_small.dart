import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';

import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class flLineChartSmall extends StatefulWidget {
  final String coinName;

  flLineChartSmall({this.coinName});


  @override
  _flLineChartSmallState createState() => _flLineChartSmallState();
}

class _flLineChartSmallState extends State<flLineChartSmall> {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  bool showAvg = false;

  @override
  void initState() {
    super.initState();
    _refreshChartData('hour');
  }

  // variable for chart data
  double _minX = 0;
  double _maxX = 20; // 1581582600000;
  double _minY = 0;
  double _maxY = 0; // 10500;
  List<FlSpot> _spots = [FlSpot(0, 1)];

  Future<void> _refreshChartData(String period) async {
    var coinName = widget.coinName;
    var url = 'https://bitginko.com/api/v4/uri/getChartData.api.php?fsym='+coinName+'&tt='+period;

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var items = jsonResponse['Result'];
      // print('chart api response: $items');

      // List<FlSpot> _tmpSpots;
      int listLength = 0; 
      if (period == 'hour') listLength = 61;
      if (period == 'day') listLength = 145;
      if (period == 'week') listLength = 168;
      if (period == 'month') listLength = 120;
      if (period == 'year') listLength = 365;

      List<FlSpot> _tmpSpots = new List(listLength);
      double _tmpMinX = 0;
      double _tmpMaxX = 0;
      double _tmpMinY = 0;
      double _tmpMaxY = 0;


      for (int i=0; i<listLength; i++){
        var obj = items[i];

        // final f = new DateFormat('yyyy-MM-dd hh:mm');
        // String dateDisplay = f.format(new DateTime.fromMillisecondsSinceEpoch(int.parse(time.toString()))) ;
        // print('testing time: '+ dateDisplay);


        double price = double.parse(obj['price']);
        // String priceDisplay = price.toStringAsFixed(0);
        double timestamp = double.parse(obj['time'].toString());

        if (i == 0) _tmpMinX = timestamp;
        if (i == listLength-1) _tmpMaxX = timestamp;
        if (_tmpMinY == 0 || price < _tmpMinY) _tmpMinY = price;
        if (price > _tmpMaxY) _tmpMaxY = price;
        

        // var tmp = new FlSpot(0, 1);
        // _tmpSpots.add(FlSpot(i.toDouble(), double.parse(priceDisplay)));
        _tmpSpots[i] = (FlSpot(timestamp, price));

        // print('chart data $i time: $time.');
        // print('chart data $i price: $priceDisplay.');
      }

      int lengthAAA = _tmpSpots.length;
      

      // print('tmp spot data length: $lengthAAA');
      // print('tmp spot data: $_tmpSpots');
      

      setState(() {
        _spots = _tmpSpots;

        _minX = _tmpMinX;
        _maxX = _tmpMaxX;
        // print('timestamp: minX:' + _minX.toString() + ' maxX' + _maxX.toString());
        _minY = _tmpMinY;
        _maxY = _tmpMaxY;

        // priceDisplayAPI = btc_price.toStringAsFixed(2);
        // priceChangeAPI = btc_change.toStringAsFixed(2);
        // priceChangePositive = isChangePositive;
      });

      // print('done setState>>>>>');

    } else {
      print('(flChartSmall) Request failed with status: ${response.statusCode}.');
    }
    
  }

  @override
  Widget build(BuildContext context) {
    // print('chart_small building =======');

    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 2.2,
          child: Container(
            
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(0),
                ),
                color: StateContainer.of(context).curTheme.backgroundDark, 
            ),//const Color(0xff232d37)),
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 0, left: 0, top: 0, bottom: 0),
              child: LineChart(mainData()),
                  //LineChart(showAvg ? avgData() : mainData(),),
            ),
          ),
        )
      ],
    );
  }

  LineChartData mainData() {
    return LineChartData(
      lineTouchData: LineTouchData(
        getTouchedSpotIndicator: (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((spotIndex) {
            final FlSpot spot = barData.spots[spotIndex];
            // if (spot.x == 0 || spot.x == 6) {
            //   return null;
            // }
            return TouchedSpotIndicatorData(
              FlLine(color: Colors.transparent, strokeWidth: 1),
              // FlDotData(dotSize: 8, getDotColor: (spot, percent, barData) => Colors.transparent),
              FlDotData(
                  getDotPainter: (spot, percent, barData, index) {
                    return FlDotCirclePainter(
                      radius: 8,
                      color: gradientColors[0],
                      strokeWidth: 2,
                      strokeColor: Colors.transparent,
                    );
                  },
                ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
            tooltipBgColor: Colors.transparent,
            getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
              return touchedBarSpots.map((barSpot) {
                final flSpot = barSpot;
                // if (flSpot.x == 0 || flSpot.x == 6) {
                //   return null;
                // }

                return LineTooltipItem(
                  'April 20 \n \$20,000',
                  const TextStyle(color: Colors.transparent),
                );
              }).toList();
            })
        ),

      gridData: FlGridData(
        show: false,
        drawVerticalLine: false,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: false,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (i) { return  
              TextStyle(
                color: Colors.red,//const Color(0xff68737d), // the x-axis label
                fontWeight: FontWeight.bold,
                fontSize: 16);
              },
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (i) { return  
              TextStyle(
                color: Colors.red,//const Color(0xff68737d), // the x-axis label
                fontWeight: FontWeight.bold,
                fontSize: 15);
              },
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '10k';
              case 3:
                return '30k';
              case 5:
                return '50k';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: Colors.transparent, width: 1)),
      minX: _minX,
      maxX: _maxX,
      minY: _minY,
      maxY: _maxY,
      lineBarsData: [
        LineChartBarData(
          spots: _spots,
          // spots: const [
          //   FlSpot(0, 1),
          //   FlSpot(20, 21),
          // ],
          isCurved: true,
          colors: gradientColors,
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: false,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  // LineChartData avgData() {
  //   return LineChartData(
  //     lineTouchData: const LineTouchData(enabled: false),
  //     gridData: FlGridData(
  //       show: true,
  //       drawHorizontalLine: true,
  //       getDrawingVerticalLine: (value) {
  //         return const FlLine(
  //           color: Color(0xff37434d),
  //           strokeWidth: 1,
  //         );
  //       },
  //       getDrawingHorizontalLine: (value) {
  //         return const FlLine(
  //           color: Color(0xff37434d),
  //           strokeWidth: 1,
  //         );
  //       },
  //     ),
  //     titlesData: FlTitlesData(
  //       show: true,
  //       bottomTitles: SideTitles(
  //         showTitles: true,
  //         reservedSize: 22,
  //         textStyle: TextStyle(
  //             color: const Color(0xff68737d),
  //             fontWeight: FontWeight.bold,
  //             fontSize: 16),
  //         getTitles: (value) {
  //           switch (value.toInt()) {
  //             case 2:
  //               return 'MAR';
  //             case 5:
  //               return 'JUN';
  //             case 8:
  //               return 'SEP';
  //           }
  //           return '';
  //         },
  //         margin: 8,
  //       ),
  //       leftTitles: SideTitles(
  //         showTitles: true,
  //         textStyle: TextStyle(
  //           color: const Color(0xff67727d),
  //           fontWeight: FontWeight.bold,
  //           fontSize: 15,
  //         ),
  //         getTitles: (value) {
  //           switch (value.toInt()) {
  //             case 1:
  //               return '10k';
  //             case 3:
  //               return '30k';
  //             case 5:
  //               return '50k';
  //           }
  //           return '';
  //         },
  //         reservedSize: 28,
  //         margin: 12,
  //       ),
  //     ),
  //     borderData: FlBorderData(
  //         show: true,
  //         border: Border.all(color: const Color(0xff37434d), width: 1)),
  //     minX: 0,
  //     maxX: 11,
  //     minY: 0,
  //     maxY: 6,
  //     lineBarsData: [
  //       LineChartBarData(
  //         spots: const [
  //           FlSpot(0, 3.44),
  //           FlSpot(2.6, 3.44),
  //           FlSpot(4.9, 3.44),
  //           FlSpot(6.8, 3.44),
  //           FlSpot(8, 3.44),
  //           FlSpot(9.5, 3.44),
  //           FlSpot(11, 3.44),
  //         ],
  //         isCurved: true,
  //         colors: [
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2),
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2),
  //         ],
  //         barWidth: 5,
  //         isStrokeCapRound: true,
  //         dotData: const FlDotData(
  //           show: false,
  //         ),
  //         belowBarData: BarAreaData(show: true, colors: [
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2)
  //               .withOpacity(0.1),
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2)
  //               .withOpacity(0.1),
  //         ]),
  //       ),
  //     ],
  //   );
  // }
}





