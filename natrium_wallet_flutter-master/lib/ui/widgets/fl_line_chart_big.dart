
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/model/chart_data.dart';
import 'package:natrium_wallet_flutter/model/price_model.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';

import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';

import '../../app_icons.dart';
import '../../styles.dart';
// import 'package:provider/provider.dart'; // johnny: undo and remove this

/// johnny: check the doc here:
/// https://github.com/imaNNeoFighT/fl_chart/blob/master/repo_files/documentations/line_chart.md

class flLineChartBig extends StatefulWidget {
  String coinName;

  flLineChartBig({this.coinName});

  @override
  _flLineChartBigState createState() => _flLineChartBigState();
}

class _flLineChartBigState extends State<flLineChartBig> {

  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  // bool showAvg = false;

  @override
  void initState() {
    super.initState();
    _refreshChartData('day');
  }

  // variable for chart data
  double _minX = 0; // use this setting will load the chart quicker
  double _maxX = 20; // 1581582600000;
  double _minY = 0;
  double _maxY = 0; // 10500;
  int listLength = 0; 
  List<FlSpot> _spots = [FlSpot(0, 1)];
  bool animationOpen;
  
  Map<String, ChartData> allChartData = <String, ChartData>{}; // johnny. String: period


  void _showSendingAnimation(BuildContext context) {
    animationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.GENERIC,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        Colors.transparent,
        // StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => animationOpen = false));
  }
  
  Future<bool> _refreshChartData(String period) async {
    print('lineChartBig_refreshChartData: ' + period);

    // johnny: add animation before refreshing finished

    ChartData _currData = allChartData[period];

    // checking if have saved data here...
    if (_currData != null) { // johnny: if previous chart data is saved, no need to call api again.
        setState(() {
          _spots = _currData.spots;
          _minX = _currData.minX;
          _maxX = _currData.maxX;
          _minY = _currData.minY;
          _maxY = _currData.maxY;
        });
    } else {

        // johnny: Call API to retrive new data info
        String coinName = widget.coinName;
        var url = 'https://bitginko.com/api/v4/uri/getChartData.api.php?fsym='+coinName+'&tt='+period;

        // Await the http get response, then decode the json-formatted response.
        var response = await http.get(url);
        if (response.statusCode == 200) {
          var jsonResponse = convert.jsonDecode(response.body);
          var items = jsonResponse['Result'];
          // print('chart api response: $items');

          // List<FlSpot> _tmpSpots;
          if (period == 'hour') 
            listLength = 61;
          else if (period == 'day') 
            listLength = 145;
          else if (period == 'week') 
            listLength = 168;
          else if (period == 'month') 
            listLength = 120;
          else if (period == 'year') 
            listLength = 365;
          else 
            listLength = 0;

          List<FlSpot> _tmpSpots = new List(listLength);
          double _tmpMinX = 0;
          double _tmpMaxX = 0;
          double _tmpMinY = 0;
          double _tmpMaxY = 0;

          for (int i=0; i<listLength; i++){
            var obj = items[i];

            // final f = new DateFormat('yyyy-MM-dd hh:mm');
            // String dateDisplay = f.format(new DateTime.fromMillisecondsSinceEpoch(int.parse(time.toString()))) ;
            // print('testing time: '+ dateDisplay);

            double price = double.parse(obj['price']);
            // String priceDisplay = price.toStringAsFixed(0);
            double timestamp = double.parse(obj['time'].toString());

            if (i == 0) _tmpMinX = timestamp;
            if (i == listLength-1) _tmpMaxX = timestamp;
            if (_tmpMinY == 0 || price < _tmpMinY) _tmpMinY = price;
            if (price > _tmpMaxY) _tmpMaxY = price;

            // var tmp = new FlSpot(0, 1);
            // _tmpSpots.add(FlSpot(i.toDouble(), double.parse(priceDisplay)));
            _tmpSpots[i] = (FlSpot(timestamp, price));

            // print('chart data $i time: $time.');
            // print('chart data $i price: $priceDisplay.');
          }

          int lengthAAA = _tmpSpots.length;
          
          // print('tmp spot data length: $lengthAAA');
          // print('tmp spot data: $_tmpSpots');
          
          setState(() {
            _spots = _tmpSpots;
            // print('spots data:');
            // print(_spots);

            _minX = _tmpMinX;
            _maxX = _tmpMaxX;
            _minY = _tmpMinY;
            _maxY = _tmpMaxY;
            // print('timestamp: minX:' + _minX.toString() + ' maxX' + _maxX.toString());

            // priceDisplayAPI = btc_price.toStringAsFixed(2);
            // priceChangeAPI = btc_change.toStringAsFixed(2);
            // priceChangePositive = isChangePositive;

            // johnny. save data in memory
            allChartData[period] = new ChartData(spots: _tmpSpots,
                                                  minX: _tmpMinX,
                                                  maxX: _tmpMaxX,
                                                  minY: _tmpMinY,
                                                  maxY: _tmpMaxY);
          });

          // print('done setState>>>>>');

        } else {
          print('(flChartBig) Request failed with status: ${response.statusCode}.');
        }

    }

    return true;
  }

  void _refreshWithAnimation(String period) async {
    _showSendingAnimation(context);
    bool finish = await _refreshChartData(period);
    if (finish) {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    print('chart_big building =======');

    return Column(
      children: <Widget>[

        // the 5 duration choice button (1H, 1D, 1W, 1M, 1Y)
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                child: Text(
                  "1H",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w100,
                    color: StateContainer.of(context).curTheme.text,
                  ),
                ),
                onTap: () => {
                  // setState(() {
                    // print('pressed hour.');
                    _refreshWithAnimation('hour')
                    // bool finish = await _refreshChartData('hour');
                    // Navigator.of(context).pop()
                  // })
                },
              ),
              InkWell(
                child: Text(
                  "1D",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w100,
                    color: StateContainer.of(context).curTheme.text,
                  ),
                ),
                onTap: () => {
                  // setState(() {
                    // _showSendingAnimation(context),
                    // _refreshChartData('day'),
                    _refreshWithAnimation('day')
                  // })
                },
              ),
              InkWell(
                child: Text(
                  "1W",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w100,
                    color: StateContainer.of(context).curTheme.text,
                  ),
                ),
                onTap: () => {
                  // setState(() {
                    _refreshWithAnimation('week')
                  // })
                },
              ),
              InkWell(
                child: Text(
                  "1M",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w100,
                    color: StateContainer.of(context).curTheme.text,
                  ),
                ),
                onTap: () => {
                  // setState(() {
                    _refreshWithAnimation('month')
                    // _refreshChartData('month')
                  // })
                },
              ),
              InkWell(
                child: Text(
                  "1Y",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w100,
                    color: StateContainer.of(context).curTheme.text,
                  ),
                ),
                onTap: () => {
                  // setState(() {
                    _refreshWithAnimation('year')
                    // _refreshChartData('year')
                  // })
                },
              ),
            ],
          )
        ),
        
        // the chart
        AspectRatio(
          aspectRatio: 1.35,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(0),
                ),
                color: StateContainer.of(context).curTheme.background, 
            ),//const Color(0xff232d37)),
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 0, left: 0, top: 20, bottom: 0),
              child: LineChart(mainData(context)),
                  //LineChart(showAvg ? avgData() : mainData(),
              ),
            ),
        )
        
        
        // SizedBox(
        //   width: 60,
        //   height: 34,
        //   child: FlatButton(
        //     onPressed: () {
        //       setState(() {
        //         showAvg = !showAvg;
        //       });
        //     },
        //     child: Text(
        //       'Avg',
        //       style: TextStyle(
        //           fontSize: 12,
        //           color:
        //               showAvg ? Colors.white.withOpacity(0.5) : Colors.white),
        //     ),
        //   ),
        // ),
      ],
    );
  }

  double tmp = 0;

  String datetimeTranslate(double timestamp) {
    final f = new DateFormat('dd/MM \n HH:mm');
    // final f = new DateFormat('hh:mm');
    String dateDisplay = f.format(new DateTime.fromMillisecondsSinceEpoch(timestamp.toInt())) ;
    // print('AAAAAA: '+dateDisplay);

    return dateDisplay;
  }

  String timeTranslate(double timestamp) {
    final f = new DateFormat('HH:mm');
    // final f = new DateFormat('hh:mm');
    String dateDisplay = f.format(new DateTime.fromMillisecondsSinceEpoch(timestamp.toInt())) ;
    // print('AAAAAA: '+dateDisplay);

    return dateDisplay;
  }

  String dayTranslate(double timestamp) {
    final f = new DateFormat('dd/MM');
    // final f = new DateFormat('hh:mm');
    String dateDisplay = f.format(new DateTime.fromMillisecondsSinceEpoch(timestamp.toInt())) ;
    // print('AAAAAA: '+dateDisplay);

    return dateDisplay;
  }

  LineChartData mainData(BuildContext context) {

    return LineChartData(
      lineTouchData: LineTouchData(
        getTouchedSpotIndicator: (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((spotIndex) {
            final FlSpot spot = barData.spots[spotIndex];
            // if (spot.x == 0 || spot.x == 6) {
            //   return null;
            // }
            // if (spot.y != tmp) {
            //   // price.priceDisplay = spot.y;
            //   tmp = spot.y;
            // }

            return TouchedSpotIndicatorData(
              FlLine(color: Colors.blueGrey, strokeWidth: 1),
              // FlDotData(dotSize: 8, getDotColor: (spot, percent, barData) => Colors.lightBlue),
              FlDotData(
                  getDotPainter: (spot, percent, barData, index) {
                    return FlDotCirclePainter(
                      radius: 8,
                      color: gradientColors[0],
                      strokeWidth: 2,
                      strokeColor: Colors.lightBlue,
                    );
                  },
                ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blue,
          getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
            return touchedBarSpots.map((barSpot) {
              final flSpot = barSpot;
              // if (flSpot.x == 0 || flSpot.x == 6) {
              //   return null;
              // }
              // print('tooltip data:');
              // print('x:' + flSpot.x.toString() + '. y: ' + flSpot.y.toString());
              return LineTooltipItem(
                ''+ timeTranslate(flSpot.x) +'\n \$' + flSpot.y.toStringAsFixed(2),
                const TextStyle(color: Colors.white),
              );
            }).toList();
          })
      ),

      gridData: FlGridData(
        show: false,
        drawVerticalLine: false,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),

      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles( // the x-axis
          showTitles: false,
          getTextStyles: (i) { return  
              TextStyle(
                color: StateContainer.of(context).curTheme.text,//const Color(0xff68737d), // the x-axis label
                fontWeight: FontWeight.bold,
                fontSize: 12);
              },
          getTitles: (value) {
            if (value.toInt() == _minX.toInt()){
              return datetimeTranslate(_minX);
            }

            if (value.toInt() == (_maxX/2 + _minX/2).toInt()){
              return datetimeTranslate(value);
            }

            if (value.toInt() == _maxX.toInt()){
              return datetimeTranslate(_maxX);
            }

            return '';
          },
          // interval: (_maxX/2 + _minX/2),
          reservedSize: 22,
          margin: 8,
        ),
        leftTitles: SideTitles( // the y-axis left
          showTitles: false,
          getTextStyles: (i) { return  
              TextStyle(
                color: StateContainer.of(context).curTheme.text,//const Color(0xff68737d), // the x-axis label
                fontWeight: FontWeight.bold,
                fontSize: 12);
              },
          getTitles: (value) {
            // print(value.toInt());

            if (value.toInt() == _minY.toInt()){
              return '\$' + value.toStringAsFixed(0);
            }

            if (value.toInt() == (_maxY/2 + _minY/2).toInt()){
              return '\$' + value.toStringAsFixed(0);
            }

            if (value.toInt() == (_maxY*0.999).toInt()){
              return '\$' + value.toStringAsFixed(0);
            }
            return '';
          },
          // interval: 1,
          reservedSize: 30,
          margin: 30,
        ),
        rightTitles: SideTitles( // the y-axis right
          showTitles: false,
          getTextStyles: (i) { return  
              TextStyle(
                color: StateContainer.of(context).curTheme.text,//const Color(0xff68737d), // the x-axis label
                fontWeight: FontWeight.bold,
                fontSize: 12);
              },
          getTitles: (value) {

            if (value.toInt() == _minY.toInt()){
              return '\$' + value.toStringAsFixed(0);
            }

            if (value.toInt() == (_maxY/2 + _minY/2).toInt()){
              return '\$' + value.toStringAsFixed(0);
            }

            if (value.toInt() == (_maxY*0.999).toInt()){
              return '\$' + value.toStringAsFixed(0);
            }
            return '';
          },
          // interval: 1,
          reservedSize: 30,
          margin: 30,
        ),
      ),
      
      borderData: FlBorderData(
          show: false,
          border: Border ( //Border.all(color: Colors.red, width: 1)),
            left: BorderSide(color: Color(0xff67727d)),
            top: BorderSide(color: Colors.transparent),
            bottom: BorderSide(color: Color(0xff67727d)),
            right: BorderSide(color: Colors.transparent),
          )
      ),

      minX: _minX,
      maxX: _maxX,
      minY: _minY,
      maxY: _maxY,

      lineBarsData: [
        LineChartBarData(
          spots: _spots,
          // spots: [
          //   FlSpot(0, 1),
          //   FlSpot(2.6, 2),
          //   FlSpot(4.9, 3),
          //   FlSpot(6.8, 4),
          //   FlSpot(8, 5),
          //   FlSpot(9.5, 6),
          //   FlSpot(11, 7),
          // ],
          isCurved: true,
          colors: gradientColors,
          barWidth: 3,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: false,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  // LineChartData avgData() {
  //   return LineChartData(
  //     lineTouchData: const LineTouchData(enabled: false),
  //     gridData: FlGridData(
  //       show: true,
  //       drawHorizontalLine: true,
  //       getDrawingVerticalLine: (value) {
  //         return const FlLine(
  //           color: Color(0xff37434d),
  //           strokeWidth: 1,
  //         );
  //       },
  //       getDrawingHorizontalLine: (value) {
  //         return const FlLine(
  //           color: Color(0xff37434d),
  //           strokeWidth: 1,
  //         );
  //       },
  //     ),
  //     titlesData: FlTitlesData(
  //       show: true,
  //       bottomTitles: SideTitles(
  //         showTitles: true,
  //         reservedSize: 22,
  //         textStyle: TextStyle(
  //             color: const Color(0xff68737d),
  //             fontWeight: FontWeight.bold,
  //             fontSize: 16),
  //         getTitles: (value) {
  //           switch (value.toInt()) {
  //             case 2:
  //               return 'MAR';
  //             case 5:
  //               return 'JUN';
  //             case 8:
  //               return 'SEP';
  //           }
  //           return '';
  //         },
  //         margin: 8,
  //       ),
  //       leftTitles: SideTitles(
  //         showTitles: true,
  //         textStyle: TextStyle(
  //           color: const Color(0xff67727d),
  //           fontWeight: FontWeight.bold,
  //           fontSize: 15,
  //         ),
  //         getTitles: (value) {
  //           switch (value.toInt()) {
  //             case 1:
  //               return '10k';
  //             case 3:
  //               return '30k';
  //             case 5:
  //               return '50k';
  //           }
  //           return '';
  //         },
  //         reservedSize: 28,
  //         margin: 12,
  //       ),
  //     ),
  //     borderData: FlBorderData(
  //         show: true,
  //         border: Border.all(color: const Color(0xff37434d), width: 1)),
  //     minX: 0,
  //     maxX: 11,
  //     minY: 0,
  //     maxY: 6,
  //     lineBarsData: [
  //       LineChartBarData(
  //         spots: const [
  //           FlSpot(0, 3.44),
  //           FlSpot(2.6, 3.44),
  //           FlSpot(4.9, 3.44),
  //           FlSpot(6.8, 3.44),
  //           FlSpot(8, 3.44),
  //           FlSpot(9.5, 3.44),
  //           FlSpot(11, 3.44),
  //         ],
  //         isCurved: true,
  //         colors: [
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2),
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2),
  //         ],
  //         barWidth: 5,
  //         isStrokeCapRound: true,
  //         dotData: const FlDotData(
  //           show: false,
  //         ),
  //         belowBarData: BarAreaData(show: true, colors: [
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2)
  //               .withOpacity(0.1),
  //           ColorTween(begin: gradientColors[0], end: gradientColors[1])
  //               .lerp(0.2)
  //               .withOpacity(0.1),
  //         ]),
  //       ),
  //     ],
  //   );
  // }
}





