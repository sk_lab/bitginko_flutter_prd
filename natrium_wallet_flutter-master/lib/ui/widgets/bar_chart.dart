import 'dart:collection';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';

class SimpleBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleBarChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory SimpleBarChart.withSampleData(List<String> months, HashMap map) {
    return new SimpleBarChart(
      _createSampleData(months, map),
      // Disable animations for image tests.
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      vertical: false,

      // the label on the bar
      barRendererDecorator: charts.BarLabelDecorator<String>(
        labelAnchor: charts.BarLabelAnchor.end,
        insideLabelStyleSpec: new charts.TextStyleSpec(
          color: charts.MaterialPalette.gray.shade700, 
        ),
        outsideLabelStyleSpec: new charts.TextStyleSpec(
          color: charts.MaterialPalette.gray.shade700,
        )
      ),      

      // Set the initial viewport by providing a new AxisSpec with the
      // desired viewport: a starting domain and the data size.
      domainAxis: new charts.OrdinalAxisSpec(
        showAxisLine: true,
        renderSpec: charts.GridlineRendererSpec(
              axisLineStyle: charts.LineStyleSpec(
                color: charts.MaterialPalette.gray.shade600, // this also doesn't change the Y axis labels
              ),
              labelStyle: new charts.TextStyleSpec(
                fontSize: 10,
                color: charts.MaterialPalette.gray.shade700,
              ),
              lineStyle: charts.LineStyleSpec(
                thickness: 1,
                color: charts.MaterialPalette.transparent,
              )
            ),
        viewport: new charts.OrdinalViewport('2018', 4)),

      primaryMeasureAxis: new charts.NumericAxisSpec(
        renderSpec: new charts.GridlineRendererSpec(

            // Tick and Label styling here.
            labelStyle: new charts.TextStyleSpec(
                fontSize: 12, // size in Pts.
                color: charts.MaterialPalette.gray.shade700),

            // Change the line colors to match text color.
            lineStyle: new charts.LineStyleSpec(
                thickness: 1,
                color: charts.MaterialPalette.gray.shade500,
              ))),

      // Optionally add a pan or pan and zoom behavior.
      // If pan/zoom is not added, the viewport specified remains the viewport.
      behaviors: [new charts.PanAndZoomBehavior()],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData(List<String> months, HashMap map) {
    // final data = [
    //   new OrdinalSales(map[0], ),
    //   new OrdinalSales('2015', 2500),
    //   new OrdinalSales('2016', 10000),
    //   new OrdinalSales('2017', 7500),
    //   // new OrdinalSales('2018', 33),
    // ];

    final List<OrdinalSales> data = new List<OrdinalSales>();
    for (String month in months) {
      int count = map[month];
        data.add(new OrdinalSales(month, count));
    }

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
        labelAccessorFn: (OrdinalSales sales, _) => '\$${sales.sales.toString()}'
      )
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
