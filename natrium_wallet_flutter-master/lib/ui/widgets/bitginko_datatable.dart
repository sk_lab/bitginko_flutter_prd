import 'package:flutter/material.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';

class BitginkoDataTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    Container(
      // margin: EdgeInsets.only(top: 15, right: 15),
      // padding: EdgeInsets.all(15),
      // height: screenAwareSize(90, context),
      // width: _media.width / 2 - 25,
      // height: 250,
      margin: EdgeInsets.only(
          // left: 14.0,
          // right: 14.0,
          top: MediaQuery.of(context).size.height * 0.005),
      decoration: BoxDecoration(
          color: StateContainer.of(context).curTheme.backgroundDark,
          // borderRadius: BorderRadius.circular(10), // 15
          boxShadow: [StateContainer.of(context).curTheme.boxShadow],
          // border: Border(
          //   top: BorderSide(width: 1.0, color: Colors.blueGrey),
          //   left: BorderSide(width: 1.0, color: Colors.blueGrey),
          //   right: BorderSide(width: 1.0, color: Colors.blueGrey),
          //   bottom: BorderSide(width: 1.0, color: Colors.blueGrey),
          // ),
          // boxShadow: [
          //   BoxShadow(
          //       color: Colors.grey.shade100, //color.withOpacity(0.4),
          //       blurRadius: 6, // 16,
          //       spreadRadius: 10, // 0.2,
          //       // offset: Offset(0, 8)
          //       ),
          // ]
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
          child: 
            DataTable(
              columns: [
                DataColumn(label: Text('資產名稱', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                DataColumn(label: Text('現時價格', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                DataColumn(label: Text('數量', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                DataColumn(label: Text('現時市值', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                DataColumn(label: Text('變動', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                DataColumn(label: Text('Action', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Text('AAA', style: TextStyle(color: StateContainer.of(context).curTheme.text))),  
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                ]),
                DataRow(cells: [
                  DataCell(Text('AAA', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                ]),
                DataRow(cells: [
                  DataCell(Text('AAA', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                ]),
                DataRow(cells: [
                  DataCell(Text('AAA', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                ]),
                DataRow(cells: [
                  DataCell(Text('AAA', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                  DataCell(Text('BBB', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
                ]),
                
              ]
            )
        )
      )
    );
    


    
  }
}