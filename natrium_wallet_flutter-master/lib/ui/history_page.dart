import 'dart:async';
import 'dart:collection';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:decimal/decimal.dart';
import 'package:dio/dio.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:event_taxi/event_taxi.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:manta_dart/manta_wallet.dart';
import 'package:manta_dart/messages.dart';
import 'package:natrium_wallet_flutter/model/history_data.dart';
import 'package:natrium_wallet_flutter/ui/popup_button.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/dimens.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';
import 'package:natrium_wallet_flutter/model/address.dart';
import 'package:natrium_wallet_flutter/model/list_model.dart';
import 'package:natrium_wallet_flutter/model/db/contact.dart';
import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/network/model/block_types.dart';
import 'package:natrium_wallet_flutter/network/model/response/account_history_response_item.dart';
import 'package:natrium_wallet_flutter/styles.dart';
import 'package:natrium_wallet_flutter/app_icons.dart';
import 'package:natrium_wallet_flutter/ui/contacts/add_contact.dart';
import 'package:natrium_wallet_flutter/ui/send/send_sheet.dart';
import 'package:natrium_wallet_flutter/ui/send/send_confirm_sheet.dart';
import 'package:natrium_wallet_flutter/ui/send/send_complete_sheet.dart';
import 'package:natrium_wallet_flutter/ui/receive/receive_sheet.dart';
import 'package:natrium_wallet_flutter/ui/settings/settings_drawer.dart';
import 'package:natrium_wallet_flutter/ui/widgets/buttons.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_drawer.dart';
import 'package:natrium_wallet_flutter/ui/widgets/app_scaffold.dart';
import 'package:natrium_wallet_flutter/ui/widgets/dialog.dart';
import 'package:natrium_wallet_flutter/ui/widgets/sheet_util.dart';
import 'package:natrium_wallet_flutter/ui/widgets/list_slidable.dart';
import 'package:natrium_wallet_flutter/ui/widgets/bar_chart.dart';
import 'package:natrium_wallet_flutter/ui/widgets/bitginko_datatable.dart';
import 'package:natrium_wallet_flutter/ui/util/routes.dart';
import 'package:natrium_wallet_flutter/ui/widgets/reactive_refresh.dart';
import 'package:natrium_wallet_flutter/ui/util/ui_util.dart';
import 'package:natrium_wallet_flutter/util/manta.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:natrium_wallet_flutter/util/sharedprefsutil.dart';
import 'package:natrium_wallet_flutter/util/hapticutil.dart';
import 'package:natrium_wallet_flutter/util/caseconverter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:natrium_wallet_flutter/bus/events.dart';
import 'dart:convert' as convert;

import '../app_icons2.dart';

class AppHistoryPage extends StatefulWidget {
  PriceConversion priceConversion;

  AppHistoryPage({this.priceConversion}) : super();

  @override
  _AppHistoryPageState createState() => _AppHistoryPageState();
}

class _AppHistoryPageState extends State<AppHistoryPage>
    with
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        FlareController {
  final GlobalKey<AppScaffoldState> _scaffoldKey =
      new GlobalKey<AppScaffoldState>();
  final Logger log = sl.get<Logger>();

  // Controller for placeholder card animations
  AnimationController _placeholderCardAnimationController;
  Animation<double> _opacityAnimation;
  bool _animationDisposed;

  // Manta
  bool mantaAnimationOpen;

  // Receive card instance
  AppReceiveSheet receive;

  // A separate unfortunate instance of this list, is a little unfortunate
  // but seems the only way to handle the animations
  final Map<String, GlobalKey<AnimatedListState>> _listKeyMap = Map();
  final Map<String, ListModel<AccountHistoryResponseItem>> _historyListMap =
      Map();

  // List of contacts (Store it so we only have to query the DB once for transaction cards)
  List<Contact> _contacts = List();

  // Price conversion state (BTC, NANO, NONE)
  PriceConversion _priceConversion;

  bool _isRefreshing = false;

  bool _lockDisabled = false; // whether we should avoid locking the app

  // Main card height
  double mainCardHeight;
  double settingsIconMarginTop = 5;
  double chartCardHeight;
  double tableCardHeight;
  // FCM instance
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // Animation for swiping to send
  ActorAnimation _sendSlideAnimation;
  ActorAnimation _sendSlideReleaseAnimation;
  double _fanimationPosition;
  bool releaseAnimation = false;

  void initialize(FlutterActorArtboard actor) {
    _fanimationPosition = 0.0;
    _sendSlideAnimation = actor.getAnimation("pull");
    _sendSlideReleaseAnimation = actor.getAnimation("release");
  }

  void setViewTransform(Mat2D viewTransform) {}

  bool advance(FlutterActorArtboard artboard, double elapsed) {
    if (releaseAnimation) {
      _sendSlideReleaseAnimation.apply(
          _sendSlideReleaseAnimation.duration * (1 - _fanimationPosition),
          artboard,
          1.0);
    } else {
      _sendSlideAnimation.apply(
          _sendSlideAnimation.duration * _fanimationPosition, artboard, 1.0);
    }
    return true;
  }

  /// Notification includes which account its for, automatically switch to it if they're entering app from notification
  Future<void> _chooseCorrectAccountFromNotification(
      Map<String, dynamic> message) async {
    // TODO repair this method
    return;
    /*
    try {
      if (message.containsKey("account")) {
        Account selectedAccount = await sl.get<DBHelper>().getSelectedAccount();
        if (message['account'] != selectedAccount.address) {
          List<Account> accounts = await sl.get<DBHelper>().getAccounts();
          for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].address == message['account']) {
              await sl.get<DBHelper>().changeAccount(accounts[i]);
              EventTaxiImpl.singleton()
                  .fire(AccountChangedEvent(account: accounts[i]));
              break;
            }
          }
        }
      }
    } catch (e) {
      log.severe(e.toString());
    }*/
  }

  @override
  void initState() {
    
    super.initState();
    _registerBus();
    this.mantaAnimationOpen = false;
    WidgetsBinding.instance.addObserver(this);
    if (widget.priceConversion != null) {
      _priceConversion = widget.priceConversion;
    } else {
      _priceConversion = PriceConversion.BTC;  
    }
    // Main Card Size
    if (_priceConversion == PriceConversion.BTC) {
      mainCardHeight = 120;
      chartCardHeight = 200;
      tableCardHeight = 200;
      settingsIconMarginTop = 7;
    } else if (_priceConversion == PriceConversion.NONE) {
      mainCardHeight = 64;
      chartCardHeight = 200;
      tableCardHeight = 200;
      settingsIconMarginTop = 7;
    } else if (_priceConversion == PriceConversion.HIDDEN) {
      mainCardHeight = 64;
      chartCardHeight = 200;
      tableCardHeight = 200;
      settingsIconMarginTop = 5;
    }
    _addSampleContact();
    _updateContacts();
    // Setup placeholder animation and start
    _animationDisposed = false;
    _placeholderCardAnimationController = new AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    _placeholderCardAnimationController
        .addListener(_animationControllerListener);
    _opacityAnimation = new Tween(begin: 1.0, end: 0.4).animate(
      CurvedAnimation(
        parent: _placeholderCardAnimationController,
        curve: Curves.easeIn,
        reverseCurve: Curves.easeOut,
      ),
    );
    _opacityAnimation.addStatusListener(_animationStatusListener);
    _placeholderCardAnimationController.forward();

    // johnnytodo: add loading icon when landing the page
    Future.delayed(new Duration(seconds: 1), () { // johnnytodo: copy this loading logic to other pages
      _refreshHistory();
      // setState(() {
      //   _isRefreshing = false;
      // });
    });

    
    // Register push notifications
    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     //print("onMessage: $message");
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     _chooseCorrectAccountFromNotification(message);
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     _chooseCorrectAccountFromNotification(message);
    //   },
    // );
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(sound: true, badge: true, alert: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   if (settings.alert || settings.badge || settings.sound) {
    //     sl.get<SharedPrefsUtil>().getNotificationsSet().then((beenSet) {
    //       if (!beenSet) {
    //         sl.get<SharedPrefsUtil>().setNotificationsOn(true);
    //       }
    //     });
    //     _firebaseMessaging.getToken().then((String token) {
    //       if (token != null) {
    //         EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //       }
    //     });
    //   } else {
    //     sl.get<SharedPrefsUtil>().setNotificationsOn(false).then((_) {
    //       _firebaseMessaging.getToken().then((String token) {
    //         EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //       });
    //     });
    //   }
    // });
    // _firebaseMessaging.getToken().then((String token) {
    //   if (token != null) {
    //     EventTaxiImpl.singleton().fire(FcmUpdateEvent(token: token));
    //   }
    // });
  }

  HistoryData historyDataInstance;
  HashMap chartData = new HashMap();
  List<String> monthList = new List<String>();
  Future<void> _refreshHistory() async {

    // get last 4 month title "yyyy-MM"
    List<DateTime> timeList = new List<DateTime>();
    
    var now = new DateTime.now();
    var monthFormatter = new DateFormat('yyyy-MM');
    var dayFormatter = new DateFormat('-dd');
    for (int i=0; i < 4; i++) {
        var tmp = new DateTime(now.year, now.month -3 + i, now.day);
        timeList.add(tmp);
        String month = monthFormatter.format(tmp);
        monthList.add(month);
        chartData.putIfAbsent(month, () => 0);
        // chartData[i] = {month, 0};
    }
    // print(monthList);
    // print(chartData);
    // print(dayFormatter.format(timeList.last));
    // print(dayFormatter.format(timeList.first));

    var tokenData2 = {
      'data': {
        'token': StateContainer.of(context).wallet.loginToken,
        'start_date': monthFormatter.format(timeList.first) + '-01', // i.e. 2020-10-01
        'end_date': monthFormatter.format(timeList.last) + dayFormatter.format(timeList.last.add(new Duration(days:1))),
      }
    };
    // print(monthFormatter.format(timeList.first) + '-01');
    // print(monthFormatter.format(timeList.last) + dayFormatter.format(timeList.last));

    Response response;
    Dio dio2 = new Dio();
    response = await dio2.post(
        // 'https://uat-bitginko.standardkepler.com/api/v4/uri/getUserHistory.api.php', // UAT
        'https://bitginko.com/api/v4/uri/getUserHistory.api.php', // PRD
        data: FormData.fromMap(tokenData2),
        options: Options(responseType: ResponseType.plain,));

    var historyObj = convert.jsonDecode(response.toString());
    historyDataInstance = new HistoryData.fromJson(historyObj); // johnny

    // print("history:");
    // print(response);
    
    if (historyDataInstance.statusCode == 0) {
      setState(() {
        // print('history_page. _refreshHistory. setState()');

        // List<HData> historyList = historyDataInstance.data;      

        // foreach month, calculate the trade if there is any BUY / SELL
        for (String month in monthList) {
          double tmpCount = 0;
          for (HData hd in historyDataInstance.data) {

            if (hd.t_target != "HKD") {

              if (hd.t_date.substring(0,7) == month) {
                if (hd.t_action == "BUY") {
                  tmpCount += double.parse(hd.t_filled_amt);
                  // print(hd.t_date);
                  // print("BUY");
                  // print(double.parse(hd.t_filled_amt));
                  
                } else if (hd.t_action == "SELL") {
                  tmpCount += double.parse(hd.t_consideration);
                  // print(hd.t_date);
                  // print("SELL");
                  // print(double.parse(hd.t_consideration));
                }
              }

            }
            
          }
          // make tmpCount into int.
          // print(tmpCount);
          chartData[month] = tmpCount.toInt();
          // print('interger hereherherherher');
          // print(tmpCount.toInt());
        }


      });

    } else {
      print('history_page. Request failed with status: ${response.statusCode}.');
    }
  }

  void _animationStatusListener(AnimationStatus status) {
    switch (status) {
      case AnimationStatus.dismissed:
        _placeholderCardAnimationController.forward();
        break;
      case AnimationStatus.completed:
        _placeholderCardAnimationController.reverse();
        break;
      default:
        return null;
    }
  }

  void _animationControllerListener() {
    setState(() {});
  }

  void _startAnimation() {
    if (_animationDisposed) {
      _animationDisposed = false;
      _placeholderCardAnimationController
          .addListener(_animationControllerListener);
      _opacityAnimation.addStatusListener(_animationStatusListener);
      _placeholderCardAnimationController.forward();
    }
  }

  void _disposeAnimation() {
    if (!_animationDisposed) {
      _animationDisposed = true;
      _opacityAnimation.removeStatusListener(_animationStatusListener);
      _placeholderCardAnimationController
          .removeListener(_animationControllerListener);
      _placeholderCardAnimationController.stop();
    }
  }

  /// Add donations contact if it hasnt already been added
  Future<void> _addSampleContact() async {
    bool contactAdded = await sl.get<SharedPrefsUtil>().getFirstContactAdded();
    if (!contactAdded) {
      bool addressExists = await sl.get<DBHelper>().contactExistsWithAddress(
          "nano_1natrium1o3z5519ifou7xii8crpxpk8y65qmkih8e8bpsjri651oza8imdd");
      if (addressExists) {
        return;
      }
      bool nameExists =
          await sl.get<DBHelper>().contactExistsWithName("@NatriumDonations");
      if (nameExists) {
        return;
      }
      await sl.get<SharedPrefsUtil>().setFirstContactAdded(true);
      Contact c = Contact(
          name: "@NatriumDonations",
          address:
              "nano_1natrium1o3z5519ifou7xii8crpxpk8y65qmkih8e8bpsjri651oza8imdd");
      await sl.get<DBHelper>().saveContact(c);
    }
  }

  void _updateContacts() {
    sl.get<DBHelper>().getContacts().then((contacts) {
      setState(() {
        _contacts = contacts;
      });
    });
  }

  StreamSubscription<HistoryHomeEvent> _historySub;
  StreamSubscription<ContactModifiedEvent> _contactModifiedSub;
  StreamSubscription<SendCompleteEvent> _sendCompleteSub;
  StreamSubscription<DisableLockTimeoutEvent> _disableLockSub;
  StreamSubscription<AccountChangedEvent> _switchAccountSub;

  void _registerBus() {
    _historySub = EventTaxiImpl.singleton()
        .registerTo<HistoryHomeEvent>()
        .listen((event) {
      diffAndUpdateHistoryList(event.items);
      setState(() {
        _isRefreshing = false;
      });
      if (StateContainer.of(context).initialDeepLink != null) {
        handleDeepLink(StateContainer.of(context).initialDeepLink);
        StateContainer.of(context).initialDeepLink = null;
      }
    });
    _sendCompleteSub = EventTaxiImpl.singleton()
        .registerTo<SendCompleteEvent>()
        .listen((event) {
      // Route to send complete if received process response for send block
      if (event.previous != null) {
        // Route to send complete
        sl
            .get<DBHelper>()
            .getContactWithAddress(event.previous.link)
            .then((contact) {
          // String contactName = contact == null ? null : contact.name;
          Navigator.of(context).popUntil(RouteUtils.withNameLike('/home'));
          Sheets.showAppHeightNineSheet(
              context: context,
              closeOnTap: true,
              removeUntilHome: true,
              widget: SendCompleteSheet(
                  amountRaw: event.previous.sendAmount,
                  destination: event.previous.link,
                  // contactName: contactName,
                  // localAmount: event.previous.localCurrencyValue,
                  // paymentRequest: event.previous.paymentRequest
                  ));
        });
      }
    });
    _contactModifiedSub = EventTaxiImpl.singleton()
        .registerTo<ContactModifiedEvent>()
        .listen((event) {
      _updateContacts();
    });
    // Hackish event to block auto-lock functionality
    _disableLockSub = EventTaxiImpl.singleton()
        .registerTo<DisableLockTimeoutEvent>()
        .listen((event) {
      if (event.disable) {
        cancelLockEvent();
      }
      _lockDisabled = event.disable;
    });
    // User changed account
    _switchAccountSub = EventTaxiImpl.singleton()
        .registerTo<AccountChangedEvent>()
        .listen((event) {
      setState(() {
        StateContainer.of(context).wallet.loading = true;
        StateContainer.of(context).wallet.historyLoading = true;
        _startAnimation();
        StateContainer.of(context).updateWallet(account: event.account);
      });
      // paintQrCode(address: event.account.address);
      if (event.delayPop) {
        Future.delayed(Duration(milliseconds: 300), () {
          Navigator.of(context).popUntil(RouteUtils.withNameLike("/home"));
        });
      } else if (!event.noPop) {
        Navigator.of(context).popUntil(RouteUtils.withNameLike("/home"));
      }
    });
  }

  @override
  void dispose() {
    _destroyBus();
    WidgetsBinding.instance.removeObserver(this);
    _placeholderCardAnimationController.dispose();
    super.dispose();
  }

  void _destroyBus() {
    if (_historySub != null) {
      _historySub.cancel();
    }
    if (_contactModifiedSub != null) {
      _contactModifiedSub.cancel();
    }
    if (_sendCompleteSub != null) {
      _sendCompleteSub.cancel();
    }
    if (_disableLockSub != null) {
      _disableLockSub.cancel();
    }
    if (_switchAccountSub != null) {
      _switchAccountSub.cancel();
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Handle websocket connection when app is in background
    // terminate it to be eco-friendly
    switch (state) {
      case AppLifecycleState.paused:
        setAppLockEvent();
        StateContainer.of(context).disconnect();
        super.didChangeAppLifecycleState(state);
        break;
      case AppLifecycleState.resumed:
        cancelLockEvent();
        StateContainer.of(context).reconnect();
        if (!StateContainer.of(context).wallet.loading &&
            StateContainer.of(context).initialDeepLink != null) {
          handleDeepLink(StateContainer.of(context).initialDeepLink);
          StateContainer.of(context).initialDeepLink = null;
        }
        super.didChangeAppLifecycleState(state);
        break;
      default:
        super.didChangeAppLifecycleState(state);
        break;
    }
  }

  // To lock and unlock the app
  StreamSubscription<dynamic> lockStreamListener;

  Future<void> setAppLockEvent() async {
    if (((await sl.get<SharedPrefsUtil>().getLock()) ||
            StateContainer.of(context).encryptedSecret != null) &&
        !_lockDisabled) {
      if (lockStreamListener != null) {
        lockStreamListener.cancel();
      }
      Future<dynamic> delayed = new Future.delayed(
          (await sl.get<SharedPrefsUtil>().getLockTimeout()).getDuration());
      delayed.then((_) {
        return true;
      });
      lockStreamListener = delayed.asStream().listen((_) {
        try {
          StateContainer.of(context).resetEncryptedSecret();
        } catch (e) {
          log.w(
              "Failed to reset encrypted secret when locking ${e.toString()}");
        } finally {
          Navigator.of(context)
              .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
        }
      });
    }
  }

  Future<void> cancelLockEvent() async {
    if (lockStreamListener != null) {
      lockStreamListener.cancel();
    }
  }

  // Used to build list items that haven't been removed.
  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    String displayName = smallScreen(context)
        ? _historyListMap[StateContainer.of(context).wallet.address][index]
            .getShorterString()
        : _historyListMap[StateContainer.of(context).wallet.address][index]
            .getShortString();
    _contacts.forEach((contact) {
      if (contact.address ==
          _historyListMap[StateContainer.of(context).wallet.address][index]
              .account
              .replaceAll("xrb_", "nano_")) {
        displayName = contact.name;
      }
    });
    return _buildTransactionCard(
        _historyListMap[StateContainer.of(context).wallet.address][index],
        animation,
        displayName,
        context);
  }

  // Return widget for list
  Widget _getListWidget(BuildContext context) {
    if (historyDataInstance == null) {
      // johnnytodo: add back loading logic here, after the api work is done
      //  || StateContainer.of(context).wallet.historyLoading) { 

      // Loading Animation
      return ReactiveRefreshIndicator(
          backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
          onRefresh: _refresh,
          isRefreshing: _isRefreshing,
          child: ListView(
            padding: EdgeInsetsDirectional.fromSTEB(0, 5.0, 0, 15.0),
            children: <Widget>[
              _buildLoadingTransactionCard(
                  "Sent", "10244000", "123456789121234", context),
              _buildLoadingTransactionCard(
                  "Received", "100,00000", "@bbedwards1234", context),
              _buildLoadingTransactionCard(
                  "Received", "100,00000", "@bbedwards1234", context),
              _buildLoadingTransactionCard(
                  "Received", "100,00000", "@bbedwards1234", context),
              _buildLoadingTransactionCard(
                  "Received", "100,00000", "@bbedwards1234", context),
            ],
          ));
    // } else if (StateContainer.of(context).wallet.history.length == 0) {
      } else if (historyDataInstance.data != null) { 
      _disposeAnimation();
      return ReactiveRefreshIndicator(
        backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
        child: ListView(
          padding: EdgeInsetsDirectional.fromSTEB(0, 5.0, 0, 15.0),
          children: 
            // _buildWelcomeTransactionCard(context),

            _buildDummyTransactionCardList(historyDataInstance.data),

        ),
        onRefresh: _refresh,
        isRefreshing: _isRefreshing,
      );
    } else {
      _disposeAnimation();
    }
    // Setup history list
    if (!_listKeyMap.containsKey(StateContainer.of(context).wallet.address)) {
      _listKeyMap.putIfAbsent(StateContainer.of(context).wallet.address,
          () => GlobalKey<AnimatedListState>());
      setState(() {
        _historyListMap.putIfAbsent(
            StateContainer.of(context).wallet.address,
            () => ListModel<AccountHistoryResponseItem>(
                  listKey:
                      _listKeyMap[StateContainer.of(context).wallet.address],
                  initialItems: StateContainer.of(context).wallet.history,
                ));
      });
    }
    return ReactiveRefreshIndicator(
      backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
      child: AnimatedList(
        key: _listKeyMap[StateContainer.of(context).wallet.address],
        padding: EdgeInsetsDirectional.fromSTEB(0, 5.0, 0, 15.0),
        initialItemCount:
            _historyListMap[StateContainer.of(context).wallet.address].length,
        itemBuilder: _buildItem,
      ),
      onRefresh: _refresh,
      isRefreshing: _isRefreshing,
    );
  }

  // Refresh list
  Future<void> _refresh() async {
    setState(() {
      _isRefreshing = true;
    });
    sl.get<HapticUtil>().success();
    StateContainer.of(context).requestUpdate();
    // Hide refresh indicator after 3 seconds if no server response
    Future.delayed(new Duration(seconds: 3), () {
      setState(() {
        _isRefreshing = false;
      });
    });
  }

  List<Widget> _buildDummyTransactionCardList(List<HData> historyList) {
    var outputList = List<Widget>();

    for (HData h in historyList) {
      print(h.t_date);
      if ( (h.t_action=="BUY" || h.t_action=="SELL") && h.t_target=="HKD" ){
        // not displaying this 
      } else {
        String priceDisplay = "";
        Decimal filledAmt = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(h.t_filled_amt);
        Decimal consideration = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(h.t_consideration);

        if (h.t_action == "SELL") {
          Decimal price = consideration/filledAmt;
          priceDisplay = NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(price.toDouble());
        } else if (h.t_action == "BUY") {
          Decimal price = filledAmt/consideration;
          priceDisplay = NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(price.toDouble());
        } else if (h.t_action == "Transfer_In") {

        } else if (h.t_action == "Transfer_Out") {

        }

        outputList.add(_buildDummyTransactionCard(h, priceDisplay, context));
      }
        
    }
    return outputList;
  }

  ///
  /// Because there's nothing convenient like DiffUtil, some manual logic
  /// to determine the differences between two lists and to add new items.
  ///
  /// Depends on == being overriden in the AccountHistoryResponseItem class
  ///
  /// Required to do it this way for the animation
  ///
  void diffAndUpdateHistoryList(List<AccountHistoryResponseItem> newList) {
    if (newList == null ||
        newList.length == 0 ||
        _historyListMap[StateContainer.of(context).wallet.address] == null)
      return;
    // Get items not in current list, and add them from top-down
    newList.reversed
        .where((item) =>
            !_historyListMap[StateContainer.of(context).wallet.address]
                .items
                .contains(item))
        .forEach((historyItem) {
      setState(() {
        _historyListMap[StateContainer.of(context).wallet.address]
            .insertAtTop(historyItem);
      });
    });
    // Re-subscribe if missing data
    if (StateContainer.of(context).wallet.loading) {
      StateContainer.of(context).requestSubscribe();
    }
  }

  Future<void> handleDeepLink(link) async {
    Address address = Address(link);
    if (address.isValid()) {
      String amount;
      String contactName;
      if (address.amount != null) {
        // Require minimum 1 rai to send
        if (BigInt.parse(address.amount) >= BigInt.from(10).pow(24)) {
          amount = address.amount;
        }
      }
      // See if a contact
      Contact contact =
          await sl.get<DBHelper>().getContactWithAddress(address.address);
      if (contact != null) {
        contactName = contact.name;
      }
      // Remove any other screens from stack
      Navigator.of(context).popUntil(RouteUtils.withNameLike('/home'));
      if (amount != null) {
        // Go to send confirm with amount
        Sheets.showAppHeightNineSheet(
            context: context,
            widget: SendConfirmSheet(
                amountRaw: amount,
                destination: address.address,
                contactName: contactName));
      } else {
        // Go to send with address
        Sheets.showAppHeightNineSheet(
            context: context,
            widget: SendSheet(
                localCurrency: StateContainer.of(context).curCurrency,
                // contact: contact, // johnny remove contact
                address: address.address));
      }
    } else if (MantaWallet.parseUrl(link) != null) {
      // Manta URI handling
      try {
        _showMantaAnimation();
        // Get manta payment request
        MantaWallet manta = MantaWallet(link);
        PaymentRequestMessage paymentRequest =
            await MantaUtil.getPaymentDetails(manta);
        if (mantaAnimationOpen) {
          Navigator.of(context).pop();
        }
        MantaUtil.processPaymentRequest(context, manta, paymentRequest);
      } catch (e) {
        if (mantaAnimationOpen) {
          Navigator.of(context).pop();
        }
        UIUtil.showSnackbar(AppLocalization.of(context).mantaError, context);
      }
    }
  }

  void _showMantaAnimation() {
    mantaAnimationOpen = true;
    Navigator.of(context).push(AnimationLoadingOverlay(
        AnimationType.MANTA,
        StateContainer.of(context).curTheme.animationOverlayStrong,
        StateContainer.of(context).curTheme.animationOverlayMedium,
        onPoppedCallback: () => mantaAnimationOpen = false));
  }


  @override
  Widget build(BuildContext context) {
    print('history building =======');

    return AppScaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      backgroundColor: StateContainer.of(context).curTheme.background,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: StateContainer.of(context).curTheme.backgroundDark,
        type: BottomNavigationBarType.fixed,
        currentIndex: 2,
        onTap: (int idx) {
              // setState(() {index = idx;}),
              print('BottomNavigationBar idx history:' + idx.toString());
              if (idx == 0) {
                Navigator.of(context).pushNamed('/wallet'); 
              } else if (idx == 1) {
                Navigator.of(context).pushNamed('/invest'); 
              } else if (idx == 2) {
                Navigator.of(context).pushNamed('/history'); 
              } else if (idx == 3) {
                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                  return UIUtil.showWebview(context,'https://bitginko.com/support');
                }));
              }

            },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.account_balance_wallet, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.account_balance_wallet, color: StateContainer.of(context).curTheme.primary), title: Text('錢包', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
          BottomNavigationBarItem(icon: Icon(Icons.attach_money, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.attach_money, color: StateContainer.of(context).curTheme.primary), title: Text('交易', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
          BottomNavigationBarItem(icon: Icon(Icons.history, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.history, color: StateContainer.of(context).curTheme.primary), title: Text('歷史', style: TextStyle(color: StateContainer.of(context).curTheme.primary60))),
          BottomNavigationBarItem(icon: Icon(Icons.chat, color: StateContainer.of(context).curTheme.text), activeIcon: Icon(Icons.chat, color: StateContainer.of(context).curTheme.primary), title: Text('客服', style: TextStyle(color: StateContainer.of(context).curTheme.text))),
        ]),
      // drawer: SizedBox(
      //   width: UIUtil.drawerWidth(context),
      //   child: AppDrawer(
      //     child: SettingsSheet(),
      //   ),
      // ),
      body: SafeArea(
        minimum: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.045, // johnnytodo: learn this ratio method, to decide the height at diff device
            // bottom: MediaQuery.of(context).size.height * 0.035
            ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  //Everything else
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //Main Card
                      // _buildMainCard(context, _scaffoldKey),
                      //Main Card End

                      //Transactions Text
                      Container(
                        margin: EdgeInsetsDirectional.fromSTEB(
                            30, 20.0, 0.0, 0.0), // 30.0, 20.0, 26.0, 0.0
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              // CaseChange.toUpperCase(
                              //     AppLocalization.of(context).transactions,
                              //     context),
                              "交易量 (每月)", // johnnyTranslate
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.w100,
                                color: StateContainer.of(context).curTheme.text,
                              ),
                            ),
                            
                          ],
                        ),
                      ), //Transactions Text End

                      // the bar chart data
                      Container(
                        margin: EdgeInsets.only(top: 5,),
                        padding: EdgeInsets.only(bottom: 10, left: 15),
                        height: 200,
                        // height:
                        //     screenAwareSize(_media.longestSide <= 775 ? 180 : 130, context),
                        decoration: BoxDecoration(
                          color: StateContainer.of(context).curTheme.backgroundDark,
                          // borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [StateContainer.of(context).curTheme.boxShadow],
                        ),
                        child: SimpleBarChart.withSampleData(monthList, chartData),
                      ),

                      // the text of 'history'
                      Container(
                        margin: EdgeInsetsDirectional.fromSTEB(
                            30.0, 20.0, 26.0, 0.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              // CaseChange.toUpperCase(
                              //     AppLocalization.of(context).transactions,
                              //     context),
                              "歷史", // johnnyTranslate
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.w100,
                                color: StateContainer.of(context).curTheme.text,
                              ),
                            ),
                          ],
                        ),
                      ), 

                      // the action history
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            
                            _getListWidget(context),

                            //List Top Gradient End (the top shade)
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 10.0,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      StateContainer.of(context)
                                          .curTheme
                                          .background00,
                                      StateContainer.of(context)
                                          .curTheme
                                          .background
                                    ],
                                    begin: AlignmentDirectional(0.5, 1.0),
                                    end: AlignmentDirectional(0.5, -1.0),
                                  ),
                                ),
                              ),
                            ), // List Top Gradient End

                            //List Bottom Gradient (the bottom shade)
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 10.0,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      StateContainer.of(context).curTheme.background00,
                                      StateContainer.of(context).curTheme.background
                                    ],
                                    begin: AlignmentDirectional(0.5, -1),
                                    end: AlignmentDirectional(0.5, 0.5),
                                  ),
                                ),
                              ),
                            ), //List Bottom Gradient End

                          ],
                        ),
                      ), //Transactions List End

                      
                      //Buttons background
                      // SizedBox(
                      //   height: 55,
                      //   width: MediaQuery.of(context).size.width,
                      // ), //Buttons background
                    ],
                  ),
                  // Buttons
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.end,
                  //   children: <Widget>[
                  //     Container(
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(100),
                  //         boxShadow: [
                  //           StateContainer.of(context).curTheme.boxShadowButton
                  //         ],
                  //       ),
                  //       height: 55,
                  //       width: (MediaQuery.of(context).size.width - 42) / 2,
                  //       margin: EdgeInsetsDirectional.only(
                  //           start: 14, top: 0.0, end: 7.0),
                  //       child: FlatButton(
                  //         shape: RoundedRectangleBorder(
                  //             borderRadius: BorderRadius.circular(20.0)),
                  //         color: receive != null
                  //             ? StateContainer.of(context).curTheme.primary
                  //             : StateContainer.of(context).curTheme.primary60,
                  //         child: AutoSizeText(
                  //           AppLocalization.of(context).receive,
                  //           textAlign: TextAlign.center,
                  //           style: AppStyles.textStyleButtonPrimary(context),
                  //           maxLines: 1,
                  //           stepGranularity: 0.5,
                  //         ),
                  //         onPressed: () {
                  //           if (receive == null) {
                  //             return;
                  //           }
                  //           // receive.mainBottomSheet(context);
                  //         },
                  //         highlightColor: receive != null
                  //             ? StateContainer.of(context).curTheme.background40
                  //             : Colors.transparent,
                  //         splashColor: receive != null
                  //             ? StateContainer.of(context).curTheme.background40
                  //             : Colors.transparent,
                  //       ),
                  //     ),
                  //     AppPopupButton(),
                  //   ],
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Transaction Card/List Item
  Widget _buildTransactionCard(AccountHistoryResponseItem item,
      Animation<double> animation, String displayName, BuildContext context) {
    String text;
    IconData icon;
    Color iconColor;
    if (item.type == BlockTypes.SEND) {
      text = AppLocalization.of(context).sent;
      icon = AppIcons.sent;
      iconColor = StateContainer.of(context).curTheme.text60;
    } else {
      text = AppLocalization.of(context).received;
      icon = AppIcons.received;
      iconColor = StateContainer.of(context).curTheme.primary60;
    }
    return Slidable(
      delegate: SlidableScrollDelegate(),
      actionExtentRatio: 0.35,
      movementDuration: Duration(milliseconds: 300),
      enabled: StateContainer.of(context).wallet != null &&
          StateContainer.of(context).wallet.accountBalance > BigInt.zero,
      onTriggered: (preempt) {
        if (preempt) {
          setState(() {
            releaseAnimation = true;
          });
        } else {
          // See if a contact
          sl
              .get<DBHelper>()
              .getContactWithAddress(item.account)
              .then((contact) {
            // Go to send with address
            Sheets.showAppHeightNineSheet(
                context: context,
                widget: SendSheet(
                  localCurrency: StateContainer.of(context).curCurrency,
                  // contact: contact, // johnny remove contact
                  address: item.account,
                  quickSendAmount: item.amount,
                ));
          });
        }
      },
      onAnimationChanged: (animation) {
        if (animation != null) {
          _fanimationPosition = animation.value;
          if (animation.value == 0.0 && releaseAnimation) {
            setState(() {
              releaseAnimation = false;
            });
          }
        }
      },
      secondaryActions: <Widget>[
        SlideAction(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            margin: EdgeInsetsDirectional.only(
                end: MediaQuery.of(context).size.width * 0.15,
                top: 4,
                bottom: 4),
            child: Container(
              alignment: AlignmentDirectional(-0.5, 0),
              constraints: BoxConstraints.expand(),
              child: FlareActor("assets/pulltosend_animation.flr",
                  animation: "pull",
                  fit: BoxFit.contain,
                  controller: this,
                  color: StateContainer.of(context).curTheme.primary),
            ),
          ),
        ),
      ],
      child: _SizeTransitionNoClip(
        sizeFactor: animation,
        child: Container(
          margin: EdgeInsetsDirectional.fromSTEB(14.0, 4.0, 14.0, 4.0),
          decoration: BoxDecoration(
            color: StateContainer.of(context).curTheme.backgroundDark,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [StateContainer.of(context).curTheme.boxShadow],
          ),
          child: FlatButton(
            highlightColor: StateContainer.of(context).curTheme.text15,
            splashColor: StateContainer.of(context).curTheme.text15,
            color: StateContainer.of(context).curTheme.backgroundDark,
            padding: EdgeInsets.all(0.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            onPressed: () {
              Sheets.showAppHeightEightSheet(
                  context: context,
                  widget: TransactionDetailsSheet(
                      hash: item.hash,
                      address: item.account,
                      displayName: displayName),
                  animationDurationMs: 175);
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 14.0, horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                            margin: EdgeInsetsDirectional.only(end: 16.0),
                            child: Icon(icon, color: iconColor, size: 20)),
                        Container(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                text,
                                textAlign: TextAlign.start,
                                style:
                                    AppStyles.textStyleTransactionType(context),
                              ),
                              RichText(
                                textAlign: TextAlign.start,
                                text: TextSpan(
                                  text: '',
                                  children: [
                                    TextSpan(
                                      text: item.getFormattedAmount(),
                                      style:
                                          AppStyles.textStyleTransactionAmount(
                                              context),
                                    ),
                                    TextSpan(
                                      text: " NANO",
                                      style: AppStyles.textStyleTransactionUnit(
                                          context),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 2.4,
                      child: Text(
                        displayName,
                        textAlign: TextAlign.end,
                        style: AppStyles.textStyleTransactionAddress(context),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  } //Transaction Card End

  String _get4decimalsNum(String bal) {
    Decimal converted = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(bal);
    return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(converted.toDouble());
  }

  // Dummy Transaction Card
  Widget _buildDummyTransactionCard(HData hd, String price, BuildContext context) {
    
    String action = hd.t_action;
    String actionDisplay = "";
    String symbol = hd.ticker;
    String amount = action == "BUY" ? _get4decimalsNum(hd.t_consideration) : _get4decimalsNum(hd.t_filled_amt);
    String status = hd.t_status;
    String time = hd.t_date;
    
    TextStyle buySellStyle;
    buySellStyle = action == "BUY" ? AppStyles.textStyleBuy(context) : AppStyles.textStyleSell(context);

    Color iconColor = StateContainer.of(context).curTheme.primary60;
    IconData icon = AppIcons2.hkd;
    if (symbol == "ETH") {
      icon = AppIcons2.eth;
    } else if (symbol == "LTC") {
      icon = AppIcons2.ltc;
    } else if (symbol == "USDT") {
      icon = AppIcons2.usdt;
    } else if (symbol == "BCH") {
      icon = AppIcons2.bch;
    } else if (symbol == "BTC") {
      icon = AppIcons2.btc;
    } else if (symbol == "DOGE") {
      icon = AppIcons2.doge;
    }

    // johnnyTranslate
    if (action == "BUY")
      actionDisplay = "買入";
    if (action == "SELL")
      actionDisplay = "賣出";
    if (action == "Transfer_In")
      actionDisplay = "存入";
    else if (action == "Transfer_Out")
      actionDisplay = "提出";
    else if (action == "RP")
      actionDisplay = "兌換";
    else if (action == "PLAN")
      actionDisplay = "月供"; // regualr buy (monthly)
    else if (action == "COMM")
      actionDisplay = "佣金";
    else if (action == "DEPOSIT_LOCK")
      actionDisplay = "鎖倉";
    else if (action == "LOCK_DONE")
      actionDisplay = "派息";

    // johnnyTranslate
    if (status == "P")
      status = "待處理";
    if (status == "T")
      status = "失敗";
    if (status == "F")
      status = "完成";
    if (status == "S")
      status = "完成";
    
    return Container(
      margin: EdgeInsetsDirectional.fromSTEB(14.0, 4.0, 14.0, 4.0),
      decoration: BoxDecoration(
        color: StateContainer.of(context).curTheme.backgroundDark,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [StateContainer.of(context).curTheme.boxShadow],
        // border: Border(top: BorderSide(color: StateContainer.of(context).curTheme.text20)),
      ),
      child: FlatButton(
        onPressed: () {
          return null;
        },
        highlightColor: StateContainer.of(context).curTheme.text15,
        splashColor: StateContainer.of(context).curTheme.text15,
        color: StateContainer.of(context).curTheme.backgroundDark,
        padding: EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Center(
          child: Padding(
            padding:
                const EdgeInsets.fromLTRB(0, 10, 10, 2),
                // const EdgeInsets.symmetric(vertical: 14.0, horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[

                      // 1st column
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        // height: 75,
                        width: MediaQuery.of(context).size.width / 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Container(
                                // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                                margin: EdgeInsetsDirectional.fromSTEB(0.0, 0, 0.0, 0.0),
                                child: Text(
                                  actionDisplay,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                  fontFamily: "NunitoSans",
                                  fontSize: AppFontSizes.small,
                                  fontWeight: FontWeight.w600,
                                  color: action == "BUY" ? Colors.green 
                                            : action == "SELL" ? Colors.red
                                            : StateContainer.of(context).curTheme.text), // johnnyFuture color choice
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                  // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                                  margin: EdgeInsetsDirectional.fromSTEB(4.0, 5, 4.0, 0),
                                  child: Icon(icon, color: iconColor, size: 22)
                                ),
                            )

                          ]
                        )
                      ),

                      // 2nd column
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        // height: 75,
                        width: MediaQuery.of(context).size.width / 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              margin: EdgeInsetsDirectional.only(top: 1.0),
                              child: Text(
                                symbol,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: "NunitoSans",
                                  fontSize: AppFontSizes.medium,
                                  fontWeight: FontWeight.w700,
                                  color: StateContainer.of(context).curTheme.text),
                              ),
                            ),
                            Container(
                              // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              margin: EdgeInsetsDirectional.only(top: 1.0),
                              child: Text(
                                time,
                                textAlign: TextAlign.start,
                                style: AppStyles.textStyleTransactionAddress(context),
                              ),
                            ),

                            
                          ],
                        ),
                      ),
                      
                    // 3rd column
                    Container(
                      // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                      // height: 75,
                      width: MediaQuery.of(context).size.width / 5,
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[

                            Text(
                              price != "" ? "價格:" : "", // johnnyTranslate
                              textAlign: TextAlign.start,
                              style: AppStyles.textStyleTransactionAddress(context),
                            ),
                            Text(
                              price != "" ? "\$"+price : "",
                              textAlign: TextAlign.start,
                              style: AppStyles.textStyleTransactionType(context),
                            ),

                        ]
                      )
                    ),


                    // 4th column
                      Container(
                        // decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                        margin: EdgeInsetsDirectional.fromSTEB(0, 1, 0, 0),
                        // height: 75,
                        width: MediaQuery.of(context).size.width / 4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[

                              Text(
                                amount + " " + symbol,
                                textAlign: TextAlign.end,
                                style: AppStyles.textStyleTransactionType(context),
                              ),
                              Text(
                                status,
                                textAlign: TextAlign.end,
                                style: AppStyles.textStyleTransactionAmount(context),
                              ),

                          ]
                        )
                        
                      ),



                
              ],
            ),
          ),
        ),
      ),
    );
  } //Dummy Transaction Card End

  // Welcome Card
  TextSpan _getExampleHeaderSpan(BuildContext context) {
    String workingStr;
    if (StateContainer.of(context).selectedAccount == null ||
        StateContainer.of(context).selectedAccount.index == 0) {
      workingStr = AppLocalization.of(context).exampleCardIntro;
    } else {
      workingStr = AppLocalization.of(context).newAccountIntro;
    }
    if (!workingStr.contains("NANO")) {
      return TextSpan(
        text: workingStr,
        style: AppStyles.textStyleTransactionWelcome(context),
      );
    }
    // Colorize NANO
    List<String> splitStr = workingStr.split("NANO");
    if (splitStr.length != 2) {
      return TextSpan(
        text: workingStr,
        style: AppStyles.textStyleTransactionWelcome(context),
      );
    }
    return TextSpan(
      text: '',
      children: [
        TextSpan(
          text: splitStr[0],
          style: AppStyles.textStyleTransactionWelcome(context),
        ),
        TextSpan(
          text: "NANO",
          style: AppStyles.textStyleTransactionWelcomePrimary(context),
        ),
        TextSpan(
          text: splitStr[1],
          style: AppStyles.textStyleTransactionWelcome(context),
        ),
      ],
    );
  }

  Widget _buildWelcomeTransactionCard(BuildContext context) {
    return Container(
      margin: EdgeInsetsDirectional.fromSTEB(14.0, 4.0, 14.0, 4.0),
      decoration: BoxDecoration(
        color: StateContainer.of(context).curTheme.backgroundDark,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [StateContainer.of(context).curTheme.boxShadow],
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 7.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0)),
                color: StateContainer.of(context).curTheme.primary,
                boxShadow: [StateContainer.of(context).curTheme.boxShadow],
              ),
            ),
            Flexible(
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 14.0, horizontal: 15.0),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: _getExampleHeaderSpan(context),
                ),
              ),
            ),
            Container(
              width: 7.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
                color: StateContainer.of(context).curTheme.primary,
              ),
            ),
          ],
        ),
      ),
    );
  } // Welcome Card End

  // Loading Transaction Card
  Widget _buildLoadingTransactionCard(
      String type, String amount, String address, BuildContext context) {
    String text;
    IconData icon;
    Color iconColor;
    if (type == "Sent") {
      text = "Senttt";
      icon = AppIcons.dotfilled;
      iconColor = StateContainer.of(context).curTheme.text20;
    } else {
      text = "Receiveddd";
      icon = AppIcons.dotfilled;
      iconColor = StateContainer.of(context).curTheme.primary20;
    }
    return Container(
      margin: EdgeInsetsDirectional.fromSTEB(14.0, 4.0, 14.0, 4.0),
      decoration: BoxDecoration(
        color: StateContainer.of(context).curTheme.backgroundDark,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [StateContainer.of(context).curTheme.boxShadow],
      ),
      child: FlatButton(
        onPressed: () {
          return null;
        },
        highlightColor: StateContainer.of(context).curTheme.text15,
        splashColor: StateContainer.of(context).curTheme.text15,
        color: StateContainer.of(context).curTheme.backgroundDark,
        padding: EdgeInsets.all(0.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Center(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 14.0, horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    // Transaction Icon
                    Opacity(
                      opacity: _opacityAnimation.value,
                      child: Container(
                          margin: EdgeInsetsDirectional.only(end: 16.0),
                          child: Icon(icon, color: iconColor, size: 20)),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // Transaction Type Text
                          Container(
                            child: Stack(
                              alignment: AlignmentDirectional(-1, 0),
                              children: <Widget>[
                                Text(
                                  text,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    fontSize: AppFontSizes.small,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.transparent,
                                  ),
                                ),
                                Opacity(
                                  opacity: _opacityAnimation.value,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .text45,
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    child: Text(
                                      text,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        fontSize: AppFontSizes.small - 4,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.transparent,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // Amount Text
                          Container(
                            child: Stack(
                              alignment: AlignmentDirectional(-1, 0),
                              children: <Widget>[
                                Text(
                                  amount,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      color: Colors.transparent,
                                      fontSize: AppFontSizes.smallest,
                                      fontWeight: FontWeight.w600),
                                ),
                                Opacity(
                                  opacity: _opacityAnimation.value,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .primary20,
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    child: Text(
                                      amount,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontFamily: "NunitoSans",
                                          color: Colors.transparent,
                                          fontSize: AppFontSizes.smallest - 3,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                // Address Text
                Container(
                  width: MediaQuery.of(context).size.width / 2.4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        child: Stack(
                          alignment: AlignmentDirectional(1, 0),
                          children: <Widget>[
                            Text(
                              address,
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: AppFontSizes.smallest,
                                fontFamily: 'OverpassMono',
                                fontWeight: FontWeight.w100,
                                color: Colors.transparent,
                              ),
                            ),
                            Opacity(
                              opacity: _opacityAnimation.value,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .text20,
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                child: Text(
                                  address,
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontSize: AppFontSizes.smallest - 3,
                                    fontFamily: 'OverpassMono',
                                    fontWeight: FontWeight.w100,
                                    color: Colors.transparent,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  } // Loading Transaction Card End

}

class TransactionDetailsSheet extends StatefulWidget {
  final String hash;
  final String address;
  final String displayName;

  TransactionDetailsSheet({this.hash, this.address, this.displayName})
      : super();

  _TransactionDetailsSheetState createState() =>
      _TransactionDetailsSheetState();
}

class _TransactionDetailsSheetState extends State<TransactionDetailsSheet> {
  // Current state references
  bool _addressCopied = false;
  // Timer reference so we can cancel repeated events
  Timer _addressCopiedTimer;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.035,
      ),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                // A stack for Copy Address and Add Contact buttons
                Stack(
                  children: <Widget>[
                    // A row for Copy Address Button
                    Row(
                      children: <Widget>[
                        AppButton.buildAppButton(
                            context,
                            // Share Address Button
                            _addressCopied
                                ? AppButtonType.SUCCESS
                                : AppButtonType.PRIMARY,
                            _addressCopied
                                ? AppLocalization.of(context).addressCopied
                                : AppLocalization.of(context).copyAddress,
                            Dimens.BUTTON_TOP_EXCEPTION_DIMENS, onPressed: () {
                          Clipboard.setData(
                              new ClipboardData(text: widget.address));
                          if (mounted) {
                            setState(() {
                              // Set copied style
                              _addressCopied = true;
                            });
                          }
                          if (_addressCopiedTimer != null) {
                            _addressCopiedTimer.cancel();
                          }
                          _addressCopiedTimer =
                              new Timer(const Duration(milliseconds: 800), () {
                            if (mounted) {
                              setState(() {
                                _addressCopied = false;
                              });
                            }
                          });
                        }),
                      ],
                    ),
                    // A row for Add Contact Button
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsetsDirectional.only(
                              top: Dimens.BUTTON_TOP_EXCEPTION_DIMENS[1],
                              end: Dimens.BUTTON_TOP_EXCEPTION_DIMENS[2]),
                          child: Container(
                            height: 55,
                            width: 55,
                            // Add Contact Button
                            child: !widget.displayName.startsWith("@")
                                ? FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      Sheets.showAppHeightNineSheet(
                                          context: context,
                                          widget: AddContactSheet(
                                              address: widget.address));
                                    },
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(100.0)),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10),
                                    child: Icon(AppIcons.addcontact,
                                        size: 35,
                                        color: _addressCopied
                                            ? StateContainer.of(context)
                                                .curTheme
                                                .successDark
                                            : StateContainer.of(context)
                                                .curTheme
                                                .backgroundDark),
                                  )
                                : SizedBox(),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // A row for View Details button
                Row(
                  children: <Widget>[
                    AppButton.buildAppButton(
                        context,
                        AppButtonType.PRIMARY_OUTLINE,
                        AppLocalization.of(context).viewDetails,
                        Dimens.BUTTON_BOTTOM_DIMENS, onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return UIUtil.showBlockExplorerWebview(
                            context, widget.hash);
                      }));
                    }),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

/// This is used so that the elevation of the container is kept and the
/// drop shadow is not clipped.
///
class _SizeTransitionNoClip extends AnimatedWidget {
  final Widget child;

  const _SizeTransitionNoClip(
      {@required Animation<double> sizeFactor, this.child})
      : super(listenable: sizeFactor);

  @override
  Widget build(BuildContext context) {
    return new Align(
      alignment: const AlignmentDirectional(-1.0, -1.0),
      widthFactor: null,
      heightFactor: (this.listenable as Animation<double>).value,
      child: child,
    );
  }
}
