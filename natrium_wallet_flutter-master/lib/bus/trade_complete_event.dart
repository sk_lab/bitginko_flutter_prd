import 'package:event_taxi/event_taxi.dart';
import 'package:natrium_wallet_flutter/model/state_block.dart';

class TradeCompleteEvent implements Event {
  final StateBlock previous;

  TradeCompleteEvent({this.previous});
}