
// johnny
class HistoryData {
    final int statusCode;
    final String message;
    final List<HData> data;

    HistoryData({this.statusCode,this.message, this.data});

    factory HistoryData.fromJson(Map<String, dynamic> parsedJson){

        var list = parsedJson['Result'] as List;
        List<HData> dataList = list.map((i) => HData.fromJson(i)).toList();


        return HistoryData(
            statusCode: parsedJson['StatusCode'],
            message: parsedJson['Message'],
            data: dataList
        );
    }
}

class HData {
    final String t_id;
    final String t_date;
    final String t_action;
    final String t_target;
    final String t_dest;
    final String t_filled_amt;
    final String t_amt;
    final String t_fee;
    final String t_consideration;
    final String t_operator;
    final String t_status;
    final String t_tmp;
    final String t_address;
    final String t_hash;
    final String ticker;
    final String pid;

    HData({this.t_id, this.t_date, this.t_action, 
    this.t_target, this.t_dest, this.t_filled_amt, 
    this.t_amt, this.t_fee, this.t_consideration, 
    this.t_operator, this.t_status, this.t_tmp, 
    this.t_address,this.t_hash,this.ticker, this.pid});

    factory HData.fromJson(Map<String, dynamic> parsedJson){
        return HData(
            t_id:parsedJson['t_id'],
            t_date:parsedJson['t_date'],
            t_action:parsedJson['t_action'],
            t_target:(parsedJson['t_target']),
            t_dest:parsedJson['t_dest'],
            t_filled_amt:parsedJson['t_filled_amt'],
            t_amt:(parsedJson['t_amt']),
            t_fee:parsedJson['t_fee'],
            t_consideration:parsedJson['t_consideration'],
            t_operator:(parsedJson['t_operator']),
            t_status:parsedJson['t_status'],
            t_tmp:parsedJson['t_tmp'],
            t_address:(parsedJson['t_address']),
            t_hash:parsedJson['t_hash'],
            ticker:parsedJson['ticker'],
            pid:parsedJson['pid']
        );
    }
}