
class UserData {
    final int statusCode;
    final String message;
    final UserDataDetail userInfoDetail;

    UserData({this.statusCode,this.message, this.userInfoDetail});

    factory UserData.fromJson(Map<String, dynamic> parsedJson){

        var tmp = parsedJson['Result'];
        UserDataDetail detail = UserDataDetail.fromJson(tmp);
        return UserData(
            statusCode: parsedJson['StatusCode'],
            message: parsedJson['Message'],
            userInfoDetail: detail
        );
    }
}

class UserDataDetail {
  final String email;
  final String uid;
  final String pid;
  final String tfa;
  final String has_coin;
  final String coin_list;
  final String deposit;
  final String target;
  final String token;

  UserDataDetail({this.email, 
              this.uid,
              this.pid,
              this.tfa,
              this.has_coin,
              this.coin_list,
              this.deposit,
              this.target,
              this.token
            });

  factory UserDataDetail.fromJson(Map<String, dynamic> parsedJson){
        return UserDataDetail(
            email:parsedJson['email'],
            uid:parsedJson['uid'],
            pid:parsedJson['pid'],
            tfa:(parsedJson['tfa']),
            has_coin:parsedJson['has_coin'],
            coin_list:parsedJson['coin_list'],
            deposit:(parsedJson['deposit']),
            target:parsedJson['target'],
            token:parsedJson['token']
        );
    }

}
