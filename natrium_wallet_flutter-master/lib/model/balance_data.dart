
import 'package:decimal/decimal.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';

import '../app_icons2.dart';

class BalanceData {
    final int statusCode;
    final String message;
    final List<CoinData> data;

    BalanceData({this.statusCode,this.message, this.data});

    factory BalanceData.fromJson(Map<String, dynamic> parsedJson){

        var list = parsedJson['Result'] as List;
        // print(list.runtimeType);
        List<CoinData> dataList = list.map((i) => CoinData.fromJson(i)).toList();


        return BalanceData(
            statusCode: parsedJson['StatusCode'],
            message: parsedJson['Message'],
            data: dataList
        );
    }
}

class CoinData {
    final String coinId;
    final String coinTicker;
    final String coinName;
    String balance; // data with all decimals
    String freeze; // data with all decimals
    final int fee;
    final IconData icon;

    CoinData({this.coinId, this.coinTicker, this.coinName, this.balance, this.freeze, this.fee, this.icon});

    factory CoinData.fromJson(Map<String, dynamic> parsedJson){
        String coinTicker = parsedJson['COIN_TICKER'];
        IconData tmpIcon;
        if (coinTicker == "ETH") {
            tmpIcon = AppIcons2.eth;
        } else if (coinTicker == "LTC") {
            tmpIcon = AppIcons2.ltc;
        } else if (coinTicker == "USDT") {
            tmpIcon = AppIcons2.usdt;
        } else if (coinTicker == "BCH") {
            tmpIcon = AppIcons2.bch;
        } else if (coinTicker == "BTC") {
            tmpIcon = AppIcons2.btc;
        } else if (coinTicker == "DOGE") {
            tmpIcon = AppIcons2.doge;
        }

        return CoinData(
            coinId:parsedJson['COIN_ID'],
            coinTicker:parsedJson['COIN_TICKER'],
            coinName:parsedJson['COIN_NAME'],
            balance:(parsedJson['balance']),
            freeze:parsedJson['freeze'],
            fee:parsedJson['fee'],
            icon:tmpIcon
        );
    }

    String get avaBalance => _get4decimalsNum(balance);

    String get freezeBalance {
      if (freeze == null)
        return "0";
      else 
        return _get4decimalsNum(freeze);
    } 

    String get totalBalance {
      Decimal converted1 = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(balance);
      if (freeze == null) freeze = "0";
      Decimal converted2 = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(freeze);
      Decimal total = converted1 + converted2;
      return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(total.toDouble());
    }

    // set avaBalance(String bal) {
    //   this.balance = bal;
    // }

    String _get4decimalsNum(String bal) {
      Decimal converted = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(bal);
      return NumberFormat.currency(locale:"en_US", symbol: "", decimalDigits:4).format(converted.toDouble());
    }

}
