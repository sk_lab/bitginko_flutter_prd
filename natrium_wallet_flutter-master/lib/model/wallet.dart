import 'package:decimal/decimal.dart';
import 'package:intl/intl.dart';
import 'package:natrium_wallet_flutter/model/available_currency.dart';
import 'package:natrium_wallet_flutter/model/user_data.dart';
import 'package:natrium_wallet_flutter/network/model/response/account_history_response_item.dart';
import 'package:natrium_wallet_flutter/util/numberutil.dart';
import 'package:natrium_wallet_flutter/model/balance_data.dart';

/// Main wallet object that's passed around the app via state
class AppWallet {
  static const String defaultRepresentative = 'bitginko_2020_representative';

  bool _loading; // Whether or not app is initially loading
  bool _historyLoading; // Whether or not we have received initial account history response
  String _address;
  BigInt _accountBalance;
  String _frontier;
  String _openBlock;
  String _representativeBlock;
  String _representative;
  String _localCurrencyPrice;
  String _btcPrice;
  // String _loginToken; // johnny
  UserData _userAcctInfo; // johnny
  BalanceData _acctBalInfo; // johnny
  int _blockCount;
  List<AccountHistoryResponseItem> _history;


  AppWallet({String address, BigInt accountBalance, String frontier, String openBlock, 
            String representativeBlock, String representative, String localCurrencyPrice,
            String btcPrice, int blockCount, List<AccountHistoryResponseItem> history, 
            bool loading, bool historyLoading, 
            var userInfo, var acctBalInfo}) {
    this._address = address;
    this._accountBalance = BigInt.from(22).pow(30); // accountBalance ?? BigInt.zero;
    this._frontier = frontier;
    this._openBlock = openBlock;
    this._representativeBlock = representativeBlock;
    this._representative = representative;
    this._localCurrencyPrice = localCurrencyPrice ?? "0";
    this._btcPrice = btcPrice ?? "0";
    this._blockCount = blockCount ?? 0;
    this._history = history ?? new List<AccountHistoryResponseItem>();
    this._loading = loading ?? true;
    this._historyLoading = historyLoading ?? true;

    // this._loginToken = loginToken ?? ""; // johnny
    this._userAcctInfo = new UserData.fromJson(userInfo); // johnny
    this._acctBalInfo = new BalanceData.fromJson(acctBalInfo); // johnny
  }

  String get address => _address;

  set address(String address) {
    this._address = address;
  }

  BigInt get accountBalance => _accountBalance;

  set accountBalance(BigInt accountBalance) {
    this._accountBalance = accountBalance;
  }

  // johnny
  List<CoinData> getBalanceData() {
    return this._acctBalInfo.data;
  }

  // johnny
  set acctBalInfo(var newData) {
    this._acctBalInfo = new BalanceData.fromJson(newData);
  }

  // johnny: get specifc coin data
  CoinData getCoinData(String coinSymbol) {

    for (int i=0; i<this._acctBalInfo.data.length; i++){
      
      CoinData currCoin = this._acctBalInfo.data[i];
      if (currCoin.coinTicker == coinSymbol){
        return currCoin;
      }
    }

    return null;
  }

  // johnny
  String getBalance_HKD() {    

    for (int i=0; i<this._acctBalInfo.data.length; i++){
      if (this._acctBalInfo.data[i].coinTicker == "HKD"){
        String bal = this._acctBalInfo.data[i].balance;
        Decimal converted = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal(bal);
        return NumberFormat.currency(locale:"en_US", symbol: "HKD ", decimalDigits:2).format(converted.toDouble());
      }
    }

    return "0";
  }

  // johnny
  String get loginToken {
    return _userAcctInfo.userInfoDetail.token;
  }

  // johnny
  String get uid {
    return _userAcctInfo.userInfoDetail.uid;
  }

  // johnny
  String get pid {
    return _userAcctInfo.userInfoDetail.pid;
  }

  // johnny
  String get email {
    return _userAcctInfo.userInfoDetail.email;
  }

  // Get pretty account balance version
  String getAccountBalanceDisplay() {
    if (accountBalance == null) {
      return "0";
    }

    var tmp = this._acctBalInfo.data[0].balance;

    return tmp.toString();
  }

  // Johnny: use this to replace the above getAccountBalanceDisplay()
  String getLocalCurrencyBalance(AvailableCurrency currency, {String locale = "en_US"}) {
    Decimal converted = Decimal.parse("1") * NumberUtil.getRawAsUsableDecimal("616.222222222222"); // _accountBalance.toString()
    return NumberFormat.currency(locale:locale, symbol: currency.getCurrencySymbol()).format(converted.toDouble());
  }

  // Johnny: not gonna use this
  String getLocalCurrencyPrice(AvailableCurrency currency, {String locale = "en_US"}) {
    Decimal converted = Decimal.parse(_localCurrencyPrice) * NumberUtil.getRawAsUsableDecimal(_accountBalance.toString());
    return NumberFormat.currency(locale:locale, symbol: currency.getCurrencySymbol()).format(converted.toDouble());
  }

  set localCurrencyPrice(String value) {
    _localCurrencyPrice = value;
  }

  String get localCurrencyConversion {
    return _localCurrencyPrice;
  }

  String get btcPrice {
    Decimal converted = Decimal.parse(_btcPrice) * NumberUtil.getRawAsUsableDecimal(_accountBalance.toString());
    // Show 4 decimal places for BTC price if its >= 0.0001 BTC, otherwise 6 decimals
    if (converted >= Decimal.parse("0.0001")) {
      return new NumberFormat("#,##0.0000", "en_US").format(converted.toDouble());
    } else {
      return new NumberFormat("#,##0.000000", "en_US").format(converted.toDouble());
    }
  }

  set btcPrice(String value) {
    _btcPrice = value;
  }

  String get representative {
   return _representative ?? defaultRepresentative;
  }

  set representative(String value) {
    _representative = value;
  }

  String get representativeBlock => _representativeBlock;

  set representativeBlock(String value) {
    _representativeBlock = value;
  }

  String get openBlock => _openBlock;

  set openBlock(String value) {
    _openBlock = value;
  }

  String get frontier => _frontier;

  set frontier(String value) {
    _frontier = value;
  }

  int get blockCount => _blockCount;

  set blockCount(int value) {
    _blockCount = value;
  }

  List<AccountHistoryResponseItem> get history => _history;

  set history(List<AccountHistoryResponseItem> value) {
    _history = value;
  }

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
  }

  bool get historyLoading => _historyLoading;

  set historyLoading(bool value) {
    _historyLoading = value;
  }
}