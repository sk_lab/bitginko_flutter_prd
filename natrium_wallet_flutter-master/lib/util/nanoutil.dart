import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:nanodart/nanodart.dart';

import 'package:natrium_wallet_flutter/model/db/appdb.dart';
import 'package:natrium_wallet_flutter/model/db/account.dart';
import 'package:natrium_wallet_flutter/appstate_container.dart';
import 'package:natrium_wallet_flutter/localization.dart';
import 'package:natrium_wallet_flutter/model/user_data.dart';
import 'package:natrium_wallet_flutter/service_locator.dart';

import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';

class NanoUtil {
  static String seedToPrivate(Map<dynamic, dynamic> params) {
    return NanoKeys.seedToPrivate(params['seed'], params['index']);
  }

  Future<String> seedToPrivateInIsolate(String seed, int index) async {
    return await compute(NanoUtil.seedToPrivate, {'seed':seed, 'index':index});
  }

  static String seedToAddress(Map<dynamic, dynamic> params) {
    return NanoAccounts.createAccount(NanoAccountType.NANO, NanoKeys.createPublicKey(seedToPrivate(params)));
  }

  Future<String> seedToAddressInIsolate(String seed, int index) async {
    return await compute(NanoUtil.seedToAddress, {'seed':seed, 'index':index});
  }

  Future<void> loginAccount(String seed, BuildContext context) async {
    Account selectedAcct = await sl.get<DBHelper>().getSelectedAccount(seed);
    if (selectedAcct == null) {
      selectedAcct = Account(index: 0, lastAccess: 0, name: AppLocalization.of(context).defaultAccountName, selected: true);
      await sl.get<DBHelper>().saveAccount(selectedAcct);
    }
    StateContainer.of(context).updateWallet(account: selectedAcct);
  }

  // johnny: refer to function "loginAccountBitginko" above, to init a wallet instance 
  Future<String> loginAccountBitginko(BuildContext context, {String email, String md5pw}) async {
    
    // var loginURL = 'https://uat-bitginko.standardkepler.com/api/v4/uri/mobile.login.api.php'; // UAT
    var loginURL = 'https://bitginko.com/api/v4/uri/mobile.login.api.php'; // PRD

    // var acctBalanceURL = 'https://uat-bitginko.standardkepler.com/api/v4/uri/getAccountBalance.api.php'; // UAT
    var acctBalanceURL = 'https://bitginko.com/api/v4/uri/getAccountBalance.api.php'; // PRD

    // print(email);
    // print(md5pw);

    // johnnytodo: get the data from the textbox input. not this hardcode
    var loginData = {
      'data': {
        'name': email, //'chun1auyeung@gmail.com', // email, // johnnytodo: tmp access
        'pass': md5pw //'9a15b2f417c1f2409fbb424be8d39aaa' // md5pw, // md5-ed input-password here // johnnytodo: tmp access
      }
    };

    var loginResponse = await http.post(
        loginURL,
        body: jsonEncode(loginData),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'}
        // headers: {"Content-Type": "application/json"},
      );
    print('[debug] Login Response statusCode: ${loginResponse.statusCode}'); // 200 means commuiciate ok
    // print('Response body: ${loginResponse.body}');
    // print(jsonDecode(loginResponse.body));

    var userObj = jsonDecode(loginResponse.body);
    print(userObj);

    var loginStatusCode = userObj['StatusCode'];
    if (loginStatusCode == 0) {

      var userToken = userObj['Result']['token'];
      // print('token:'+ userToken);

      var tokenData = {
        'data': {
          'token': userToken
        }
      };
    
      Response acctBalanceResponse;
      Dio dio = new Dio();
      acctBalanceResponse = await dio.post(
          acctBalanceURL,
          data: FormData.fromMap(tokenData),
          options: Options(responseType: ResponseType.plain,));
      // print(acctBalanceResponse.toString());

    
      // print('[debug] acctBalance Response statusCode: ${response.statusCode}'); // 200 means commuiciate ok
      // print(jsonDecode(acctBalanceResponse.toString()));
      var acctBalObj = jsonDecode(acctBalanceResponse.toString());
      // print(acctObj['Result'][0]['balance']);

      Account selectedAcct = Account(index: 0, lastAccess: 0, name: AppLocalization.of(context).defaultAccountName, selected: true);
      StateContainer.of(context).updateWalletBitginko(account: selectedAcct, userAcctInfo: userObj, acctInfo: acctBalObj);

      // Account selectedAcct = await sl.get<DBHelper>().getSelectedAccount(seed);
      // if (selectedAcct == null) {
      //   selectedAcct = Account(index: 0, lastAccess: 0, name: AppLocalization.of(context).defaultAccountName, selected: true);
      //   await sl.get<DBHelper>().saveAccount(selectedAcct);
      // }
      // StateContainer.of(context).updateWallet(account: selectedAcct);
      return "Success";

    } else {
      return userObj['Message'].toString();
    }

    
  }

  // johnny
  Future<String> registerAccountBitginko(BuildContext context, 
      {String email, String pw, String name, String code, String phone}) async {
    
    // var signupURL = 'https://uat-bitginko.standardkepler.com/api/v4/uri/mobile.signup.api.php'; // UAT
    var signupURL = 'https://bitginko.com/api/v4/uri/mobile.signup.api.php'; // PRD

    var postData = {
      'data': {
        'name': name, // 'flutter johnny', // 'chun1auyeung@gmail.com', 
        'pass': pw, //'9a15b2f417c1f2409fbb424be8d39aaa', // md5-ed input-password here
        'email': email, // 'chun1auyeung1233aaa@gmail.com',
        'phone': phone, // '63372096',
        'type': 'I', // fixed
        'accp': true, // fixed
        'referral': code, // '',
      }
    };

    var signupResponse = await http.post(
        signupURL,
        body: jsonEncode(postData),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'}
        // headers: {"Content-Type": "application/json"},
      );
    print('[debug] Signup Response statusCode: ${signupResponse.statusCode}'); // 200 means commuiciate ok
    print('Response body: ${signupResponse.body}');
    // print(jsonDecode(signupResponse.body));

    var userObj = jsonDecode(signupResponse.body);

    var signupStatusCode = userObj['StatusCode'];    
    if (signupStatusCode == 0) {
      return "Success";
    } else {
      return userObj['Message'].toString();
    }

  }

  

}
